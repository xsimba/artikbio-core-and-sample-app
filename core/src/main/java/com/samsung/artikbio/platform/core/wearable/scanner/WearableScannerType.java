package com.samsung.artikbio.platform.core.wearable.scanner;

/**
 * Types of Wearable Scanners. Only the enumerated type BLE is implemented. Other types of wearable
 * scanners may be implemented in future releases.
 */
public enum WearableScannerType {
	/**
	 * Bluetooth Low Energy.
	 */
	BLE,
}
