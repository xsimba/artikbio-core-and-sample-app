package com.samsung.artikbio.platform.core.persistance;

/**
 * Exception thrown when a {@link Dao} method attempts to create
 * a record which already exists.
 */
public class RecordExistsException extends RuntimeException {

	private static final long serialVersionUID = 930799831475377571L;

	/**
	 * Creates Exception with message.
	 *
	 * @param message to send with Exception.
	 */
	public RecordExistsException(final String message) {
		super(message);
	}

	/**
	 * Creates message for this Exception from {@code clazz} and {@code id}.
	 *
	 * @param clazz we were trying to retrieve.
	 * @param id    of record we were looking for.
	 */
	public RecordExistsException(final Class<?> clazz, final Object id) {
		this(clazz.getSimpleName() + " record with id: " + id + " exists.");
	}

}