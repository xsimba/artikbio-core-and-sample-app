package com.samsung.artikbio.platform.core.persistance.datastore;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.samsung.artikbio.platform.core.persistance.model.UserIdentity;
import com.samsung.artikbio.platform.core.persistance.model.WearableIdentity;
import com.samsung.artikbio.platform.core.persistance.realm.CoreStorageHelper;
import com.samsung.artikbio.platform.core.persistance.realm.StorageHelper;
import com.samsung.artikbio.platform.core.persistance.realm.UUIDRealmDatabaseDao;
import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.Validations;

import java.io.File;
import java.util.Arrays;
import java.util.UUID;

import io.realm.RealmConfiguration;

/**
 * Provides static methods for saving, updating and retrieving
 * {@link UserIdentity} and {@link WearableIdentity} entities.
 */
public final class LocalDataStore {

	/**
	 * Name to use for Realm database configurations created within
	 * Core SDK.
	 */
	public static final String REALM_NAME = "com-samsung-artikbio-platform-core";

	/**
	 * Name of the system preference to store the current user identity uuid.
	 */
	private static final String PREF_CURRENT_USER_IDENTITY
			= "com.samsung.artikbio.platform.core.current.user.uuid";

	/** Key used to encrypt the data store. Cannot be null (encryption is required) */
	private static byte[] mEncryptionKey;

	/**
	 * Default private constructor (non-instantiable class).
	 */
	private LocalDataStore () {
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Initializes the LocalDataStore with a default current user if there's not one.
	 *
	 * @param encryptionKey the encryption key used to encrypt the data store file
	 *
	 * @throws IllegalArgumentException if {@code encryptionKey} is not
	 * {@link RealmConfiguration#KEY_LENGTH}
	 *                                  {@code byte}s long
	 */
	public static void init (final byte[] encryptionKey) {
		if (encryptionKey == null) {
			throw new IllegalArgumentException(
					"LocalDataStore can not be initialized with null encryptionKey.");
		}
		setEncryptionKey(encryptionKey);
		UUID userIdentityUUID = LocalDataStore.getCurrentUserIdentityUUID();
		if (userIdentityUUID == null) {
			LocalDataStore.setCurrentUserIdentity(new UserIdentity(
					null, null, null, null,
					null));
		}
	}

	/**
	 * Creates a wearable identity in the local data store for the given {@code address}.
	 * If an identity with the same {@code address} is already registered in the local data store,
	 * the existing {@link WearableIdentity} properties are updated instead.
	 *
	 * @param uuid             the unique identifier for the wearable device
	 * @param address          the address of the wearable device
	 * @param displayName      the displayName of the wearable device
	 * @param manufacturer     the manufacturer of the wearable device
	 * @param model            the model of the wearable device
	 * @param firmwareRevision the firmwareRevision of the wearable device
	 * @param softwareRevision the softwareRevision of the wearable device
	 * @param hardwareRevision the hardwareRevision of the wearable device
	 *
	 * @return the newly registered  {@link WearableIdentity} or the one that was updated
	 */
	public static WearableIdentity registerWearable (@NonNull final UUID uuid,
	                                                 @NonNull final String address,
	                                                 @Nullable final String displayName,
	                                                 @NonNull final String manufacturer,
	                                                 @NonNull final String model,
	                                                 @NonNull final String firmwareRevision,
	                                                 @NonNull final String softwareRevision,
	                                                 @NonNull final String hardwareRevision) {
		assertInitialized();
		Validations.validateNotNull(uuid, "uuid");
		Validations.validateNotNull(address, "address");
		WearableIdentityDao wearableIdentityDao = new WearableIdentityDao(
				ContextProvider.getInstance().getApplicationContext());
		try {
			WearableIdentity wearableIdentity = new WearableIdentity(
					uuid, address, displayName,
					manufacturer, model,
					firmwareRevision,
					softwareRevision,
					hardwareRevision);
			return wearableIdentityDao.save(wearableIdentity);
		} finally {
			closeDao(wearableIdentityDao);
		}
	}

	private static void assertInitialized () {
		if (mEncryptionKey == null) {
			throw new IllegalStateException("LocalDataStore needs to be initialized with init()");
		}
	}

	/**
	 * Closes the dao received.
	 *
	 * @param dao to close
	 */
	private static void closeDao (final UUIDRealmDatabaseDao<?, ?> dao) {
		if (!isInMemory()) {
			dao.close();
		}
	}

	/**
	 * Determines whether Core is storing data in memory or on disk.
	 *
	 * @return {@code true} if data is being persisted only to memory.
	 */
	static boolean isInMemory () {
		return CoreStorageHelper.INSTANCE.isInMemory();
	}

	/**
	 * Creates a user identity and sets it as the current user identity for associating with
	 * wearable
	 * events.
	 * If the userIdentity uuid is specified and found in the local data store, the contents of the
	 * matching userIdentity will be updated
	 *
	 * @param userIdentity the current {@link UserIdentity} to create and set
	 *
	 * @return the UUID of the current user after it was set
	 */
	public static UUID setCurrentUserIdentity (@NonNull final UserIdentity userIdentity) {
		assertInitialized();
		Validations.validateNotNull(userIdentity, "userIdentity");
		UserIdentityDao userIdentityDao = new UserIdentityDao(
				ContextProvider.getInstance().getApplicationContext());
		UUID userUUID = null;
		try {
			UserIdentity currentUserIdentity = getCurrentUserIdentity();
			UserIdentity userIdentityToSave = userIdentity;
			if (userIdentity.getId() == null && currentUserIdentity != null
					&& currentUserIdentity.getUserId() == null) {
				// userIdentity is being set for the first time, associate previous anonymous
				// events
				// to this first user, by updating auto-generated default user details
				userIdentityToSave = new UserIdentity(currentUserIdentity.getId(),
						userIdentity.getUserId(),
						userIdentity.getFirstName(), userIdentity.getLastName(),
						userIdentity.getEmail());

			}
			userUUID = userIdentityDao.save(userIdentityToSave).getId();
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(
					ContextProvider.getInstance().getApplicationContext());
			sp.edit().putString(PREF_CURRENT_USER_IDENTITY, userUUID.toString()).apply();
		} finally {
			closeDao(userIdentityDao);
		}
		return userUUID;
	}

	/**
	 * Returns the UUID of the current user identity.
	 *
	 * @return the UUID of the current user identity
	 */
	public static UUID getCurrentUserIdentityUUID () {
		assertInitialized();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(
				ContextProvider.getInstance().getApplicationContext());
		UUID result = null;
		String uuidString = sp.getString(PREF_CURRENT_USER_IDENTITY, null);
		if (uuidString != null) {
			result = UUID.fromString(uuidString);
		}
		return result;
	}

	/**
	 * Returns the {@link WearableIdentity} object associated with the uuid or null if not found.
	 *
	 * @param uuid a wearable identity unique identifier
	 *
	 * @return the {@link WearableIdentity} object associated with the uuid or null if not found
	 */
	public static WearableIdentity getWearableIdentity (final UUID uuid) {
		assertInitialized();
		if (uuid == null) {
			return null;
		}
		WearableIdentityDao wearableIdentityDao = new WearableIdentityDao(
				ContextProvider.getInstance().getApplicationContext());
		WearableIdentity result = getWearableIdentity(wearableIdentityDao, uuid);
		closeDao(wearableIdentityDao);
		return result;
	}

	/**
	 * Returns the {@link WearableIdentity} object associated with the uuid or null if not found.
	 *
	 * @param wearableIdentityDao a {@link WearableIdentityDao} instance to use
	 * @param uuid                a wearable identity unique identifier
	 *
	 * @return the {@link WearableIdentity} object associated with the uuid or null if not found
	 */
	private static WearableIdentity getWearableIdentity (
			final WearableIdentityDao wearableIdentityDao,
			final UUID uuid) {
		WearableIdentity result = null;
		if (uuid != null) {
			result = wearableIdentityDao.read(uuid);
		}
		return result;
	}

	/**
	 * Returns the {@link WearableIdentity} object associated with the provided deviceAddress or
	 * null if not found.
	 *
	 * @param deviceAddress a device address
	 *
	 * @return the {@link WearableIdentity} object associated with the provided deviceAddress or
	 * null if not found
	 */
	public static WearableIdentity getWearableIdentity (final String deviceAddress) {
		assertInitialized();
		if (deviceAddress == null) {
			return null;
		}
		WearableIdentityDao wearableIdentityDao = new WearableIdentityDao(
				ContextProvider.getInstance().getApplicationContext());
		WearableIdentity result = getWearableIdentity(wearableIdentityDao,
				wearableIdentityDao.getUUIDByAddress(deviceAddress));
		closeDao(wearableIdentityDao);
		return result;
	}

	/**
	 * Returns the current {@link UserIdentity}. Should never be {@code null}
	 *
	 * @return the current {@link UserIdentity}
	 */
	public static UserIdentity getCurrentUserIdentity () {
		assertInitialized();
		return getUserIdentity(getCurrentUserIdentityUUID());
	}

	/**
	 * Returns the {@link UserIdentity} object associated with the uuid or null if not found.
	 *
	 * @param uuid a user identity unique identifier
	 *
	 * @return the {@link UserIdentity} object associated with the uuid or null if not found
	 */
	public static UserIdentity getUserIdentity (final UUID uuid) {
		assertInitialized();
		if (uuid == null) {
			return null;
		}
		UserIdentityDao userIdentityDao = new UserIdentityDao(ContextProvider.getInstance()
				.getApplicationContext());
		UserIdentity result = getUserIdentity(userIdentityDao, uuid);
		closeDao(userIdentityDao);
		return result;
	}

	/**
	 * Return the {@link UserIdentity} object associated with the provided userId or
	 * null if not found.
	 *
	 * @param userId a domain specific unique user id
	 *
	 * @return the {@link UserIdentity} object associated with the provided userId or null if not
	 * found
	 */
	public static UserIdentity getUserIdentity (final String userId) {
		assertInitialized();
		if (userId == null) {
			return null;
		}
		UserIdentityDao userIdentityDao = new UserIdentityDao(ContextProvider.getInstance()
				.getApplicationContext());
		UserIdentity result = getUserIdentity(userIdentityDao,
				userIdentityDao.getUUIDByUserId(userId));
		closeDao(userIdentityDao);
		return result;
	}

	/**
	 * Returns the {@link UserIdentity} object associated with the uuid or null if not found.
	 *
	 * @param userIdentityDao a {@link UserIdentityDao} instance to use
	 * @param uuid            a user identity unique identifier
	 *
	 * @return the {@link UserIdentity} object associated with the uuid or null if not found
	 */
	private static UserIdentity getUserIdentity (final UserIdentityDao userIdentityDao,
	                                             final UUID uuid) {
		UserIdentity result = null;
		if (uuid != null) {
			result = userIdentityDao.read(uuid);
		}
		return result;
	}

	/**
	 * Removes the {@link UserIdentity} object associated with the uuid and returns true if removal
	 * was successful or false if it was not.
	 *
	 * @param uuid a user identity unique identifier to remove
	 *
	 * @return {@code true} if the user identity was found and could be removed
	 */
	public static boolean removeUserIdentity (@NonNull final UUID uuid) {
		assertInitialized();
		Validations.validateNotNull(uuid, "uuid");
		boolean result;
		if (uuid.equals(getCurrentUserIdentityUUID())) {
			// trying to erase current user identity
			final SharedPreferences sp =
					PreferenceManager.getDefaultSharedPreferences(ContextProvider.getInstance()
							.getApplicationContext());
			sp.edit().remove(PREF_CURRENT_USER_IDENTITY).apply();
			result = removeEntityByUUID(new UserIdentityDao(ContextProvider.getInstance()
					.getApplicationContext()), uuid);
			// set new default current user identity
			LocalDataStore.setCurrentUserIdentity(new UserIdentity(
					null, null, null, null,
					null));
		} else {
			result = removeEntityByUUID(new UserIdentityDao(ContextProvider.getInstance()
					.getApplicationContext()), uuid);
		}
		return result;
	}

	/**
	 * Removes the {@link WearableIdentity} object associated with the uuid and returns true if
	 * removal was successful or false if it was not.
	 *
	 * @param uuid a user identity unique identifier to remove
	 *
	 * @return {@code true} if the wearable identity was found and could be removed
	 */
	public static boolean removeWearableIdentity (@NonNull final UUID uuid) {
		assertInitialized();
		Validations.validateNotNull(uuid, "uuid");
		return removeEntityByUUID(new WearableIdentityDao(ContextProvider.getInstance()
				.getApplicationContext()), uuid);
	}

	private static boolean removeEntityByUUID (final UUIDRealmDatabaseDao<?, ?> dao,
	                                           final UUID uuid) {
		if (uuid == null || dao == null) {
			return false;
		}
		boolean result = dao.delete(uuid);
		closeDao(dao);
		return result;
	}

	/**
	 * Informs Core to store data in memory only, not persisted to disk.
	 */
	public static void inMemory () {
		CoreStorageHelper.INSTANCE.inMemory();
	}

	/**
	 * Sets or resets durability. Used for testing.
	 *
	 * @param durability to set.
	 */
	static void setDurability (final StorageHelper.Durability durability) {
		CoreStorageHelper.INSTANCE.setDurability(durability);
	}

	/**
	 * Gets a {@link RealmConfiguration} appropriate for use by core DAO
	 * instances based on the given {@code context}.
	 *
	 * @param context for which {@code RealmConfiguration} is created.
	 *
	 * @return the new {@code RealmConfiguration}.
	 */
	public static RealmConfiguration getConfiguration (@NonNull final Context context) {
		Validations.validateNotNull(context, "context");
		RealmConfiguration.Builder builder = CoreStorageHelper.INSTANCE
				.getConfigurationBuilder(context);
		if (mEncryptionKey != null) {
			builder = builder.encryptionKey(mEncryptionKey);
		} else {
			throw new IllegalStateException(
					"Encryption key is not set. Initialize with LocalDataStore.init().");
		}
		return builder.build();
	}

	/**
	 * Clear all data managed by {@code LocalDataStore} and re-{@link #init}
	 * to get back to a pristine state.
	 */
	public static void reset () {
		assertInitialized();
		final UserIdentityDao userIdentityDao =
				new UserIdentityDao(ContextProvider.getInstance().getApplicationContext());
		final WearableIdentityDao wearableIdentityDao =
				new WearableIdentityDao(ContextProvider.getInstance().getApplicationContext());
		final SharedPreferences sp =
				PreferenceManager.getDefaultSharedPreferences(ContextProvider.getInstance()
						.getApplicationContext());
		try {
			userIdentityDao.reset();
			wearableIdentityDao.reset();
			sp.edit().remove(PREF_CURRENT_USER_IDENTITY).apply();
		} finally {
			closeDao(userIdentityDao);
			closeDao(wearableIdentityDao);
		}
		LocalDataStore.init(mEncryptionKey);
	}

	/**
	 * Store the {@code encryptionKey} to be used for encrypting the data store.
	 *
	 * @param encryptionKey to be used to setEncryptionKey data store.
	 *
	 * @return the previously set {@code encryptionKey} if one existed, {@code null} otherwise.
	 *
	 * @throws IllegalArgumentException if {@code encryptionKey} is not
	 * {@link RealmConfiguration#KEY_LENGTH}
	 *                                  {@code byte}s long.
	 */
	private static byte[] setEncryptionKey (final byte[] encryptionKey) {
		final byte[] previousEncryptionKey = LocalDataStore.mEncryptionKey;
		if (encryptionKey == null) {
			throw new IllegalArgumentException("encryptionKey can not be set to null.");
		} else if (encryptionKey.length != RealmConfiguration.KEY_LENGTH) {
			throw new IllegalArgumentException(
					String.format(
							"The provided key must be %s bytes. Yours was: %s",
							RealmConfiguration.KEY_LENGTH,
							encryptionKey.length
					)
			);
		} else {
			LocalDataStore.mEncryptionKey = Arrays.copyOf(
					encryptionKey,
					RealmConfiguration.KEY_LENGTH
			);
		}
		return previousEncryptionKey;
	}

	/**
	 * Removes the data store file and all persisted information is lost. If the data store is only
	 * persisted to memory no information is removed and the returning value is {@code false}
	 *
	 * @return {@code true} if the data store file was correctly deleted, {@code false} if the file
	 * couldn't be deleted.
	 */
	public static boolean deleteDataStore () {
		assertInitialized();
		boolean result = false;
		if (!isInMemory()) {
			try {
				reset();
			} finally {
				RealmConfiguration configuration = getConfiguration(
						ContextProvider.getInstance().getApplicationContext());
				File realmFile = new File(configuration.getRealmDirectory(),
						configuration.getRealmFileName());
				result = realmFile.delete();
			}
		}
		return result;
	}
}
