package com.samsung.artikbio.platform.core;

import android.content.Context;

import com.samsung.artikbio.platform.core.persistance.datastore.LocalDataStore;
import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.Logger;

/**
 * Core singleton class used for initialization, setting up local persistent storage, and
 * application logging
 */
public final class Core {

	/**
	 * Default private constructor (non-instantiable class).
	 */
	private Core () {
		// Non-instantiable class
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Initializes Core asynchronously and stores the given {@link Context} for later use.
	 * When initialization is complete, {@link ICoreInitListener#onCoreInitialized()} is invoked.
	 *
	 * @param context          to hold.
	 * @param coreInitListener a {@link ICoreInitListener} instance to get notified when the
	 *                         initialization process is complete.
	 * @param encryptionKey    <For Future Purpose></For>
	 *
	 * @throws IllegalArgumentException if given {@code context} is {@code null} or if the {@code
	 *                                  encryptionKey} is {@code null}
	 */
	public static synchronized void init (final Context context,
	                                      final ICoreInitListener coreInitListener,
	                                      final byte[] encryptionKey) {
		if (context == null) {
			throw new IllegalArgumentException("Core can not be initialized with null Context.");
		}
		ContextProvider.createInstance(context);
		LocalDataStore.init(encryptionKey);
		Logger.init();
		if (coreInitListener != null) {
			coreInitListener.onCoreInitialized();
		}
		Logger.d("Core Initialized");
	}

	/**
	 * Initializes Core and stores the given {@link Context} for later use.
	 *
	 * @param context       to hold.
	 * @param encryptionKey to be passed along to {@link LocalDataStore#init(byte[])} for
	 *                         encrypting
	 *                      the database file
	 *
	 * @throws IllegalArgumentException if given {@code context} is {@code null} or if the {@code
	 *                                  encryptionKey} is {@code null}
	 */
	public static synchronized void init (final Context context,
	                                      final byte[] encryptionKey) {
		init(context, null, encryptionKey);
	}
}
