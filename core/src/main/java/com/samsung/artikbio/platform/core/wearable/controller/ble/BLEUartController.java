package com.samsung.artikbio.platform.core.wearable.controller.ble;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.applications.AbstractBLEProfile;
import com.samsung.artikbio.platform.core.applications.UartBleService;
import com.samsung.artikbio.platform.core.event.WearableEvent;
import com.samsung.artikbio.platform.core.uart.UartEvent;
import com.samsung.artikbio.platform.core.uart.UartService;
import com.samsung.artikbio.platform.core.util.Constants;
import com.samsung.artikbio.platform.core.wearable.ble.BLEConnection;
import com.samsung.artikbio.platform.core.wearable.controller.AbstractBLEController;
import com.samsung.artikbio.platform.core.wearable.controller.IUartController;

public class BLEUartController extends AbstractBLEController implements IUartController {


	private UartBleService mUartBleService;

	private UartService mUartService;

	private boolean isStreamingStarted;
	private AbstractBLEProfile.BLEProfileSubscribeCallback mSubscribeCallback = (service,
	                                                                             characteristic,
	                                                                             status) -> {
		if (!isStreamingStarted) {
			isStreamingStarted = mUartService.startUartStreaming();
		}
	};
	private AbstractBLEProfile.BLEProfileUnsubscribeCallback mUnsubscribeCallback = (service,
	                                                                                 characteristic,
	                                                                                 status) -> {
		if (isStreamingStarted) {
			isStreamingStarted = mUartService.stopUartStreaming();
		}

	};

	/**
	 * Public constructor
	 *
	 * @param bleConnection a {@link BLEConnection} associated with a wearable device
	 */
	public BLEUartController (@NonNull final BLEConnection bleConnection, @NonNull final
	BLEWearableController wearableController) {
		super(bleConnection, wearableController);
		mUartService = new UartService(mWearableController);
	}

	protected synchronized void enable (@NonNull final Class<? extends WearableEvent> eventClass) {
		if (eventClass.equals(UartEvent.class)) {
			if (mUartBleService == null) {
				mUartBleService = new UartBleService(mBLEConnection);
			}
			mUartBleService.subscribe(mSubscribeCallback, mUartService);
		}
	}

	protected synchronized void disable (@NonNull final Class<? extends WearableEvent>
			                                     eventClass) {
		if (eventClass.equals(UartEvent.class)) {
			if (mUartBleService != null) {
				mUartBleService.unsubscribe(mUnsubscribeCallback);
				mUartBleService = null;
			}
		}
	}

	protected synchronized boolean isEnabled (@NonNull final Class<? extends WearableEvent>
			                                          eventClass) {
		if (eventClass.equals(UartEvent.class)) {
			return mUartBleService != null && mUartBleService.isSubscribed(Constants
					.UART_SERVICE_UUID, Constants.UART_RX_UUID);
		}
		return false;
	}

	protected boolean isBLEUartEventClass (final Class eventClass) {
		return eventClass != null && (eventClass.equals(UartEvent.class));
	}
}
