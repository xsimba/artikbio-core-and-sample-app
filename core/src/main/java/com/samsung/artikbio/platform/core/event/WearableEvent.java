package com.samsung.artikbio.platform.core.event;

import com.samsung.artikbio.platform.core.util.Validations;

import java.util.UUID;

public class WearableEvent implements Event {

	protected UUID mWearableIdentityUuid;

	public WearableEvent (final UUID wearableIdentityUuid) {
		Validations.validateNotNull(wearableIdentityUuid, "wearableIdentityUuid");
		mWearableIdentityUuid = wearableIdentityUuid;
	}

	public UUID getWearableIdentityUuid () {
		return mWearableIdentityUuid;
	}

	@Override
	public String getEventId () {
		return this.toString();
	}
}
