package com.samsung.artikbio.platform.core.persistance.realm;

/**
 * Meant to be implemented by a mutable model object in order to aid in
 * persisting an SDK exposed immutable model.
 *
 * @param <Model>       immutable data type to be saved by {@link RealmDatabaseDao}.
 * @param <Persistable> mutable data type that mirrors {@link Model}. This is
 *                      generally the class implementing {@code RealmConverter}.
 */
public interface RealmConverter<Model, Persistable> {

	/**
	 * Copy the values from an immutable {@link Model} to a mutable one.
	 * Implementation should call all the setters of the mutable object so
	 * they do not need to be called between {@link io.realm.Realm#beginTransaction()}
	 * and {@link io.realm.Realm#commitTransaction()}. If {@link Persistable}
	 * is an active realm object with a primary key, it should not be set
	 * during update (only create).
	 *
	 * @param immutable {@link Model} with data to be copied into a
	 *                  mutable instance.
	 * @param mutable   persistable into which {@code immutable} will be copied.
	 * @return a mutable copy of the given {@link Model} instance.
	 */
	Persistable copyModel(Model immutable, Persistable mutable);

	/**
	 * Create a new mutable {@link Persistable} from an immutable
	 * {@link Model}. The resulting {@link Persistable} should contain
	 * the same values as the given {@link Model}.
	 *
	 * @param immutable with data from which a new {@link Persistable}
	 *                  will be created.
	 * @return a new mutable copy of the given {@link Model} instance.
	 */
	Persistable persistableFrom(Model immutable);

	/**
	 * Method to convert from a mutable {@link Persistable} to an the related
	 * immutable {@link Model}.
	 *
	 * @param mutable {@link Persistable} with data to be used when
	 *                constructing {@link Model}.
	 * @return an immutable {@link Model} instance.
	 */
	Model modelFrom(Persistable mutable);

}