package com.samsung.artikbio.platform.core.wearable.controller;

import com.samsung.artikbio.platform.core.util.Validations;
import com.samsung.artikbio.platform.core.wearable.ble.BLEConnection;
import com.samsung.artikbio.platform.core.wearable.controller.ble.BLEWearableController;

public abstract class AbstractBLEController {

	protected BLEWearableController mWearableController;

	protected BLEConnection mBLEConnection;

	public AbstractBLEController (final BLEConnection bleConnection, final BLEWearableController
			wearableController) {
		Validations.validateNotNull(bleConnection, "bleConnection");
		Validations.validateNotNull(wearableController, "wearableController");

		mBLEConnection = bleConnection;
		mWearableController = wearableController;

	}
}
