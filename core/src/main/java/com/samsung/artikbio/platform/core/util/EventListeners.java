package com.samsung.artikbio.platform.core.util;

import java.util.HashSet;
import java.util.Set;

/**
 * Utility class for holding a collection of unique listeners.
 *
 * @param <T> the type of the listeners to hold in the collection
 */
public class EventListeners<T> implements IEventListeners<T> {


	private final Set<T> mListeners = new HashSet<>();

	/**
	 * Adds a listener to the collection of listeners.
	 *
	 * @param listener the listener to add. If the listener is {@code null} it is not added and
	 *                    this
	 *                 call returns {@code false}
	 *
	 * @return {@code true} if the listener was added or {@code false} if not
	 */
	@Override
	public boolean addListener (
			final T listener) {
		if (listener == null) {
			return false;
		}
		if (isRegistered(listener)) {
			mListeners.remove(listener);
		}
		return mListeners.add(listener);
	}

	/**
	 * Removes a listener from the collection of listeners.
	 *
	 * @param listener the listener to remove. If the listener is {@code null} nothing is removed
	 *                 and this call returns {@code false}
	 *
	 * @return {@code true} if the listener was removed or {@code false} if not
	 */
	@Override
	public boolean removeListener (
			final T listener) {
		if (listener == null) {
			return false;
		}
		return mListeners.remove(listener);
	}

	/**
	 * Removes all listeners in the collection.
	 */
	@Override
	public void removeAllListeners () {
		mListeners.clear();
	}


	/**
	 * Return a copy of the collection of listeners.
	 *
	 * @return the collection of listeners
	 */
	@Override
	public Set<T> getListeners () {
		return new HashSet<>(mListeners);
	}

	/**
	 * Return whether the listener is registered to receive events.
	 *
	 * @param listener the listener to check
	 *
	 * @return {@code true} if the listener is registered or {@code false} if not
	 */
	@Override
	public boolean isRegistered (final T listener) {
		return mListeners.contains(listener);
	}

	/**
	 * Returns whether there are listeners in the collection or not.
	 *
	 * @return {@code true} if there are no listeners in the collection or {@code false} otherwise
	 */
	@Override
	public boolean isEmpty () {
		return mListeners.isEmpty();
	}

}
