package com.samsung.artikbio.platform.core.wearable;

import com.samsung.artikbio.platform.core.util.Validations;

import net.jcip.annotations.Immutable;

import java.io.Serializable;

/**
 * Immutable class containing information about the wearable battery status.
 */
@Immutable
public class WearableBatteryStatus implements Serializable {

	private static final long serialVersionUID = 5351177736465035272L;

	/**
	 * The battery level.
	 */
	private byte batteryLevel;

	/**
	 * Constructor for a WearableBatteryStatus class instance using a battery level.
	 *
	 * @param level is an integer number between 0 and 100 representing battery power percentage
	 *              remaining
	 *
	 * @throws IllegalArgumentException if level is out range
	 */
	public WearableBatteryStatus (final byte level) {
		Validations.validateInRangeInclusive(level, 0, 100, "level");
		this.batteryLevel = level;
	}

	/**
	 * Returns the battery level.
	 *
	 * @return an integer number between 0 and 100 representing battery power percentage remaining
	 */
	public byte getBatteryLevel () {
		return batteryLevel;
	}
}
