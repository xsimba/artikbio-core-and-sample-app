package com.samsung.artikbio.platform.core.wearable.scanner;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.Validations;
import com.samsung.artikbio.platform.core.wearable.scanner.ble.BLEWearableScanner;

import net.jcip.annotations.Immutable;

/**
 * Non-instantiable class that provides functionality for creating a concrete {@link
 * IWearableScanner} instance.
 */
@Immutable
public class WearableScannerFactory {

	/**
	 * Default private constructor (non-instantiable class).
	 */
	private WearableScannerFactory () {
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Returns a {@link IWearableScanner} concrete instance based on the {@link
	 * WearableScannerType}.
	 *
	 * @param wearableScannerType type of wearable scanner to generate
	 *
	 * @return a concrete instance of {@link IWearableScanner}
	 *
	 * @see WearableScannerType
	 * @see IWearableScanner
	 */
	public static IWearableScanner getScanner (
			@NonNull final WearableScannerType wearableScannerType) {
		Validations.validateNotNull(wearableScannerType, "wearableScannerType");
		ContextProvider.getInstance().assertContextAvailable();
		switch (wearableScannerType) {
			case BLE:
				return BLEWearableScanner.getInstance();
			default:
				throw new IllegalArgumentException("Invalid Scanner type");
		}
	}

	/**
	 * Return the default BLE Wearable Scanner instance.
	 *
	 * @return a BLE Wearable Scanner instance
	 */
	public static IWearableScanner getDefaultScanner () {
		ContextProvider.getInstance().assertContextAvailable();
		return BLEWearableScanner.getInstance();
	}
}
