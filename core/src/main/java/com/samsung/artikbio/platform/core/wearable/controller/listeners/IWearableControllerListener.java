package com.samsung.artikbio.platform.core.wearable.controller.listeners;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.wearable.WearableBatteryStatus;
import com.samsung.artikbio.platform.core.wearable.controller.IWearableController;

/**
 * Delivers the status of the connection between the companion and the wearable devices.
 */
public interface IWearableControllerListener {

	/**
	 * Invoked when the connection to the wearable device is ongoing.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 */
	void onConnecting (@NonNull IWearableController wearableController);

	/**
	 * Invoked when the connection to the wearable device is established.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 */
	void onConnected (@NonNull IWearableController wearableController);

	/**
	 * Invoked when disconnection from the wearable device is ongoing.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 */
	void onDisconnecting (@NonNull IWearableController wearableController);

	/**
	 * Invoked when connection to the wearable device is dropped.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 */
	void onDisconnected (@NonNull IWearableController wearableController);

	/**
	 * Invoked when the paired status is changed (either from paired to unpaired or from unpaired
	 * to paired).
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 * @param isPaired           the pairing status after its change
	 */
	void onPairedStatusChanged (@NonNull IWearableController wearableController,
	                            boolean isPaired);

	/**
	 * Invoked when there is a battery status update.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 * @param batteryStatus      the new battery status
	 *
	 * @see WearableBatteryStatus
	 */
	void onBatteryStatusUpdate (@NonNull IWearableController wearableController,
	                            @NonNull WearableBatteryStatus batteryStatus);

	/**
	 * Invoked when there is a failure in the Wearable Controller.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 * @param error              Details of the {@link Error}
	 */
	void onFailure (@NonNull IWearableController wearableController,
	                @NonNull Error error);

	/**
	 * Invoked when subscription to battery status update events is successful.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 */
	void subscribedToBatteryStatusUpdateEvents (@NonNull IWearableController wearableController);

	/**
	 * Invoked when unsubscription from battery status update events is successful.
	 *
	 * @param wearableController The instance of the {@link IWearableController} this callback is
	 *                           registered with
	 *
	 * @since v4
	 */
	void unsubscribedFromBatteryStatusUpdateEvents (@NonNull IWearableController
			                                                wearableController);
}
