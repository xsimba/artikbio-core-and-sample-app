package com.samsung.artikbio.platform.core.wearable.ble.generics;

public interface IConnectable {
	boolean connect (boolean autoConnect, ConnectCallback callback);

	boolean disconnect (DisconnectCallback callback);

	enum States {
		CONNECTING,
		CONNECTED,
		DISCONNECTING,
		DISCONNECTED
	}

	interface ConnectCallback {
		void onConnect (Status status);
	}

	interface DisconnectCallback {
		void onDisconnect (Status status);
	}
}
