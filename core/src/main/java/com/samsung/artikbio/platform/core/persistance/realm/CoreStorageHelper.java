package com.samsung.artikbio.platform.core.persistance.realm;

import com.samsung.artikbio.platform.core.persistance.datastore.CoreModule;

import io.realm.RealmMigration;

/**
 * {@link StorageHelper} implementation for the Core SDK.
 */

public class CoreStorageHelper extends StorageHelper {

	/** A pre-created instance of {@link CoreStorageHelper} for convenience. */
	public static final CoreStorageHelper INSTANCE = new CoreStorageHelper();

	/**
	 * Name to use for Realm database configurations created within
	 * Core SDK.
	 */
	public static final String REALM_NAME = "com-samsung-artikbio-platform-core";

	/** Current schema version. */
	static final long CORE_SCHEMA_VERSION = 1L;

	/** The {@link io.realm.annotations.RealmModule} for Core. */
	private final CoreModule mBaseModule = new CoreModule();

	/** No modules other than {@link #mBaseModule}. */
	private final Object[] modules = new Object[0];

	/** No migration defined. */
	private final RealmMigration migration = new CoreMigration();


	@Override
	protected String getRealmName() {
		return REALM_NAME + REALM_EXT;
	}

	@Override
	protected Object getBaseModule() {
		return mBaseModule;
	}

	@Override
	protected Object[] getModules() {
		return this.modules;
	}

	@Override
	protected long getSchemaVersion() {
		return CORE_SCHEMA_VERSION;
	}

	@Override
	protected RealmMigration getMigration() {
		return this.migration;
	}
}
