package com.samsung.artikbio.platform.core;

/**
 * Marks a public API as being for internal SDK use. This should only be
 * used when Java visibility rules require something to be public but
 * would, ideally, been internal to the project.
 */
public @interface Internal {
}