package com.samsung.artikbio.platform.core.util;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.SecureRandom;

/**
 * Utility class to generate a random installation key and persist it to the local filesystem.
 * The generate key can later be accessed and does not change until the local file is removed or
 * the App is uninstalled
 */
public final class EncryptionKeyUtil {

	private static final String KEY_FILE = "ENCRYPTION_KEY";
	private static byte[] mKey = null;

	/**
	 * Default private constructor (non-instantiable class).
	 */
	private EncryptionKeyUtil () {
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Returns the previously stored key if it exists or tries to generate a new one and persist
	 * it.
	 *
	 * @param context the Application context
	 *
	 * @return a 64 bytes key new or previously stored in the local file system throws
	 * RuntimeException if the file couldn't be created or readCharacteristic
	 */
	public static synchronized byte[] getKey (@NonNull final Context context) throws IOException {
		Validations.validateNotNull(context, "context");
		if (mKey == null) {
			final File installation = new File(context.getFilesDir(), KEY_FILE);
			if (!installation.exists()) {
				writeKeyFile(installation);
			}
			mKey = readKeyFile(installation);
		}
		return mKey;
	}

	private static void writeKeyFile (final File installation) throws IOException {
		final FileOutputStream out = new FileOutputStream(installation);
		final byte[] key = new byte[64];
		(new SecureRandom()).nextBytes(key);
		out.write(key);
		out.close();
	}

	private static byte[] readKeyFile (final File installation) throws IOException {
		final RandomAccessFile file = new RandomAccessFile(installation, "r");
		final byte[] bytes = new byte[(int) file.length()];
		file.readFully(bytes);
		file.close();
		return bytes;
	}
}