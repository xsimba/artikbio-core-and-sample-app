package com.samsung.artikbio.platform.core.wearable.ble.generics;

import java.util.UUID;

public interface IReadCharacteristic {

	boolean readCharacteristic (UUID service, UUID characteristic, ReadCharacteristicCallback
			callback);

	interface ReadCharacteristicCallback {
		void onReadCharacteristic (UUID service, UUID characteristic, byte[] value, Status status);
	}
}