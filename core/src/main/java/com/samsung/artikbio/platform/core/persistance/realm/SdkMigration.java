package com.samsung.artikbio.platform.core.persistance.realm;

import android.support.v4.util.LongSparseArray;

import com.samsung.artikbio.platform.core.Internal;
import com.samsung.artikbio.platform.core.util.Logger;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * {@link RealmMigration} implementation which uses multiple instances of
 * {@link VersionedMigration} to move the schema version in increments
 * rather than in one big jump.
 */
@Internal
public class SdkMigration implements RealmMigration {

	/**
	 * Hold migrations which can be accessed according to the version
	 * the Realm is expected to be in before executing the migration.
	 * The {@code long} key is a {@link VersionedMigration#initialVersion()}.
	 */
	private final LongSparseArray<VersionedMigration> migrations;

	/**
	 * Create a {@link RealmMigration} with an empty group of migration
	 * instances.
	 */
	protected SdkMigration() {
		this.migrations = new LongSparseArray<>(0);
	}

	/**
	 * Create a {@link RealmMigration} with a subset of
	 * {@link VersionedMigration} instances. Each {@link VersionedMigration}
	 * moves the schema version in a fixed increment rather than pushing
	 * all the way in one method.
	 *
	 * @param migrations to use as migration steps.
	 */
	public SdkMigration(final VersionedMigration... migrations) {
		this.migrations = new LongSparseArray<>(migrations.length);
		for (final VersionedMigration versionedMigration : migrations) {
			this.migrations.put(versionedMigration.initialVersion(), versionedMigration);
		}
	}

	/**
	 * Execute a series of {@link RealmMigration}s in an order determined
	 * by the {@code currentVersion} and the version returned by each
	 * successive migration. All {@link RealmMigration}s in the series are
	 * performed within a single transaction.
	 *
	 * @param realm to be migrated.
	 * @param currentVersion of the {@code realm}.
	 * @param requestedVersion the expected new version of {@code realm}'s
	 *        after migration.
	 */
	@Override
	public final void migrate(final DynamicRealm realm, final long currentVersion, final long requestedVersion) {
		Logger.i("Attempting to move schema from v%d to v%d.", currentVersion, requestedVersion);
		final boolean requiresLocalTransaction = !realm.isInTransaction();
		if (requiresLocalTransaction) { realm.beginTransaction(); }
		long version = realm.getVersion();
		VersionedMigration migration = this.migrations.get(version);
		while (migration != null && version < requestedVersion) {
			migration.migrate(realm, version, requestedVersion);
			version = migration.resultingVersion();
			migration = this.migrations.get(version);
		}
		if (version != requestedVersion) {
			if (requiresLocalTransaction) { realm.cancelTransaction(); }
			throw new RealmMigrationNeededException(
					realm.getPath(),
					String.format(
							"Unable to migrate schema from v%d to v%d. Appropriate migrations not found.",
							currentVersion,
							requestedVersion
					)
			);
		}
		if (requiresLocalTransaction) { realm.commitTransaction(); }
		Logger.i("Move schema from v%d to v%d.", currentVersion, realm.getVersion());
	}

	/**
	 * A {@code RealmMigration} with a method to return the version of the
	 * database after the migration is completed. {@code VersionedMigration}
	 * implementations are used as intermediate steps so the resulting
	 * version may not match the version requested in the migrate method.
	 */
	protected interface VersionedMigration extends RealmMigration {
		/**
		 * Return the schema version the database must be in for
		 * {@link #migrate(DynamicRealm, long, long)} to apply.
		 *
		 * @return the expected current schema version before migrate.
		 */
		long initialVersion();

		/**
		 * Return the schema version this migration leaves the database in
		 * after {@link #migrate(DynamicRealm, long, long)} completes.
		 *
		 * @return the schema version after {@code migrate}.
		 */
		long resultingVersion();
	}

	/**
	 * Throw an exception if {@code currentVersion} doesn't match
	 * {@code expectedVersion}.
	 *
	 * @param realm being migrated.
	 * @param currentVersion version when migration is starting.
	 * @param expectedVersion version expected when starting migration.
	 */
	public static void checkStartingVersion(final DynamicRealm realm, final long currentVersion, final long expectedVersion) {
		if (currentVersion != expectedVersion) {
			throw new RealmMigrationNeededException(
					realm.getPath(),
					String.format(
							"Can't migrate from v%d when expecting v%d.",
							currentVersion,
							expectedVersion
					)
			);
		}
	}

}