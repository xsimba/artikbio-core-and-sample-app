package com.samsung.artikbio.platform.core.persistance;

import java.util.SortedSet;

/**
 * Basic CRUD data access definition plus an {@link #exists(Object)},
 * {@link #save(Object)} and {@link #list()} method.
 *
 * @param <Key>   used as an identifier to access a uniquely identified
 *                record.
 * @param <Model> type of data stored by the Dao.
 */
public interface Dao<Key, Model> {

	/**
	 * Saves a new {@code model} instance to the database.
	 *
	 * @param model to be saved.
	 * @return the persisted model.
	 */
	Model create(Model model);

	/**
	 * Gets an instance using its primary key.
	 *
	 * @param id of the instance to retrieve.
	 * @return the instance whose primary key is {@code id} if it exists
	 * {@code null} if a record with primary key of {@code id}
	 * doesn't exist.
	 */
	Model read(Key id);

	/**
	 * Updates an existing record with data from {@code model}.
	 *
	 * @param id    of the instance to update.
	 * @param model with which to update the record whose primary key is
	 *              {@code id}.
	 * @return the updated record.
	 */
	Model update(Key id, Model model);

	/**
	 * Removes a record from the database based on it's primary key.
	 *
	 * @param id of the instance to remove.
	 * @return whether or the record was removed.
	 */
	boolean delete(Key id);

	/**
	 * Determines if a record with identifier of {@code id} exists in database.
	 *
	 * @param id of the record for which this method looks.
	 * @return true if a record with {@code id} exists, false otherwise.
	 */
	boolean exists(Key id);

	/**
	 * Clear all data stored by this {@code Dao} implementation such that
	 * after this method completes the backing data store is empty.
	 */
	void reset();

	/**
	 * Updates a record if one exists, otherwise create a new record.
	 *
	 * @param model to be saved.
	 * @return the saved record.
	 */
	Model save(Model model);

	/**
	 * Set of all records.
	 *
	 * @return the sorted set of all records.
	 */
	SortedSet<Model> list();

}