package com.samsung.artikbio.platform.core.applications;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.util.Constants;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Connection;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Status;

/**
 * This class contains the API for the UART BLE standard service communication.
 */
public class UartBleService extends AbstractBLEProfile {
	/**
	 * Default constructor.
	 *
	 * @param connection the connection to the device.
	 */
	public UartBleService (@NonNull final Connection connection) {
		super(connection);
	}

	public void subscribe (final BLEProfileSubscribeCallback subscribeCallback, final
	IUartReceptionCallback valueChangeCallback) {
		subscribe(Constants.UART_SERVICE_UUID, Constants.UART_RX_UUID,
				(service, characteristic, status) -> {
					if (status == Status.SUCCESS) {
						Logger.i("UART[" + getConnection().getWearableToken().getDeviceAddress() +
								"] subscribe success");
					} else {
						Logger.e("UART[" + getConnection().getWearableToken().getDeviceAddress() +
								"] subscribe failed reason : " + status);
					}
					if (subscribeCallback != null) {
						subscribeCallback.onSubscribe(service, characteristic, status);
					}
				},
				(service, characteristic, value) -> {
					if (valueChangeCallback != null && characteristic.equals(Constants
							.UART_RX_UUID)) {
						valueChangeCallback.onUartValueReported(value, Status.SUCCESS);
					}
				}
		);
	}

	public interface IUartReceptionCallback {
		void onUartValueReported (byte[] data, Status status);
	}
}
