package com.samsung.artikbio.platform.core.persistance;

/**
 * Exception thrown when a {@link Dao} method attempts to access
 * a record which does not exist.
 */
public class NoRecordException extends RuntimeException {

	private static final long serialVersionUID = 6996138496674798611L;

	/**
	 * Creates Exception with message.
	 *
	 * @param message to send with Exception.
	 */
	public NoRecordException(final String message) {
		super(message);
	}

	/**
	 * Creates message for this Exception from {@code clazz} and {@code id}.
	 *
	 * @param clazz we were trying to retrieve.
	 * @param id    of record we were looking for.
	 */
	public NoRecordException(final Class<?> clazz, final Object id) {
		this(clazz.getSimpleName() + " record with id: " + id + " not found.");
	}

}