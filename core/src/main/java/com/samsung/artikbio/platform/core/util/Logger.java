package com.samsung.artikbio.platform.core.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.samsung.artikbio.platform.core.BuildConfig;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import timber.log.Timber;

/**
 * Default Logger implementation.
 */
public final class Logger {

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyy HH:mm:ss.SSS",
			Locale.US);
	private static File mLogFile;
	private static Level mLogLevel;
	private static long mMaxSize = -1;
	private static LogToFileTree mTree;


	/**
	 * Default private constructor (non-instantiable class).
	 */
	private Logger() {
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Log a debug message.
	 *
	 * @param s       describing the debug event to be logged
	 * @param objects associated with the debug event
	 */
	public static void d(final String s, final Object... objects) {
		setTag();
		Timber.d(s, objects);
	}

	private static void setTag () {
		if (mTree != null) {
			mTree.setTag();
		}
	}

	/**
	 * Log a debug exception.
	 *
	 * @param throwable exception thrown upon debug event
	 * @param s         describing the debug exception
	 * @param objects   associated with the debug exception
	 */
	public static void d(final Throwable throwable, final String s, final Object... objects) {
		setTag();
		Timber.d(throwable, s, objects);
	}

	/**
	 * Log an info message.
	 *
	 * @param s       describing the info event to be logged
	 * @param objects associated with the info event
	 */
	public static void i(final String s, final Object... objects) {
		setTag();
		Timber.i(s, objects);
	}

	/**
	 * Log an info exception.
	 *
	 * @param throwable exception thrown upon info event
	 * @param s         describing the info exception
	 * @param objects   associated with the info exception
	 */
	public static void i(final Throwable throwable, final String s, final Object... objects) {
		setTag();
		Timber.i(throwable, s, objects);
	}

	/**
	 * Log a warning message.
	 *
	 * @param s       describing the warning event to be logged
	 * @param objects associated with the warning event
	 */
	public static void w(final String s, final Object... objects) {
		setTag();
		Timber.w(s, objects);
	}

	/**
	 * Log a warning exception.
	 *
	 * @param throwable exception thrown upon warning event
	 * @param s         describing the warning exception
	 * @param objects   associated with the warning exception
	 */
	public static void w(final Throwable throwable, final String s, final Object... objects) {
		setTag();
		Timber.w(throwable, s, objects);
	}

	/**
	 * Log an error message.
	 *
	 * @param s       describing the error event to be logged
	 * @param objects associated with the error event
	 */
	public static void e(final String s, final Object... objects) {
		setTag();
		Timber.e(s, objects);
	}

	/**
	 * Log a verbose message.
	 *
	 * @param s       describing the verbose event to be logged
	 * @param objects associated with the verbose event
	 */
	public static void v(final String s, final Object... objects) {
		setTag();
		Timber.v(s, objects);
	}

	/**
	 * Log a verbose exception.
	 *
	 * @param throwable exception thrown upon verbose event
	 * @param s         describing the verbose exception
	 * @param objects   associated with the verbose exception
	 */
	public static void v(final Throwable throwable, final String s, final Object... objects) {
		setTag();
		Timber.v(throwable, s, objects);
	}

	/**
	 * Log a terrible failure message.
	 *
	 * @param s       describing the verbose assert to be logged
	 * @param objects associated with the assert event
	 */
	public static void wtf(final String s, final Object... objects) {
		setTag();
		Timber.wtf(s, objects);
	}

	/**
	 * Log a terrible failure exception.
	 *
	 * @param throwable exception thrown upon assert event
	 * @param s         describing the assert exception
	 * @param objects   associated with the assert exception
	 */
	public static void wtf(final Throwable throwable, final String s, final Object... objects) {
		setTag();
		Timber.wtf(throwable, s, objects);
	}

	/**
	 * Constructor for Logger class, specifying the log file and log level.
	 *
	 * @param loggerFile file to log into
	 * @throws IOException if the loggerFile cannot be created
	 */
	public static void init(@NonNull final File loggerFile)
			throws IOException {
		init(loggerFile, Level.VERBOSE);
	}

	/**
	 * Constructor for Logger class, specifying the log file and log level.
	 *
	 * @param loggerFile file to log into
	 * @param level      log levels to writeCharacteristic to the file
	 * @throws IOException if the loggerFile cannot be created
	 */
	public static void init(@NonNull final File loggerFile, final Logger.Level level)
			throws IOException {
		Validations.validateNotNull(loggerFile, "loggerFile");
		if (!loggerFile.exists()) {
			try {
				final boolean isFileCreated = loggerFile.createNewFile();
				final String fileCreated = isFileCreated ? "file successfully created" : "";
				Validations.validateNotEmpty(fileCreated, "fileCreated");
			} catch (IOException ex) {
				throw new IOException("Problem creating file (" + loggerFile + ") in Logger init.",
						ex);
			}

		}
		final String fileCheck = (loggerFile.exists() && loggerFile.canWrite())
				? "file checks out" : "";
		Validations.validateNotEmpty(fileCheck, "fileCheck");

		init();
		mLogFile = loggerFile;
		mLogLevel = level;
		validateLogFile(loggerFile);
	}

	/**
	 * Initialize the Logger. Needs to be invoked before using any other method in this class.
	 */
	public static void init () {
		mLogLevel = Level.VERBOSE;
		mMaxSize = -1;
		if (BuildConfig.DEBUG) {
			if (mTree == null) {
				mTree = new LogToFileTree();
			}
			Timber.uprootAll();
			Timber.plant(mTree);
		}
	}

	private static void validateLogFile (final File logFile) {
		//Check if the logFile is null
		Validations.validateNotNull(logFile, "Log file");

		//Check if the logFile exists
		if (!logFile.exists()) {
			try {
				boolean create = logFile.createNewFile();
				if (!create) {
					//Check if the failure is because logFile does not represents a File(i.e a
					// directory maybe)
					if (!logFile.isFile()) {
						throw new IllegalStateException("The log file does not represent a File");
					} else if (!logFile.exists()) {
						//Check if the failure is because logFile already exists(now)
						throw new IllegalStateException("Log file does not exist");
					}
				}
			} catch (IOException e) {
				e(e, "Create log file");
				throw new IllegalStateException("Log file cannot be created");
			}
		}

		//Check if the logFile represents a File(not a directory)
		if (!logFile.isFile()) {
			throw new IllegalStateException("The log file does not represent a File");
		}

		//Check if the logFile is writable
		if (!logFile.canWrite()) {
			throw new IllegalStateException("Log file is not writable in this context");
		}
	}

	/**
	 * Log an error exception.
	 *
	 * @param throwable exception thrown upon error event
	 * @param s         describing the error exception
	 * @param objects   associated with the error exception
	 */
	public static void e (final Throwable throwable, final String s, final Object... objects) {
		setTag();
		Timber.e(throwable, s, objects);
	}

	/**
	 * Constructor for Logger class, specifying the log file and log level.
	 *
	 * @param level log levels to writeCharacteristic to the file
	 *
	 * @throws IOException if the loggerFile cannot be created
	 */
	public static void init (@NonNull final Logger.Level level)
			throws IOException {
		init();
		mLogLevel = level;
	}

	/**
	 * Constructor for Logger class, specifying the log file, log level and the maximum file size
	 * of
	 * of the log file.
	 *
	 * @param loggerFile file to log into
	 * @param level      log levels to writeCharacteristic to the file
	 * @param maxSize    maximum size of the file in bytes
	 * @throws IOException if the loggerFile cannot be created
	 */
	public static void init(@NonNull final File loggerFile, final Logger.Level level,
	                        final long maxSize) throws IOException {
		Validations.validateNotNull(loggerFile, "loggerFile");
		if (!loggerFile.exists()) {
			try {
				final boolean isFileCreated = loggerFile.createNewFile();
				final String fileCreated = isFileCreated ? "file successfully created" : "";
				Validations.validateNotEmpty(fileCreated, "fileCreated");
			} catch (IOException ioException) {
				throw new IOException("Problem creating file (" + loggerFile + ") in Logger init.",
						ioException);
			}

		}
		final String fileCheck = (loggerFile.exists() && loggerFile.canWrite())
				? "file checks out" : "";
		Validations.validateNotEmpty(fileCheck, "fileCheck");
		init();
		mLogFile = loggerFile;
		Logger.mMaxSize = maxSize;
		mLogLevel = level;
		validateLogFile(loggerFile);
	}

	/**
	 * Erases the content of the log file.
	 *
	 * @return true if there is a log file and it is cleaned, false otherwise.
	 */
	public static boolean cleanLogFile() {
		try {
			PrintWriter writer = new PrintWriter(getLogFileOrDefault());
			writer.print("");
			writer.close();
			return true;
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			return false;
		} catch (NullPointerException npe) {
			npe.printStackTrace();
			return false;
		}
	}

	private static File getLogFileOrDefault () {
		if (mLogFile != null) {
			return mLogFile;
		} else {
			Context context = ContextProvider.getInstance().getApplicationContext();
			if (context == null) {
				return null;
			}

			File internalStorageDir = context.getFilesDir();
			mLogFile = new File(internalStorageDir, "logs.txt");
			if (mLogFile.exists()) {
				return mLogFile;
			} else {
				try {
					mLogFile.createNewFile();
					return mLogFile;
				} catch (IOException e) {
					e(e, "_getLogFile");
					return null;
				}
			}
		}
	}

	private static void writeToLogFile(final int priority, final String tag, final String message,
	                                   final Throwable t) {
		File logFile = getLogFileOrDefault();
		if (logFile != null) {
			if (mMaxSize > 0 && logFile.length() >= mMaxSize) {
				removeOldLogs();
				addNewLogs(getFormattedLogText(priority, tag, message, t));
			} else {
				addNewLogs(getFormattedLogText(priority, tag, message, t));
			}
		}
	}

	private static void addNewLogs(final String log) {
		BufferedWriter bufferedWriter = null;
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(mLogFile, true));
			bufferedWriter.append(log);
			bufferedWriter.newLine();
			bufferedWriter.close();
		} catch (IOException e) {
			e(e, "addNewLogs: %s", log);
		}
	}

	private static void removeOldLogs() {
		try {
			RandomAccessFile raf = new RandomAccessFile(getLogFile(), "rw");
			long pos = 0;
			while (raf.readLine() != null) {
				byte[] notDeleted = new byte[(int) (raf.length() - raf.getFilePointer())];
				raf.read(notDeleted);
				raf.getChannel().truncate(pos);
				raf.seek(pos);
				raf.write(notDeleted);
				pos += raf.getFilePointer();
			}
			raf.close();
		} catch (FileNotFoundException fileNotFoundException) {
			e(fileNotFoundException, "removeOldLogs, " + fileNotFoundException.getMessage());
		} catch (IOException ioException) {
			e(ioException, "removeOldLogs, " + ioException.getMessage());
		}
	}

	/**
	 * Retrieves the log file. One of the init() methods must be called before attempting to
	 * retrieve
	 * the log file.
	 *
	 * @return File
	 */
	public static File getLogFile() {
		return mLogFile;
	}

	private static String getFormattedLogText(final int priority, final String tag,
	                                          final String message,
	                                          final Throwable t) {
		StringBuilder sb = new StringBuilder("");
		sb.append(DATE_FORMAT.format(Calendar.getInstance().getTime()));
		sb.append("  ");
		sb.append(getLogType(priority));
		sb.append("/");
		sb.append(tag);
		sb.append(": ");
		sb.append(message);
		return sb.toString();
	}

	private static String getLogType(final int priority) {
		switch (priority) {
			case Log.ASSERT:
				return "ASSERT";
			case Log.DEBUG:
				return "DEBUG";
			case Log.ERROR:
				return "ERROR";
			case Log.INFO:
				return "INFO";
			case Log.VERBOSE:
				return "VERBOSE";
			case Log.WARN:
				return "WARN";
			default:
				return "DEBUG";
		}
	}

	/**
	 * Set a one-off tag for the next logging operation.
	 *
	 * @param tag the tag to use in the next logging operation
	 */
	public static void setTag(final String tag) {
		if (mTree != null) {
			mTree.setTag(tag);
		}
	}

	/**
	 * The logging levels.
	 */
	public enum Level {
		/**
		 * Verbose Level.
		 */
		VERBOSE,
		/**
		 * Debug Level.
		 */
		DEBUG,
		/**
		 * Info Level.
		 */
		INFO,
		/**
		 * Warning Level.
		 */
		WARN,
		/**
		 * Error Level.
		 */
		ERROR
	}

	/**
	 * Tree which logs to a file(default: logs.txt).
	 */
	private static class LogToFileTree extends Timber.DebugTree {

		private static final int CALL_STACK_INDEX = 3;

		private static String explicitTag;

		@Override
		protected void log (final int priority, final String tag, final String message,
		                    final Throwable t) {
			super.log(priority, tag, message, t);
			if (priority >= (mLogLevel.ordinal() + Log.VERBOSE)) {
				//writeToLogFile(priority, tag, message, t);
			}
		}

		public synchronized void setTag () {
			if (explicitTag != null) {
				Timber.tag(explicitTag);
				explicitTag = null;
			} else {
				StackTraceElement[] stackTrace = new Throwable().getStackTrace();
				if (stackTrace.length > CALL_STACK_INDEX) {
					Timber.tag(super.createStackElementTag(stackTrace[CALL_STACK_INDEX]));
				}
			}
		}

		public synchronized void setTag (final String tag) {
			explicitTag = tag;
		}
	}
}