package com.samsung.artikbio.platform.core.event;

public interface Event {

	String getEventId ();
}
