package com.samsung.artikbio.platform.core.event;

import android.support.annotation.NonNull;

import java.util.Collection;

/**
 * Defines a service which can be registered with {@link EventManager}
 * to be retrieved later by name or the event types it can process.
 */
public interface EventHandler {
	/**
	 * Uniquely identifying name of the service implementation. This name
	 * should be unique across all implementations so it is recommended
	 * to prefix the service name with the application or library package
	 * name. Only one instance may be registered with a given name at a
	 * time.
	 *
	 * @return the name of the service.
	 */
	String getName ();

	/**
	 * The event types this service implementation can process via
	 * {@link #handle(Event)}. The {@code Collection} may be empty (in
	 * which case it will receive no events) but it must not be
	 * {@code null}.
	 *
	 * @return the types of {@link Event} instances this service can process.
	 */
	@NonNull
	Collection<Class<? extends Event>> getEventTypes ();

	/**
	 * Process the given {@link Event}. If an error occurs during processing
	 * an exception may be thrown and {@link EventManager} will post an
	 * {@link EventHandlerErrorEvent}. Alternatively implementations handle
	 * errors internally or post different error {@link Event} instances to
	 * {@link EventManager}.
	 *
	 * @param event to be processed.
	 */
	void handle (Event event);
}
