package com.samsung.artikbio.platform.core.util;

import java.util.Locale;

/**
 * Utility class to validate parameters.
 */
public final class Validations {

	/**
	 * Default private constructor (non-instantiable class).
	 */
	private Validations () {
		throw new UnsupportedOperationException("This class is non-instantiable");
	}


	/**
	 * Throw an IllegalArgumentException with a message formatted with name, if the object is
	 * {@code
	 * null}.
	 *
	 * @param object The object to check for {@code null}
	 * @param name   The name of the object to use in the error message
	 *
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void validateNotNull (final Object object, final String name) {
		if (object == null) {
			throw new IllegalArgumentException(String.format("%s can't be null", name));
		}
	}

	/**
	 * Throw an IllegalArgumentException with a message formatted with {@code name} if {@code
	 * value}
	 * is {@code null} or empty.
	 *
	 * @param value to check if it's {@code null} or empty.
	 * @param name  the name of the value to use in the error message
	 *
	 * @throws IllegalArgumentException if the value is {@code null} or empty.
	 */
	public static void validateNotEmpty (final String value, final String name) {
		if (value == null || value.length() == 0) {
			throw new IllegalArgumentException(
					String.format("%s can't be %s", name, value == null ? "null" : "empty"));
		}
	}

	/**
	 * Throw an IllegalArgumentException with a with a message formatted with {@code name} if
	 * {@code
	 * value} is not in the range [min, max] inclusive.
	 *
	 * @param value    value to check if it's in range
	 * @param minValue minimum value for the range (inclusive)
	 * @param maxValue maximum value for the range (inclusive)
	 * @param name     the name of the value to use in the error message
	 *
	 * @throws IllegalArgumentException if the value is out of range
	 */
	public static void validateInRangeInclusive (final int value, final int minValue,
	                                             final int maxValue, final String name) {
		if (value > maxValue || value < minValue) {
			throw new IllegalArgumentException(String.format(Locale.US,
					"%s (%d) is not in range [%d, %d] (inclusive)", name, value, minValue,
					maxValue));
		}
	}


}