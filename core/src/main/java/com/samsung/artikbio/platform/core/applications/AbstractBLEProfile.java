package com.samsung.artikbio.platform.core.applications;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.samsung.artikbio.platform.core.wearable.ble.generics.Connection;
import com.samsung.artikbio.platform.core.wearable.ble.generics.IConnectable;
import com.samsung.artikbio.platform.core.wearable.ble.generics.IConnectionStateChangeListener;
import com.samsung.artikbio.platform.core.wearable.ble.generics.IReadCharacteristic;
import com.samsung.artikbio.platform.core.wearable.ble.generics.ISubscribeCharacteristic;
import com.samsung.artikbio.platform.core.wearable.ble.generics.IWriteCharacteristic;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Status;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public abstract class AbstractBLEProfile implements IConnectionStateChangeListener {

	// This boolean is used to know if the profile is subscribe or not.
	private final Set<Pair<UUID, UUID>> mSubscribedSet;
	private Connection mConnection;

	/**
	 * Default constructor.
	 *
	 * @param connection the connection to the device.
	 */
	public AbstractBLEProfile (@NonNull Connection connection) {
		mConnection = connection;
		mSubscribedSet = new HashSet<>();
		mConnection.addConnectionStateChangeListener(this);
	}

	@Override
	public void onConnectionStateChange (IConnectable.States oldState, IConnectable.States
			newState) {
		if (newState == IConnectable.States.DISCONNECTED) {
			mSubscribedSet.clear();
			mConnection.removeConnectionStateChangeListener(this);
		}
	}

	/**
	 * Subscribe method.
	 * To get notify for subscribe status change or value change
	 */
	protected void subscribe (final UUID service, final UUID characteristic, final
	BLEProfileSubscribeCallback subscribeCallback, final BLEProfileValueChangeCallback
			                          valueChangeCallback) {
		if (!IConnectable.States.CONNECTED.equals(mConnection.getConnectionState())) {
			if (subscribeCallback != null) {
				subscribeCallback.onSubscribe(service, characteristic, Status.NOT_CONNECTED);
			}
		} else if (isSubscribed(service, characteristic)) {
			if (subscribeCallback != null) {
				subscribeCallback.onSubscribe(service, characteristic, Status.NOT_READY);
			}
		} else {
			mConnection.subscribeToCharacteristic(service, characteristic,
					ISubscribeCharacteristic.SubscribeToCharacteristicType.NOTIFICATION, new
							ISubscribeCharacteristic.SubscribeToCharacteristicCallback() {

								@Override
								public void onSubscribeToCharacteristic (final UUID service, final
								UUID characteristic, final ISubscribeCharacteristic
										.SubscribeToCharacteristicType subscribeType, final Status
										                                         status) {
									if (status == Status.SUCCESS) {
										mSubscribedSet.add(new Pair<>(service, characteristic));
									}
									if (subscribeCallback != null) {
										subscribeCallback.onSubscribe(service, characteristic,
												status);
									}
								}

								@Override
								public void onValueChangeOfCharacteristic (final UUID service,
								                                           final UUID
										                                           characteristic,
								                                           final byte[] value) {
									if (valueChangeCallback != null) {
										valueChangeCallback.onValueChange(service, characteristic,
												value);
									}
								}
							});
		}
	}

	/**
	 * Getter to know if this profile is subscribe.
	 *
	 * @return true if the profile is subscribe, false otherwise.
	 */
	public boolean isSubscribed (UUID service, UUID characteristic) {
		return mSubscribedSet.contains(new Pair<>(service, characteristic));
	}

	/**
	 * Unsubscribe method.
	 */
	public void unsubscribe (final BLEProfileUnsubscribeCallback unsubscribeCallback) {
		if (mSubscribedSet.size() == 0) {
			unsubscribe(null, null, unsubscribeCallback);
		} else {
			for (Pair<UUID, UUID> subscribedKey :
					mSubscribedSet) {
				unsubscribe(subscribedKey.first, subscribedKey.second, unsubscribeCallback);
			}
		}
	}

	/**
	 * Unsubscribe method.
	 */
	public void unsubscribe (final UUID service, final UUID characteristic, final
	BLEProfileUnsubscribeCallback unsubscribeCallback) {
		if (!IConnectable.States.CONNECTED.equals(mConnection.getConnectionState())) {
			if (unsubscribeCallback != null) {
				unsubscribeCallback.onUnsubscribe(service, characteristic, Status.NOT_CONNECTED);
			}
		} else if (!isSubscribed(service, characteristic)) {
			if (unsubscribeCallback != null) {
				unsubscribeCallback.onUnsubscribe(service, characteristic, Status.NOT_READY);
			}
		} else {
			mConnection.unsubscribeToCharacteristic(service, characteristic, status -> {
				if (status == Status.SUCCESS) {
					mSubscribedSet.remove(new Pair<>(service, characteristic));
				}
				if (unsubscribeCallback != null) {
					unsubscribeCallback.onUnsubscribe(service, characteristic, status);
				}
			});
		}
	}

	/**
	 * Read method
	 *
	 * @param service
	 * @param characteristic
	 * @param callback
	 */
	public void read (UUID service, UUID characteristic, IReadCharacteristic
			.ReadCharacteristicCallback callback) {
		if (!IConnectable.States.CONNECTED.equals(getConnection().getConnectionState())) {
			callback.onReadCharacteristic(service, characteristic, new byte[] {}, Status
					.NOT_CONNECTED);
		} else {
			mConnection.readCharacteristic(service, characteristic, callback);
		}
	}

	/**
	 * Getter of the connection.
	 *
	 * @return an instance of the connection.
	 */
	protected Connection getConnection () {
		return mConnection;
	}

	/**
	 * Write Method
	 *
	 * @param service
	 * @param characteristic
	 * @param data
	 * @param callback
	 */
	public void write (UUID service, UUID characteristic, byte[] data, IWriteCharacteristic
			.WriteCharacteristicCallback callback) {
		if (!IConnectable.States.CONNECTED.equals(getConnection().getConnectionState())) {
			callback.onWriteCharacteristic(service, characteristic, new byte[] {}, Status
					.NOT_CONNECTED);
		} else {
			mConnection.writeCharacteristic(service, characteristic, data, callback);
		}
	}

	public interface BLEProfileSubscribeCallback {
		/**
		 * This method is called when the subscription status change.
		 *
		 * @param service        the service UUID of the subscription.
		 * @param characteristic the characteristic of the subscription.
		 * @param status         the status of the packet who change the subscribe state.
		 */
		void onSubscribe (UUID service, UUID characteristic, Status status);
	}

	public interface BLEProfileUnsubscribeCallback {
		/**
		 * This method is called when the subscription status change.
		 *
		 * @param service        the service UUID of the subscription.
		 * @param characteristic the characteristic of the subscription.
		 * @param status         the status of the packet who change the subscribe state.
		 */
		void onUnsubscribe (UUID service, UUID characteristic, Status status);
	}

	public interface BLEProfileValueChangeCallback {
		/**
		 * This method is called when the subscribed value change.
		 *
		 * @param service        the service UUID of the subscription.
		 * @param characteristic the characteristic of the subscription.
		 * @param value          the value.
		 */
		void onValueChange (UUID service, UUID characteristic, byte[] value);
	}
}
