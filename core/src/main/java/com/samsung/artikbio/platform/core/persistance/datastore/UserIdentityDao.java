package com.samsung.artikbio.platform.core.persistance.datastore;

import android.content.Context;

import com.samsung.artikbio.platform.core.persistance.model.UserIdentity;
import com.samsung.artikbio.platform.core.persistance.realm.UUIDRealmDatabaseDao;

import java.util.UUID;

import io.realm.RealmConfiguration;

/**
 * A {@link UUIDRealmDatabaseDao} which is responsible for saving {@link UserIdentity} data.
 */
public class UserIdentityDao extends UUIDRealmDatabaseDao<UserIdentity, PersistableUserIdentity> {

	private static final String USER_ID = "userId";

	/**
	 * Creates a new instance using the given {@link Context} to create
	 * a {@link RealmConfiguration}.
	 *
	 * @param context to use when accessing data.
	 */
	public UserIdentityDao(final Context context) {
		this(LocalDataStore.getConfiguration(context));
	}

	/**
	 * Creates a new instance using the provided {@link RealmConfiguration}.
	 *
	 * @param configuration to pass along to {@link UUIDRealmDatabaseDao}.
	 */
	public UserIdentityDao(final RealmConfiguration configuration) {
		super(configuration, UserIdentity.class, PersistableUserIdentity.class);
	}

	@Override
	public PersistableUserIdentity copyModel(final UserIdentity model,
	                                         final PersistableUserIdentity record) {
		if (record.getId() == null) {
			record.setId(model.getId() == null ? null : model.getId().toString());
		}
		record.setUserId(model.getUserId());
		record.setFirstName(model.getFirstName());
		record.setLastName(model.getLastName());
		record.setEmail(model.getEmail());
		return record;
	}

	@Override
	public UserIdentity modelFrom(final PersistableUserIdentity record) {
		return new UserIdentity(
				UUID.fromString(record.getId()),
				record.getUserId(),
				record.getFirstName(),
				record.getLastName(),
				record.getEmail()
		);
	}

	/**
	 * Return the {@code UUID} of the {@link UserIdentity} object that matches the provided
	 * {@code userId} or null if there's no match.
	 *
	 * @param userId to look for
	 * @return the {@code UUID} of the {@link UserIdentity} object that matches the provided
	 * {@code userId} or null if there's no match.
	 */
	public UUID getUUIDByUserId(final String userId) {
		UUID result = null;
		PersistableUserIdentity persistableUserIdentity = query().equalTo(USER_ID, userId)
				.findFirst();
		if (persistableUserIdentity != null) {
			result = UUID.fromString(persistableUserIdentity.getId());
		}
		return result;
	}

}