package com.samsung.artikbio.platform.core.util;

import android.content.Intent;
import android.os.Parcelable;

import com.samsung.artikbio.platform.core.persistance.DBAction;

import java.io.Serializable;
import java.util.UUID;

public class Constants {

	public static final UUID CLIENT_CHARACTERISTIC_CONFIG_UUID =
			UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
	public static final UUID DEVICE_INFO_SERVICE_UUID =
			UUID.fromString("0000180A-0000-1000-8000-00805f9b34fb");
	public static final UUID BATTERY_SERVICE_UUID =
			UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
	public static final UUID BATTERY_LEVEL_UUID =
			UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
	public static final UUID MANUFACTURER_NAME_UUID =
			UUID.fromString("00002A29-0000-1000-8000-00805f9b34fb");
	public static final UUID MODEL_NUMBER_STRING_UUID =
			UUID.fromString("00002A24-0000-1000-8000-00805f9b34fb");
	public static final UUID SERIAL_NUMBER_UUID =
			UUID.fromString("00002A25-0000-1000-8000-00805f9b34fb");
	public static final UUID FIRMWARE_REV_UUID =
			UUID.fromString("00002A26-0000-1000-8000-00805f9b34fb");
	public static final UUID SOFTWARE_REV_UUID =
			UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb");
	public static final UUID HARDWARE_REV_UUID =
			UUID.fromString("00002a27-0000-1000-8000-00805f9b34fb");

	// TODO: 5/18/17 Add other UUIDs
	public static final UUID UART_SERVICE_UUID =
			UUID.fromString("4261b848-7209-4076-837a-fd0aed7a8731");
	public static final UUID UART_RX_UUID =
			UUID.fromString("4261b848-7209-4076-837a-fd0aed7a8732");

	/**
	 * Defines the keys used as {@link Intent} extras.
	 */
	public interface CoreExtras {
		/**
		 * Included in the {@link Intent} broadcasts from
		 * {@link DBAction}, as a {@link Serializable}, to identify
		 * the record affected.
		 */
		String EXTRA_DATABASE_ID = DBAction.class.getName() + ".ID";
		/**
		 * Optionally included in {@link Intent} broadcasts from
		 * {@link DBAction}, as a {@link Parcelable}, to hold the
		 * affected record if the record is {@link Parcelable}.
		 */
		String EXTRA_DATABASE_RECORD = DBAction.class.getName() + ".RECORD";
	}
}
