package com.samsung.artikbio.platform.core.wearable.controller.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.Internal;
import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.event.WearableEvent;
import com.samsung.artikbio.platform.core.persistance.datastore.LocalDataStore;
import com.samsung.artikbio.platform.core.persistance.model.WearableIdentity;
import com.samsung.artikbio.platform.core.util.Constants;
import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.EventListeners;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.util.Validations;
import com.samsung.artikbio.platform.core.wearable.WearableBatteryStatus;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.ble.BLEConnection;
import com.samsung.artikbio.platform.core.wearable.ble.generics.IConnectable;
import com.samsung.artikbio.platform.core.wearable.ble.generics.IConnectionStateChangeListener;
import com.samsung.artikbio.platform.core.wearable.ble.generics.ISubscribeCharacteristic;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Status;
import com.samsung.artikbio.platform.core.wearable.controller.IUartController;
import com.samsung.artikbio.platform.core.wearable.controller.IWearableController;
import com.samsung.artikbio.platform.core.wearable.controller.listeners.IWearableControllerListener;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * BLE implementation of {@link IWearableController}.
 */
@Internal
public final class BLEWearableController implements IWearableController {

	private final WearableToken mWearableToken;
	private final BluetoothDevice mBluetoothDevice;
	private final Map<UUID, String> mCharacteristics = new ConcurrentHashMap<>();
	private final Handler mHandler;
	private final BLEConnection mBLEConnection;
	private final BluetoothAdapter mBluetoothAdapter;
	private final PairingBroadcastReceiver mPairingReceiver;
	private final ConnectionStateChangeListener mConnectionStateChangeListener;
	private final EventListeners<IWearableControllerListener> mListeners = new EventListeners<>();
	private WearableIdentity mWearableIdentity;
	private boolean mIsConnected;
	private ConnectionPriority mCurrentConnectionPriority;
	private UUID[] deviceInfoUUIDs = {
			Constants.HARDWARE_REV_UUID,
			Constants.SOFTWARE_REV_UUID,
			Constants.FIRMWARE_REV_UUID,
			Constants.SERIAL_NUMBER_UUID,
			Constants.MODEL_NUMBER_STRING_UUID,
			Constants.MANUFACTURER_NAME_UUID
	};

	private BLEUartController mBLEUartController;

	public BLEWearableController (@NonNull final WearableToken token,
	                              @NonNull final IWearableControllerListener listener,
	                              @NonNull final Handler handler,
	                              @NonNull final BluetoothDevice bluetoothDevice,
	                              @NonNull final BluetoothAdapter bluetoothAdapter,
	                              @NonNull final BLEConnection bleConnection) {
		Validations.validateNotNull(token, "token");
		Validations.validateNotNull(handler, "handler");
		Validations.validateNotNull(bluetoothDevice, "bluetoothDevice");
		Validations.validateNotNull(bluetoothAdapter, "bluetoothAdapter");
		Validations.validateNotNull(bleConnection, "bleConnection");

		addWearableControllerListener(listener);
		mWearableToken = token;
		mHandler = handler;
		mBluetoothDevice = bluetoothDevice;
		mBluetoothAdapter = bluetoothAdapter;
		mIsConnected = false;
		mBLEConnection = bleConnection;
		mCurrentConnectionPriority = ConnectionPriority.BALANCED; // default

		mBLEUartController = new BLEUartController(mBLEConnection, this);

		mConnectionStateChangeListener = new ConnectionStateChangeListener(BLEWearableController
				.this);
		mBLEConnection.addConnectionStateChangeListener(mConnectionStateChangeListener);
		mPairingReceiver = new PairingBroadcastReceiver(mBluetoothDevice, BLEWearableController
				.this);
		ContextProvider.getInstance().getApplicationContext().registerReceiver(mPairingReceiver,
				new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
	}

	public void dispose () {
		try {
			ContextProvider.getInstance().getApplicationContext().unregisterReceiver
					(mPairingReceiver);
		} catch (IllegalStateException e) {
			Logger.e("IllegalStateException in unregister receiver, did you call it twice?");
		}
		mBLEConnection.removeConnectionStateChangeListener(mConnectionStateChangeListener);
	}

	public boolean requestPriorityConnection (final ConnectionPriority priority) {
		if (isConnected()) {
			switch (priority) {
				case HIGH_PRIORITY:
					mCurrentConnectionPriority = ConnectionPriority.HIGH_PRIORITY;
					return mBLEConnection.requestHighConnectionPriority();
				case BALANCED:
					mCurrentConnectionPriority = ConnectionPriority.BALANCED;
					return mBLEConnection.requestBalancedConnectionPriority();
				case LOW_POWER:
					mCurrentConnectionPriority = ConnectionPriority.LOW_POWER;
					return mBLEConnection.requestLowPowerConnectionPirority();
				default:
					Logger.w("Unknown Connection Priority requested");
			}
		}
		return false;
	}

	public ConnectionPriority getCurrentConnectionPriority () {
		return mCurrentConnectionPriority;
	}

	@Override
	public WearableToken getWearableToken () {
		return mWearableToken;
	}

	@Override
	public synchronized void connect () {
		connect(true);
	}

	@Override
	public synchronized void connect (final boolean autoconnect) {
		if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
			Logger.e("Connect Error : Bluetooth is not enabled (%d)", Error.BLE_ERROR_BT_DISABLED);
			notifyErrorInMainThread(new Error(Error.BLE_ERROR_BT_DISABLED), this);
			return;
		}

		if (isConnected()) {
			Logger.e("Connect Error: Device already connected (%d", Error
					.BLE_ERROR_DEVICE_ALREADY_CONNECTED);
			notifyErrorInMainThread(new Error(Error.BLE_ERROR_DEVICE_ALREADY_CONNECTED), this);
			return;
		}

		if (mBluetoothDevice != null) {
			mBLEConnection.connect(autoconnect, status -> {
				if (status == Status.ERROR) {
					Logger.e("BLEConnection->Connection Failed");
					notifyErrorInMainThread(new Error(Error.BLE_ERROR_CONNECTION_FAILED),
							BLEWearableController.this);
				} else if (status == Status.SUCCESS) {
					Logger.d("BLEConnection->Connection Success");
				}
			});
		} else {
			Logger.e("Connect Error : Invalid token (%d)", Error.BLE_ERROR_INVALID_WEARABLE_TOKEN);
			notifyErrorInMainThread(new Error(Error.BLE_ERROR_INVALID_WEARABLE_TOKEN), this);
		}
	}

	@Override
	public synchronized void getBatteryStatus () {
		if (!isConnected()) {
			notifyErrorInMainThread(new Error(Error.BLE_ERROR_NOT_CONNECTED), this);
			return;
		}
		readBatteryLevel();
	}

	@Override
	public void subscribeToBatteryStatusUpdateEvents () {
		mBLEConnection.subscribeToCharacteristic(Constants.BATTERY_SERVICE_UUID, Constants
				.BATTERY_LEVEL_UUID, ISubscribeCharacteristic.SubscribeToCharacteristicType
				.NOTIFICATION, new ISubscribeCharacteristic.SubscribeToCharacteristicCallback() {


			@Override
			public void onSubscribeToCharacteristic (final UUID service, final UUID
					characteristic, final ISubscribeCharacteristic.SubscribeToCharacteristicType
					                                         subscribeType, final Status status) {
				if (status == Status.SUCCESS) {
					notifyEventInMainThread(Event.SUBSCRIBED_TO_BATTERY_STATUS_UPDATE_EVENTS,
							BLEWearableController.this);
				} else {
					notifyErrorInMainThread(new Error(Error.SUBSCRIBE_FAILED),
							BLEWearableController.this);
				}
			}

			@Override
			public void onValueChangeOfCharacteristic (final UUID service, final UUID
					characteristic, final byte[] bytes) {
				if (characteristic.equals(Constants.BATTERY_LEVEL_UUID)) {
					mCharacteristics.put(characteristic, Byte.toString(bytes[0]));
					notifyBatteryStatusInMainThread(new WearableBatteryStatus(getBatteryLevel()),
							BLEWearableController.this);
				}
			}
		});
	}

	@Override
	public void unsubscribeFromBatteryStatusUpdateEvents () {
		mBLEConnection.unsubscribeToCharacteristic(Constants.BATTERY_SERVICE_UUID, Constants
				.BATTERY_LEVEL_UUID, status -> {
			if (status == Status.SUCCESS) {
				notifyEventInMainThread(Event.UNSUBSCRIBED_FROM_BATTERY_STATUS_UPDATE_EVENTS,
						BLEWearableController.this);
			} else {
				notifyErrorInMainThread(new Error(Error.SUBSCRIBE_FAILED),
						BLEWearableController.this);
			}
		});
	}

	@Override
	public WearableIdentity getWearableIdentity () {
		return mWearableIdentity;
	}

	@Override
	public boolean disconnect () {
		return mBLEConnection.disconnect(status -> {
			if (status != Status.SUCCESS) {
				notifyErrorInMainThread(new Error(Error.DISCONNECTED), BLEWearableController
						.this);
			}
		});
	}

	@Override
	public void pair () {
		if (isPaired()) {
			return;
		}
		mBLEConnection.pair(status -> {
			if (Status.SUCCESS == status) {
				notifyEventInMainThread(Event.PAIRED, BLEWearableController.this);
			}
		});
	}

	@Override
	public void unpair () {
		if (!isPaired()) {
			return;
		}
		mBLEConnection.unpair(status -> {
			if (Status.SUCCESS == status) {
				notifyEventInMainThread(Event.UNPAIRED, BLEWearableController.this);
			}
		});
	}

	@Override
	public boolean isPaired () {
		return mBluetoothDevice.getBondState() == BluetoothDevice.BOND_BONDED;
	}

	@Override
	public synchronized boolean isConnected () {
		return mIsConnected && mBLEConnection.getConnectionState() == IConnectable.States
				.CONNECTED;
	}

	@Override
	public boolean addWearableControllerListener (final IWearableControllerListener listener) {
		return mListeners.addListener(listener);
	}

	@Override
	public boolean removeWearableControllerListener (final IWearableControllerListener listener) {
		return mListeners.removeListener(listener);
	}

	@Override
	public void removeAllWearableControllerListeners () {
		mListeners.removeAllListeners();
	}

	@Override
	public void enableWearableEvent (@NonNull final Class<? extends WearableEvent>... events) {
		Validations.validateNotNull(events, "events");
		for (Class eventClass : events) {
			if (mBLEUartController.isBLEUartEventClass(eventClass)) {
				mBLEUartController.enable(eventClass);
			}
		}
	}

	@Override
	public void disableWearableEvent (@NonNull final Class<? extends WearableEvent>... events) {
		Validations.validateNotNull(events, "events");
		for (Class eventClass : events) {
			if (mBLEUartController.isBLEUartEventClass(eventClass)) {
				mBLEUartController.disable(eventClass);
			}
		}
	}

	@Override
	public boolean isWearableEventEnabled (@NonNull final Class<? extends WearableEvent>
			                                           eventClass) {
		Validations.validateNotNull(eventClass, "eventClass");
		return mBLEUartController.isBLEUartEventClass(eventClass) && mBLEUartController.isEnabled
				(eventClass);
	}

	@Override
	public synchronized IUartController getUartController () {
		return mBLEUartController;
	}

	private void readBatteryLevel () {
		readCharacteristic(Constants.BATTERY_SERVICE_UUID, Constants.BATTERY_LEVEL_UUID);
	}

	private void readCharacteristic (final @NonNull UUID service, final @NonNull UUID
			characteristic) {
		mBLEConnection.readCharacteristic(service, characteristic, (service1, characteristic1,
		                                                            bytes, status) -> {
			if (status == Status.SUCCESS) {
				if (bytes != null && bytes.length > 0) {
					mCharacteristics.put(characteristic1, new String(bytes));
					if (Constants.BATTERY_LEVEL_UUID.equals(characteristic1)) {
						mCharacteristics.put(characteristic1, Byte.toString(bytes[0]));
						notifyBatteryStatusInMainThread(new WearableBatteryStatus
								(getBatteryLevel()), BLEWearableController.this);
					} else if (Constants.MANUFACTURER_NAME_UUID.equals(characteristic1)) {
						if (createWearableIdentity()) {
							mIsConnected = true;
							notifyEventInMainThread(Event.CONNECTED, BLEWearableController
									.this);
						} else {
							notifyErrorInMainThread(new Error(Error
									.BLE_INVALID_UNIQUE_IDENTIFIER), BLEWearableController
									.this);
							disconnect();
						}
					}
				} else {
					Logger.w("Characteristic [%s] value is empty", characteristic1);
				}
			} else {
				notifyErrorInMainThread(new Error(Error
						.BLE_ERROR_CHARACTERISTIC_READ_ERROR), BLEWearableController.this);
			}
		});
	}

	/**
	 * Notify all the registered {@link IWearableControllerListener}s of change in battery status.
	 * The callback is made on the main UI thread, to let users update the UI accordingly.
	 *
	 * @param batteryStatus
	 * @param controller
	 */
	private void notifyBatteryStatusInMainThread (final WearableBatteryStatus batteryStatus,
	                                              final IWearableController controller) {
		for (IWearableControllerListener listener : mListeners.getListeners()) {
			mHandler.post(() -> listener.onBatteryStatusUpdate(controller, batteryStatus));
		}
	}

	private byte getBatteryLevel () {
		byte status = -1;
		String batteryStatus = mCharacteristics.get(Constants.BATTERY_LEVEL_UUID);
		if (batteryStatus != null) {
			try {
				status = Byte.parseByte(batteryStatus);
				if (status < 0 || status > 100) {
					Logger.e("Battery status value out of range: %s", batteryStatus);
				}
			} catch (NumberFormatException nfe) {
				Logger.e("Couldn't parse battery status value: %s", batteryStatus);
			}
		} else {
			Logger.w("Battery status value not available");
		}
		return status;
	}

	private boolean createWearableIdentity () {
		if (mCharacteristics.get(Constants.SERIAL_NUMBER_UUID) != null) {
			try {
				mWearableIdentity = LocalDataStore.registerWearable(getUUIDFromSerialNumber
								(mCharacteristics.get(Constants.SERIAL_NUMBER_UUID)),
						mBluetoothDevice.getAddress(),
						mBluetoothDevice.getName(),
						mCharacteristics.get(Constants.MANUFACTURER_NAME_UUID),
						mCharacteristics.get(Constants.MODEL_NUMBER_STRING_UUID),
						mCharacteristics.get(Constants.FIRMWARE_REV_UUID),
						mCharacteristics.get(Constants.SOFTWARE_REV_UUID),
						mCharacteristics.get(Constants.HARDWARE_REV_UUID)
				);
				return true;
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Notify all the registered {@link IWearableControllerListener}s of an event. The callback is
	 * made on the main UI thread, to let users update the UI accordingly
	 *
	 * @param event      the {@link Event} to notify
	 * @param controller this {@link IWearableController} instance
	 */
	private void notifyEventInMainThread (final Event event, final IWearableController
			controller) {
		for (final IWearableControllerListener listener : mListeners.getListeners()) {
			mHandler.post(() -> {
				switch (event) {
					case CONNECTING:
						listener.onConnecting(controller);
						break;
					case CONNECTED:
						listener.onConnected(controller);
						break;
					case DISCONNECTING:
						listener.onDisconnecting(controller);
						break;
					case DISCONNECTED:
						listener.onDisconnected(controller);
						break;
					case PAIRED:
						listener.onPairedStatusChanged(controller, true);
						break;
					case UNPAIRED:
						listener.onPairedStatusChanged(controller, false);
						break;
					case SUBSCRIBED_TO_BATTERY_STATUS_UPDATE_EVENTS:
						listener.subscribedToBatteryStatusUpdateEvents(controller);
						break;
					case UNSUBSCRIBED_FROM_BATTERY_STATUS_UPDATE_EVENTS:
						listener.unsubscribedFromBatteryStatusUpdateEvents(controller);
						break;
					default:
						Logger.w("Unknown event: %s", event);
				}
			});
		}
	}

	private UUID getUUIDFromSerialNumber (final String uuidString) {
		UUID result = null;
		if (uuidString != null) {
			if (uuidString.length() == 32) {
				result = UUID.fromString(addUUIDDashes(uuidString));
			} else if (uuidString.length() == 36) {
				result = UUID.fromString(uuidString);
			} else {
				// FIXME: 5/23/17 SERIAL NUMBER from device not a UUID
				/** Since the SERIAL number from device is not of type UUID we will
				 hardcode a ID as a workround instead of throwing an Exception
				 IMEC should fix it by adding support of unique serial number for every device
				 which will be of type UUID
				 */
				/*Logger.e("Invalid Device Unique Identifier: %s", uuidString);
				throw new IllegalArgumentException(
						"Invalid Device Unique Identifier: " + uuidString);*/
				result = UUID.fromString("099801cd-f5d4-44fd-935b-26aff0eeb2d5"); //some random
				// UUID generated from https://www.uuidgenerator.net/
			}
		}
		Logger.d("Device Unique Identifier: %s", result);
		return result;
	}

	private static String addUUIDDashes (final String uuidNoDashes) {
		StringBuffer stringBuffer = new StringBuffer(uuidNoDashes);
		return stringBuffer.insert(20, '-').insert(16, '-').insert(12, '-').insert(8, '-')
				.toString();
	}

	/**
	 * Notify all the registered {@link IWearableControllerListener}s of an error condition. The
	 * callback is made on the main UI thread, to let users update the UI accordingly.
	 *
	 * @param error      the {@link Error} to notify
	 * @param controller this {@link IWearableController} instance
	 */
	private void notifyErrorInMainThread (final Error error, final IWearableController
			controller) {
		for (final IWearableControllerListener listener : mListeners.getListeners()) {
			mHandler.post(() -> listener.onFailure(controller, error));
		}
	}

	/**
	 * Method invoked when the bonding state changes.
	 *
	 * @param bondState the new bonding state
	 */
	public void onBondStateChanged (final int bondState) {
		switch (bondState) {
			case BluetoothDevice.BOND_BONDED:
				notifyEventInMainThread(Event.PAIRED, this);
				break;
			case BluetoothDevice.BOND_NONE:
				notifyEventInMainThread(Event.UNPAIRED, this);
				break;
			default:
				Logger.w("Unknown bond state: %s", bondState);
		}
	}

	private void readDeviceInformation () {
		for (UUID deviceInfoUUID : deviceInfoUUIDs) {
			readDeviceInfoCharacteristic(deviceInfoUUID);
		}
	}

	private void readDeviceInfoCharacteristic (final UUID characteristicUuid) {
		readCharacteristic(Constants.DEVICE_INFO_SERVICE_UUID, characteristicUuid);
	}

	private enum Event {
		CONNECTING,
		CONNECTED,
		DISCONNECTING,
		DISCONNECTED,
		PAIRED,
		UNPAIRED,
		SUBSCRIBED_TO_BATTERY_STATUS_UPDATE_EVENTS,
		UNSUBSCRIBED_FROM_BATTERY_STATUS_UPDATE_EVENTS
	}

	public enum ConnectionPriority {
		HIGH_PRIORITY,
		BALANCED,
		LOW_POWER
	}

	private static class ConnectionStateChangeListener implements IConnectionStateChangeListener {

		BLEWearableController mBLEWearableController;

		ConnectionStateChangeListener (final BLEWearableController BLEWearableController) {
			mBLEWearableController = BLEWearableController;
		}

		@Override
		public void onConnectionStateChange (final IConnectable.States oldState,
		                                     final IConnectable.States newState) {
			if (oldState != newState && mBLEWearableController != null) {
				switch (newState) {
					case CONNECTING:
						Logger.d("CONNECTING");
						mBLEWearableController.notifyEventInMainThread(Event.CONNECTING,
								mBLEWearableController);
						break;
					case CONNECTED:
						Logger.d("CONNECTED");
						mBLEWearableController.notifyEventInMainThread(Event.CONNECTED,
								mBLEWearableController);
						mBLEWearableController.readDeviceInformation();
						mBLEWearableController.readBatteryLevel();
						break;
					case DISCONNECTING:
						Logger.d("DISCONNECTING");
						mBLEWearableController.notifyEventInMainThread(Event.DISCONNECTING,
								mBLEWearableController);
						break;
					case DISCONNECTED:
						Logger.d("DISCONNECTED");
						mBLEWearableController.notifyEventInMainThread(Event.DISCONNECTED,
								mBLEWearableController);
						break;
					default:
						Logger.w("Unknown State: %s", newState);
				}
			}
		}

		@Override
		public int hashCode () {
			return mBLEWearableController != null ? mBLEWearableController.hashCode() : super
					.hashCode();
		}

		@Override
		public boolean equals (final Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}

			final ConnectionStateChangeListener that = (ConnectionStateChangeListener) o;

			return mBLEWearableController == null && that.mBLEWearableController == null
					|| mBLEWearableController != null && mBLEWearableController.equals(that
					.mBLEWearableController);
		}
	}

	private class PairingBroadcastReceiver extends BroadcastReceiver {

		private final BluetoothDevice mBluetoothDevice;

		private final BLEWearableController mBleWearableController;

		public PairingBroadcastReceiver (final BluetoothDevice bluetoothDevice, final
		BLEWearableController bleWearableController) {
			mBluetoothDevice = bluetoothDevice;
			mBleWearableController = bleWearableController;
		}

		@Override
		public void onReceive (final Context context, final Intent intent) {
			if (intent.getAction().equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				if (device != null) {
					if (mBluetoothDevice.getAddress().equals(device.getAddress())) {
						mBleWearableController.onBondStateChanged(mBluetoothDevice.getBondState());
					}
				}
			}
		}
	}
}
