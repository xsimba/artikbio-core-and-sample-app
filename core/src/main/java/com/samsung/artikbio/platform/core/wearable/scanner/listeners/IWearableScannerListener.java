package com.samsung.artikbio.platform.core.wearable.scanner.listeners;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.scanner.IWearableScanner;

/**
 * Delivers events for the wearable scanning process.
 */
public interface IWearableScannerListener {

	/*
	 * A wearable device was found.
     *
     * @param wearableScanner the instance of the {@link IWearableScanner} used for scanning
     * @param wearableToken   a wearable token which uniquely identifies the wearable device and
     *                        can be used with the {@link WearableControllerFactory}
     *                        to get a {@link IWearableController} implementation to interact with
     *                        the wearable device
     */
	void onWearableFound (@NonNull IWearableScanner wearableScanner,
	                      @NonNull WearableToken wearableToken);

	/**
	 * There was an error in the scanning process.
	 *
	 * @param wearableScanner the instance of the {@link IWearableScanner} used for scanning
	 * @param error           the {@link Error} object with the error description
	 */
	void onScannerError (@NonNull IWearableScanner wearableScanner,
	                     @NonNull Error error);
}
