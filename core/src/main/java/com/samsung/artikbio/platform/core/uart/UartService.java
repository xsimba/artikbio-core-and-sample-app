package com.samsung.artikbio.platform.core.uart;

import com.samsung.artikbio.platform.core.applications.UartBleService;
import com.samsung.artikbio.platform.core.event.EventManager;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Status;
import com.samsung.artikbio.platform.core.wearable.controller.IWearableController;

import java.util.concurrent.ArrayBlockingQueue;

public class UartService implements UartBleService.IUartReceptionCallback {

	private static boolean isStreamingStarted;
	final int INPUT_BUFFER_SIZE = 2 * 1024 * 1024;
	private Thread mUartProcessingThread;
	private volatile boolean mUartProcessingThreadInterruptFlag;
	private ArrayBlockingQueue<Byte> mBuffer = new ArrayBlockingQueue<>(INPUT_BUFFER_SIZE);

	private IWearableController mWearableController;

	public UartService (final IWearableController wearableController) {
		mWearableController = wearableController;
	}

	/*public native void receiveData();
		public native void initGlobalRef();
		public static native void setConfig(int config, int sensorType, int sensorProperty, int
		sensorPropertyValue);
		public static native void setStateConfig(int config, int sensorType, String str);
		public native void setQuery(int config, int sensorType , int sensorProperty ,int
		sensorPropertyValue);

		*/
	public boolean startUartStreaming () {

		if (mUartProcessingThread == null) {
			mUartProcessingThreadInterruptFlag = false;

			mUartProcessingThread = new Thread(() -> {
				while (!Thread.interrupted() && !mUartProcessingThreadInterruptFlag) {
					try {
						receiveData();
					} catch (Exception e) {
						Logger.i(e.toString());
						mUartProcessingThreadInterruptFlag = true;
					}
				}
			});

			if (!mUartProcessingThread.isAlive()) {
				Logger.d("Starting Uart Protocol thread");
				mUartProcessingThread.setName("UartStreaming");
				mUartProcessingThread.start();
			}
		}
		return isStreamingStarted = mUartProcessingThread.isAlive();
	}

	/** Load jni .so on initialization *//*
	static {
		System.loadLibrary("artikbio-protocol-jni");
	}*/
	public void receiveData () {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Logger.d("Receive Data after sleeping for 2 sec");
	}

	public boolean stopUartStreaming () {
		Logger.d("Stop Uart Protocol Thread");
		if (mUartProcessingThread != null && mUartProcessingThread.isAlive()) {
			mUartProcessingThread.interrupt();
			mUartProcessingThread = null;
		}
		return isStreamingStarted = mUartProcessingThread != null;// returns true or false based
		// on the thread instance
	}

	@Override
	public void onUartValueReported (final byte[] data, final Status status) {
		Logger.d("Data received");
		EventManager.getInstance().post(new UartEvent(data, mWearableController
				.getWearableIdentity().getId()));
	}
}
