/**
 * Module initialization methods
 *
 * <p>This is-the top level package for Core SDK, containing the {@link
 * com.samsung.artikbio.platform.core.Core} singleton which is used to initialize the Core SDK
 */
package com.samsung.artikbio.platform.core;