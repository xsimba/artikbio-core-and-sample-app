package com.samsung.artikbio.platform.core.wearable.controller;

import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.event.WearableEvent;
import com.samsung.artikbio.platform.core.persistance.model.WearableIdentity;
import com.samsung.artikbio.platform.core.wearable.WearableBatteryStatus;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.controller.listeners.IWearableControllerListener;
import com.samsung.artikbio.platform.core.wearable.scanner.IWearableScanner;

/**
 * Provides the communication layer between the wearable device and the companion device.
 * To communicate with the Wearable Controller corresponding to a specific wearable device,
 * the caller needs to use the {@link WearableControllerFactory} providing a {@link WearableToken}
 * obtained from a {@link IWearableScanner} as part of the scanning process
 */

public interface IWearableController {

	/**
	 * Returns the {@link WearableToken} of the device this controller is associated with.
	 *
	 * @return the wearable token of the device this controller is associated with
	 */
	WearableToken getWearableToken ();

	/**
	 * Establishes a connection with the wearable (with autoconnect set to {@code true})
	 * <p>
	 * Once the connection is made, the callbacks will be made until the wearable is
	 * disconnected or
	 * a failure occurs. Connection status updates are reported in {@link
	 * IWearableControllerListener#onConnecting(IWearableController)} and {@link
	 * IWearableControllerListener#onConnected(IWearableController)} while errors are reported in
	 * {@link IWearableControllerListener#onFailure(IWearableController, Error)}
	 */
	void connect ();

	/**
	 * Establishes a connection with the wearable (with autoconnect set to {@code true})
	 * <p>
	 * Once the connection is made, the callbacks will be made until the wearable is
	 * disconnected or
	 * a failure occurs. Connection status updates are reported in {@link
	 * IWearableControllerListener#onConnecting(IWearableController)} and {@link
	 * IWearableControllerListener#onConnected(IWearableController)} while errors are reported in
	 * {@link IWearableControllerListener#onFailure(IWearableController, Error)}
	 *
	 * @param autoconnect {@code true} to connect with autoconnect enabled or {@code false} to
	 *                    connect with autoconnect disabled
	 */
	void connect (boolean autoconnect);

	/**
	 * Requests the current battery status. The response is reported in {@link
	 * IWearableControllerListener#onBatteryStatusUpdate(IWearableController,
	 * WearableBatteryStatus)}
	 *
	 * @see WearableBatteryStatus
	 */
	void getBatteryStatus ();

	/**
	 * Subscribes to receive battery status update events. Events will be reported in {@link
	 * IWearableControllerListener#subscribedToBatteryStatusUpdateEvents(IWearableController)}
	 */
	void subscribeToBatteryStatusUpdateEvents ();

	/**
	 * Unsubscribe from battery status update events. Events will stop being reported in {@link
	 * IWearableControllerListener#unsubscribedFromBatteryStatusUpdateEvents(IWearableController)}
	 */
	void unsubscribeFromBatteryStatusUpdateEvents ();

	/**
	 * Returns a {@link WearableIdentity} object with information about the wearable device
	 * or null if this controller is not connected to the wearable device.
	 *
	 * @return a {@link WearableIdentity} object with information about the wearable device
	 */
	WearableIdentity getWearableIdentity ();

	/**
	 * Disconnects from the wearable. Disconnection status updates are reported in {@link
	 * IWearableControllerListener#onDisconnecting(IWearableController)} and {@link
	 * IWearableControllerListener#onDisconnected(IWearableController)}
	 *
	 * @return true if disconnection request was successfully accepted or false if not (ie: if
	 * there
	 * was no connection to the wearable to disconnect from)
	 */
	boolean disconnect ();

	/**
	 * Pairs with the wearable device. Pairing status updates are reported in {@link
	 * IWearableControllerListener#onPairedStatusChanged(IWearableController, boolean)} only if the
	 * pairing status is changed as a result of this call (if the wearable was already paired there
	 * will be no call to onPairedStatusChanged)
	 */
	void pair ();

	/**
	 * Unpairs from the wearable device. Unpairing status updates are reported in {@link
	 * IWearableControllerListener#onPairedStatusChanged(IWearableController, boolean)} only if the
	 * pairing status is changed as a result of this call (if the wearable was already unpaired
	 * there will be no call to onPairedStatusChanged)
	 */
	void unpair ();

	/**
	 * Indicates whether this wearable is paired or not.
	 *
	 * @return whether this wearable is paired or not
	 */
	boolean isPaired ();

	/**
	 * Indicates whether this controller is connected to the wearable device.
	 *
	 * @return whether this controller is connected to the wearable device
	 */
	boolean isConnected ();

	/**
	 * Add an {@link IWearableControllerListener} instance to receive notifications of
	 * asynchronous events.
	 *
	 * @param listener an instance of {@link IWearableControllerListener} to receive notifications
	 *                 of asynchronous events
	 *
	 * @return {@code true} if the listener was successfully added, {@code false} otherwise. If
	 * listener is null it returns {@code false}
	 */
	boolean addWearableControllerListener (IWearableControllerListener listener);

	/**
	 * Remove an {@link IWearableControllerListener} instance which was previously
	 * added. If no listener was previously added this does nothing.
	 *
	 * @param listener to remove
	 *
	 * @return {@code true} if the listener was successfully removed, {@code false} otherwise. If
	 * listener is null it returns {@code false}
	 */
	boolean removeWearableControllerListener (IWearableControllerListener listener);

	/**
	 * Remove all existing listeners.
	 */
	void removeAllWearableControllerListeners ();

	/**
	 * Enable listening to events of the specified types.
	 *
	 * @param eventClasses Classes of events to listen
	 */
	void enableWearableEvent (Class<? extends WearableEvent>... eventClasses);

	/**
	 * Disable listening to events of the specified types.
	 *
	 * @param eventClasses Classes of events to unsubscribe from
	 */
	void disableWearableEvent (Class<? extends WearableEvent>... eventClasses);

	/**
	 * Check whether the controller is listening to events of the specified class.
	 *
	 * @param eventClass Class the event may be listening to
	 *
	 * @return true if the controller is subscribed to that event, false otherwise
	 */
	boolean isWearableEventEnabled (Class<? extends WearableEvent> eventClass);

	/**
	 * Returns an instance of a {@link IUartController} or null if there's no connection to
	 * the wearable device or if the wearable device is not supported.
	 *
	 * @return the instance of the uart controller associated to this wearable controller
	 */
	IUartController getUartController ();
}
