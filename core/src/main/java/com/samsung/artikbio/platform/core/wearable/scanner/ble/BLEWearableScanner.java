package com.samsung.artikbio.platform.core.wearable.scanner.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.samsung.artikbio.platform.core.Internal;
import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.util.Validations;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.ble.BLEDeviceRegistry;
import com.samsung.artikbio.platform.core.wearable.scanner.IWearableScanner;
import com.samsung.artikbio.platform.core.wearable.scanner.listeners.IWearableScannerListener;

import java.util.HashSet;
import java.util.Set;

/**
 * Wearable Scanner API performs a discovery of Samsung wearable devices. This is an asynchronous,
 * non-blocking operation. Results of the discovery are reported to the caller via a supplied
 * callback without exposing details of the communication protocol.
 * <p>
 * This class is the entry point into the Core SDK. Clients use this class to scan for Samsung
 * wearables and obtain a WearableToken for any discovered wearable devices.
 */
@Internal
public final class BLEWearableScanner implements IWearableScanner, BluetoothAdapter.LeScanCallback {

	private BluetoothAdapter mBluetoothAdapter;

	private boolean mScanning;

	private IWearableScannerListener mWearableScannerListener;

	private Handler mMainLooperHandler;

	// Used to filter out already discovered wearables and avoid duplicate notifications
	private Set<String> discoveredWearables = new HashSet<>();

	private BLEWearableScanner () {
		mMainLooperHandler = new Handler(Looper.getMainLooper());
		mBluetoothAdapter = ContextProvider.getInstance().getBluetoothAdapter();
	}

	/**
	 * Return the singleton instance.
	 *
	 * @return the singleton instance
	 */
	public static BLEWearableScanner getInstance () {
		return Holder.INSTANCE;
	}

	@Override
	public synchronized boolean startScan (@NonNull final IWearableScannerListener
			                                           wearableScannerListener) {
		Validations.validateNotNull(wearableScannerListener, "wearableScannerListener");
		Logger.d("Start Scanning");
		if (mScanning) {
			Logger.e("Error %d", Error.BLE_ERROR_SCANNER_ALREADY_STARTED);
			wearableScannerListener.onScannerError(this,
					new Error(Error.BLE_ERROR_SCANNER_ALREADY_STARTED));
			return false;
		}

		if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
			Logger.e("Error %d", Error.BLE_ERROR_BT_DISABLED);
			wearableScannerListener.onScannerError(this,
					new Error(Error.BLE_ERROR_BT_DISABLED));
			return false;
		}
		mWearableScannerListener = wearableScannerListener;
		discoveredWearables.clear();
		mScanning = mBluetoothAdapter.startLeScan(this);
		return mScanning;
	}

	@Override
	public synchronized void stopScan () {
		Logger.d("Stop Scanning");
		if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
			mBluetoothAdapter.stopLeScan(this);
		}
		mScanning = false;
		mWearableScannerListener = null;
	}

	@Override
	public boolean isScanning () {
		return mScanning;
	}

	@Override
	public void onLeScan (final BluetoothDevice bluetoothDevice, final int rssi, final byte[]
			scanRecord) {
		Validations.validateNotNull(bluetoothDevice, "bluetoothDevice");
		Validations.validateNotNull(scanRecord, "scanRecord");
		synchronized (this) { // protect mWearableScannerListener from potentially changing from
			// another thread's startScan
			if (discoveredWearables.add(bluetoothDevice.getAddress()) &&
					mWearableScannerListener != null) {
				Logger.d("Found new blueotooth device [Address: %s]", bluetoothDevice.getAddress());
				final WearableToken wearableToken = createWearableToken(bluetoothDevice.getName(),bluetoothDevice
								.getAddress());
				BLEDeviceRegistry.register(wearableToken, bluetoothDevice);
				notifyDeviceFound(mWearableScannerListener, wearableToken);
			}
		}
	}

	private WearableToken createWearableToken (@Nullable  final String name,
	                                           @NonNull  final String address) {
		return new WearableToken(address, name);
	}

	private void notifyDeviceFound (final IWearableScannerListener listener,
	                                final WearableToken wearableToken) {
		mMainLooperHandler.post(new Runnable() {
			@Override
			public void run () {
				listener.onWearableFound(BLEWearableScanner.this, wearableToken);
			}
		});
	}

	private static class Holder {
		private static final BLEWearableScanner INSTANCE = new BLEWearableScanner();
	}
}
