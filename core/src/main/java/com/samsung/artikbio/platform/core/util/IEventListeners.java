package com.samsung.artikbio.platform.core.util;

import java.util.Set;

/**
 * Interface for a collection of unique listeners.
 *
 * @param <T> the type of the listeners to hold in the collection
 */
public interface IEventListeners<T> {

	/**
	 * Adds a listener to the collection of listeners.
	 *
	 * @param listener the listener to add. If the listener is {@code null} it is not added and
	 *                    this
	 *                 call returns {@code false}
	 *
	 * @return {@code true} if the listener was added or {@code false} if not
	 */
	boolean addListener (T listener);

	/**
	 * Removes a listener from the collection of listeners.
	 *
	 * @param listener the listener to remove. If the listener is {@code null} nothing is removed
	 *                 and this call returns {@code false}
	 *
	 * @return {@code true} if the listener was removed or {@code false} if not
	 */
	boolean removeListener (T listener);

	/**
	 * Removes all listeners in the collection.
	 */
	void removeAllListeners ();

	/**
	 * Return a copy of the collection of listeners.
	 *
	 * @return the collection of listeners
	 */
	Set<T> getListeners ();

	/**
	 * Return whether the listener is registered to receive events.
	 *
	 * @param listener the listener to check
	 *
	 * @return {@code true} if the listener is registered or {@code false} if not
	 */
	boolean isRegistered (T listener);

	/**
	 * Returns whether there are listeners in the collection or not.
	 *
	 * @return {@code true} if there are no listeners in the collection or {@code false} otherwise
	 */
	boolean isEmpty ();
}