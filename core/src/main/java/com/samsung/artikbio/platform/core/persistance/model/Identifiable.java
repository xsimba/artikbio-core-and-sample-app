package com.samsung.artikbio.platform.core.persistance.model;

/**
 * Interface intended to be implemented by immutable model objects.
 * Primarily exists so the parametrized identifier type can be accessed
 * when using a generic for the immutable model.
 *
 * @param <T> type to be used as the identifier.
 */
public interface Identifiable<T> {
	/**
	 * Gets the {@code T} which identifies this object.
	 *
	 * @return identifier value.
	 */
	T getId();
}
