package com.samsung.artikbio.platform.core.persistance.datastore;

import com.samsung.artikbio.platform.core.persistance.model.UserIdentity;
import com.samsung.artikbio.platform.core.persistance.realm.RealmPersistable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * A {@link RealmObject} for holding
 * {@link UserIdentity} data.
 */
public class PersistableUserIdentity extends RealmObject implements RealmPersistable {

	/** Unique identifier for this user identity. */
	@Required
	@PrimaryKey
	private String id;

	/** the userId. */
	private String userId;

	private String firstName;

	private String lastName;

	private String email;


	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(final String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}
}