package com.samsung.artikbio.platform.core.persistance.realm;

import io.realm.DynamicRealm;

/**
 * {@link SdkMigration} implementation specific to Core data structures.
 */
class CoreMigration extends SdkMigration {

	/**
	 * Construct an {@link SdkMigration} instance with all the necessary
	 * {@link VersionedMigration}s.
	 */
	CoreMigration() {
		super(
				new V0ToV1()
		);
	}

	// region v0 to v1
	/**
	 * Migrate from v0 (no version specified) to v1.
	 */
	static final class V0ToV1 implements VersionedMigration {
		@Override
		public long initialVersion() {
			return 0L;
		}

		@Override
		public void migrate(final DynamicRealm realm, final long currentVersion, final long nextVersion) {
			checkStartingVersion(realm, currentVersion, this.initialVersion());
		}

		@Override
		public long resultingVersion() {
			return 1L;
		}
	}
	// endregion v0 to v1

}