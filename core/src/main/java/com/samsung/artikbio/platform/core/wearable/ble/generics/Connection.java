package com.samsung.artikbio.platform.core.wearable.ble.generics;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.wearable.WearableToken;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class Connection implements IConnectable, IReadCharacteristic, IWriteCharacteristic,
                                            ISubscribeCharacteristic {

	private List<IConnectionStateChangeListener> mConnectionStateChangeListeners;
	private WearableToken mWearableToken;
	private States mCurrentConnectionState;
	private States mOldConnectionState;


	/**
	 * Default constructor for a connection.
	 *
	 * @param token the device token to make a connection on.
	 */
	public Connection (final @NonNull WearableToken token) {
		mWearableToken = token;
		mCurrentConnectionState = States.DISCONNECTED;
		mConnectionStateChangeListeners = new CopyOnWriteArrayList<>();
	}

	/**
	 * Get the WearableToken bind to this connection.
	 *
	 * @return an instance of a WearableToken.
	 */
	public WearableToken getWearableToken () {
		return mWearableToken;
	}

	/**
	 * Add a listener on the connection state. This listener will be callback when the state of
	 * this
	 * connection change.
	 *
	 * @param listener an instance of IConnectionStateChangeListener.
	 *
	 * @return true if the listener is added to the list on listeners, false otherwise.
	 */
	public boolean addConnectionStateChangeListener (final IConnectionStateChangeListener
			                                                 listener) {
		if (!mConnectionStateChangeListeners.contains(listener)) {
			return mConnectionStateChangeListeners.add(listener);
		}
		return false;
	}

	/**
	 * Remove a listener from the list on connection state change listeners.
	 *
	 * @param listener an instance of IConnectionStateChangeListener
	 *
	 * @return true if the listener is removed from the list on listeners, false otherwise.
	 */
	public boolean removeConnectionStateChangeListener (final IConnectionStateChangeListener
			                                                    listener) {
		return mConnectionStateChangeListeners.remove(listener);
	}

	/**
	 * Connection should call this method to fall back on its previously save state.
	 *
	 * @return true if the connection state has been change using a previously save one.
	 *
	 * @see #changeConnectionState(States, boolean)
	 */
	protected boolean fallBackOnPreviousConnectionState () {
		if (mOldConnectionState != null && mOldConnectionState != mCurrentConnectionState) {
			changeConnectionState(mOldConnectionState);
			return true;
		}
		return false;
	}

	/**
	 * Connection should call this method to change their state, this will ensure proper call to
	 * connection state change listeners.
	 *
	 * @param states the new state of the connection.
	 */
	protected void changeConnectionState (final States states) {
		changeConnectionState(states, false);
	}

	/**
	 * Connection should call this method to change their state, this will ensure proper call to
	 * connection state change listeners.
	 *
	 * @param states     the new state of the connection.
	 * @param revertible if true the previous state of this connection will be save and can be set
	 *                   again by calling {@link #fallBackOnPreviousConnectionState()}
	 */
	protected void changeConnectionState (final States states, final boolean revertible) {
		if (!mCurrentConnectionState.equals(states)) {
			mOldConnectionState = mCurrentConnectionState;
			mCurrentConnectionState = states;
			for (IConnectionStateChangeListener listener : mConnectionStateChangeListeners) {
				listener.onConnectionStateChange(mOldConnectionState, mCurrentConnectionState);
			}
			if (!revertible) {
				mOldConnectionState = null;
			}
		}
	}

	/**
	 * Get the state of this connection.
	 *
	 * @return the state of this connection.
	 */
	public States getConnectionState () {
		return mCurrentConnectionState;
	}
}
