package com.samsung.artikbio.platform.core.event;

import android.support.annotation.Nullable;

import net.jcip.annotations.Immutable;

/**
 * And {@link Event} implementation which represents an mError
 * state when attempting to process an {@link Event}.
 */
@Immutable
public class EventHandlerErrorEvent implements Event {

	/** Holds the thrown mError, if there was one. */
	private final Throwable mError;
	/** The {@link Event} for which this is set. */
	private final Event mEvent;
	/** A mMessage related to the {@code code} from handling {@code mEvent}. */
	private final String mMessage;
	/** Name of the {@link EventHandler} which handled {@code mEvent}. */
	private final String mEventHandlerName;
	/** System time at which the mError mEvent was created. */
	private final long mTimestamp;
	/** Event Id based on {@code mEvent}, {@code mEventHandlerName} and {@code mTimestamp}. */
	private final String mEventId;

	/**
	 * Create a new instance setting all fields.
	 *
	 * @param error            to set.
	 * @param event            to set.
	 * @param message          to set.
	 * @param eventHandlerName to set.
	 */
	EventHandlerErrorEvent (final Throwable error, final Event event, final String message, final
	String eventHandlerName) {
		mError = error;
		mEvent = event;
		mMessage = message;
		mEventHandlerName = eventHandlerName;
		mTimestamp = System.currentTimeMillis();
		mEventId = event.getEventId()
				+ ":::"
				+ eventHandlerName
				+ ":::"
				+ mTimestamp
		;
	}

	@Override
	public String getEventId () {
		return mEventId;
	}

	public long getTimestamp () {
		return mTimestamp;
	}

	/**
	 * The error thrown or returned when attempting to process
	 * {@link #getEvent()}.
	 *
	 * @return the error thrown if there was one, {@code null} otherwise.
	 */
	@Nullable
	public Throwable getError () {
		return mError;
	}

	/**
	 * The {@link Event} for which this is a processing result.
	 *
	 * @return the event which was processed.
	 */
	public Event getEvent () {
		return mEvent;
	}

	/**
	 * A message returned by the service to accompany the status code.
	 *
	 * @return the message provided by the service.
	 */
	@Nullable
	public String getMessage () {
		return mMessage;
	}

	/**
	 * A value corresponding to {@link EventHandler#getName()} for the
	 * {@link EventHandler} which produced this {@code EventStatus} for
	 * {@link #getEvent()}.
	 *
	 * @return the service name which produced this {@code EventStatus}.
	 */
	public String getEventHandlerName () {
		return mEventHandlerName;
	}

	/**
	 * An implementation of the builder pattern for creating
	 * {@link EventHandlerErrorEvent} instances.
	 */
	public static final class Builder {
		private Throwable mError;
		private Event mEvent;
		private String mMessage;
		private String mEventHandlerName;

		/**
		 * Create a new Builder with all empty fields.
		 */
		public Builder () {
			this(null, (String) null);
		}

		/**
		 * Create a new Builder with {@code event} and {@code eventHandlerName}
		 * set.
		 *
		 * @param event            to set.
		 * @param eventHandlerName to set.
		 */
		public Builder (final Event event, final String eventHandlerName) {
			mError = null;
			mEvent = event;
			mMessage = null;
			mEventHandlerName = eventHandlerName;
		}

		public Builder (final Event event, final EventHandler eventHandler) {
			this(event, eventHandler.getName());
		}

		/**
		 * Set the {@link Throwable} error for the {@code EventStatus} to
		 * generate.
		 *
		 * @param e to set.
		 *
		 * @return the {@code Builder} instance.
		 */
		public Builder error (final Throwable e) {
			mError = e;
			return this;
		}

		/**
		 * Set the {@link Event} for the {@code EventStatus} to generate.
		 *
		 * @param e to set.
		 *
		 * @return the {@code Builder} instance.
		 */
		public Builder event (final Event e) {
			mEvent = e;
			return this;
		}

		/**
		 * Set the message for the {@code EventStatus} to generate.
		 *
		 * @param m to set.
		 *
		 * @return the {@code Builder} instance.
		 */
		public Builder message (final String m) {
			mMessage = m;
			return this;
		}

		/**
		 * Set the {@code eventHandlerName} by getting it from {@link EventHandler}.
		 *
		 * @param eventHandler to set.
		 *
		 * @return the {@code Builder} instance.
		 */
		public Builder eventHandler (final EventHandler eventHandler) {
			return eventHandlerName(eventHandler.getName());
		}

		/**
		 * Set the {@code eventHandlerName} for the {@code EventStatus} to
		 * generate.
		 *
		 * @param name to set.
		 *
		 * @return the {@code Builder} instance.
		 */
		public Builder eventHandlerName (final String name) {
			mEventHandlerName = name;
			return this;
		}

		/**
		 * Generate the {@link EventHandlerErrorEvent} instance.
		 *
		 * @return the generated instance.
		 */
		public EventHandlerErrorEvent build () {
			if (mEvent == null) {
				throw new IllegalStateException("Event must be provided.");
			} else if (mEventHandlerName == null) {
				throw new IllegalStateException("EventHandler name must be set.");
			}
			return new EventHandlerErrorEvent(mError, mEvent, mMessage, mEventHandlerName);
		}
	}
}
