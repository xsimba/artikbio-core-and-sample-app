package com.samsung.artikbio.platform.core.persistance;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;

import com.samsung.artikbio.platform.core.Internal;
import com.samsung.artikbio.platform.core.persistance.model.Identifiable;
import com.samsung.artikbio.platform.core.util.Constants;
import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.Logger;

import java.io.Serializable;

/**
 * Types of actions at the {@link Dao} level for which broadcast
 * notifications may be generated. Uses the {@link Context} from
 * {@link ContextProvider#getApplicationContext()} to send local broadcasts.
 */
@Internal
public enum DBAction {
	/** Create Action. */
	CREATE,
	/** Delete Action. */
	DELETE,
	/** Update Action. */
	UPDATE,;

	/**
	 * Send a broadcast for this action, {@link Intent} broadcasts have {@link Intent#getAction()}
	 * set to the fully qualified class name of the provided {@code modelClass} combined with the
	 * {@link #name()}. The generated {@link Intent} is sent over a {@link LocalBroadcastManager}
	 * retrieved using {@link ContextProvider#getApplicationContext()}.
	 * <p>
	 * <p>The resulting {@link Intent} will, at a minimum, contain an extra with identifier for
	 * {@code model} as {@link Serializable}, either {@link Identifiable#getId()} if the {@link
	 * Identifiable} type parameter is {@link Serializable} or the {@link Object#toString()}
	 * representation if not. The {@link Serializable} extra will have the key {@link
	 * Constants.CoreExtras#EXTRA_DATABASE_ID}.
	 * <p>
	 * <p>If the given {@code model} is {@link Parcelable} it will be included in the {@link
	 * Intent}. The {@link Parcelable} extra will have the key
	 * {@link Constants.CoreExtras#EXTRA_DATABASE_RECORD}.
	 *
	 * @param modelClass used to prefix the database action taken in {@link Intent#getAction()}.
	 * @param model      about which a broadcast is to be sent.
	 * @param <T>        type of model to send, must be {@link Identifiable}.
	 *
	 * @see #getAction(Class)
	 */
	public <T extends Identifiable<?>> void sendBroadcast (final Class<? extends T> modelClass,
	                                                       final T model) {
		final Intent intent = new Intent(this.getAction(modelClass));
		intent.putExtras(DBAction.getExtras(model));
		this.sendBroadcastInternal(intent);
	}

	/**
	 * Send a broadcast for this action, {@link Intent} broadcasts have {@link Intent#getAction()}
	 * set to the fully qualified class name of the provided {@code modelClass} combined with the
	 * {@link #name()}. The generated {@link Intent} is sent over a {@link LocalBroadcastManager}
	 * retrieved using {@link ContextProvider#getApplicationContext()}.
	 * <p>
	 * <p>The resulting {@link Intent} will contain an extra with the {@code key} as {@link
	 * Serializable}, either {@code key} if it is {@link Serializable} or {@code key#toString()} if
	 * not. The {@link Serializable} extra will have the key
	 * {@link Constants.CoreExtras#EXTRA_DATABASE_ID}.
	 *
	 * @param modelClass used to prefix the database action taken in {@link Intent#getAction()}.
	 * @param key        used to identify the record.
	 * @param <T>        type of model to send, must be {@link Identifiable}.
	 * @param <K>        type of key that identifies {@code modelClass} records.
	 *
	 * @see #getAction(Class)
	 */
	public <K, T extends Identifiable<K>> void sendBroadcast (final Class<? extends T> modelClass,
	                                                          final K key) {
		final Intent intent = new Intent(this.getAction(modelClass));
		if (key instanceof Serializable) {
			intent.putExtra(Constants.CoreExtras.EXTRA_DATABASE_ID, (Serializable) key);
		} else if (key != null) {
			intent.putExtra(Constants.CoreExtras.EXTRA_DATABASE_ID, key.toString());
		}
		this.sendBroadcastInternal(intent);
	}

	/**
	 * Get the {@link Intent} action {@code String} for the given
	 * {@code modelClass} and this {@code DatabaseAction}.
	 *
	 * @param modelClass used to prefix the database action taken in {@link Intent#getAction()}.
	 * @param <T>        type of model to send, must be {@link Identifiable}.
	 *
	 * @return an action identifier combining {@code modelClass} and {@link #name()}.
	 */
	public <T extends Identifiable<?>> String getAction (final Class<? extends T> modelClass) {
		return modelClass.getName() + "." + this.name();
	}

	/**
	 * Create a {@link Bundle} of data to allow retrieval of the
	 * {@code model} by an implementation receiving an {@link Intent}
	 * broadcast.
	 *
	 * @param model to be identified in the resulting {@link Bundle}.
	 * @param <T>   type of the model, must be {@link Identifiable}.
	 *
	 * @return a {@link Bundle} containing identifiers for {@code model}.
	 */
	static <T extends Identifiable<?>> Bundle getExtras (final T model) {
		final Bundle extras = new Bundle(2);
		final Object id = model.getId();
		if (id instanceof Serializable) {
			extras.putSerializable(Constants.CoreExtras.EXTRA_DATABASE_ID, (Serializable) id);
		} else if (id != null) {
			extras.putSerializable(Constants.CoreExtras.EXTRA_DATABASE_ID, id.toString());
		}
		if (model instanceof Parcelable) {
			extras.putParcelable(Constants.CoreExtras.EXTRA_DATABASE_RECORD, (Parcelable) model);
		}
		return extras;
	}

	/**
	 * Broadcast the {@link Intent} using a {@link LocalBroadcastManager}
	 * retrieved using {@link ContextProvider#getApplicationContext()}. If no {@link Context} is
	 * set then log the exception.
	 *
	 * @param broadcast to be sent.
	 *
	 * @see #getBroadcaster()
	 */
	void sendBroadcastInternal (final Intent broadcast) {
		final LocalBroadcastManager broadcaster = this.getBroadcaster();
		if (broadcaster == null) {
			return;
		}
		broadcaster.sendBroadcast(broadcast);
	}

	/**
	 * Get a {@link LocalBroadcastManager} using {@link ContextProvider#getApplicationContext()}.
	 * If no {@link Context} is set then log the exception.
	 *
	 * @return the {@link LocalBroadcastManager}.
	 *
	 * @see {@link ContextProvider#getApplicationContext()}
	 * @see LocalBroadcastManager#getInstance(Context)
	 */
	LocalBroadcastManager getBroadcaster () {
		try {
			final Context context = ContextProvider.getInstance().getApplicationContext();
			return LocalBroadcastManager.getInstance(context);
		} catch (IllegalStateException e) {
			// Uses android Log and Logger because Logger probably isn't
			// initialized if Core isn't initialized. Of course if Core
			// isn't initialized how is DBAction being triggered?
			Logger.w(e, "Unable to broadcast %s.", this.name());
			android.util.Log
					.w("DatabaseAction", String.format("Unable to broadcast %s.", this.name()), e);
		}
		return null;
	}

}