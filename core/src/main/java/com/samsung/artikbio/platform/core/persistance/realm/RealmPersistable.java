package com.samsung.artikbio.platform.core.persistance.realm;

import com.samsung.artikbio.platform.core.persistance.Dao;
import com.samsung.artikbio.platform.core.persistance.Persistable;
import com.samsung.artikbio.platform.core.persistance.model.Identifiable;

/**
 * Meant to be implemented by a class which will be persisted by a
 * {@link RealmDatabaseDao} instance. Realm based implementations of
 * {@link Dao} are only able to
 * store values of specific types (list follows) so the {@link java.util.UUID}
 * identifiers provided by {@link Identifiable}
 * must be converted to an acceptable type, namely {@code String}.
 *
 * <ul>
 * <li>{@code boolean}</li>
 * <li>{@code byte}</li>
 * <li>{@code short}</li>
 * <li>{@code int}</li>
 * <li>{@code long}</li>
 * <li>{@code float}</li>
 * <li>{@code double}</li>
 * <li>{@code String}</li>
 * <li>{@code java.util.Date}</li>
 * <li>{@code byte[]}</li>
 * </ul>
 *
 * See <a href=https://realm.io/docs/java/latest/#models>https://realm.io/docs/java/latest/#models</a>
 */
public interface RealmPersistable extends Persistable<String> {

}