package com.samsung.artikbio.platform.core.persistance.realm;

import android.content.Context;
import android.content.Intent;

import com.samsung.artikbio.platform.core.persistance.DBAction;
import com.samsung.artikbio.platform.core.persistance.Dao;
import com.samsung.artikbio.platform.core.persistance.DatedRecord;
import com.samsung.artikbio.platform.core.persistance.NoRecordException;
import com.samsung.artikbio.platform.core.persistance.Persistable;
import com.samsung.artikbio.platform.core.persistance.RecordExistsException;
import com.samsung.artikbio.platform.core.persistance.model.Identifiable;

import java.io.Closeable;
import java.util.Calendar;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * {@link Dao} implementation which saves data to a Realm database. The
 * {@link RealmConverter} interface is used to translate from the mutable
 * objects required by the Realm persistence implementation into the
 * immutable objects to be presented to the application implementer.
 * <b>Storage lifetime is tied to {@link Context} lifetime.</b> A
 * {@link Context} is provided at instance creation time and if, for
 * example, the database needs to be accessed over the lifetime of multiple
 * {@link android.app.Activity}s the instantiator must ensure the provided
 * {@link Context} exists at least as long.
 *
 * @param <Key>        the identifier type used in the {@link Identifiable}
 *                     {@code Model} implementation.
 * @param <Model>      the immutable model type which returned by {@link Dao}
 *                     methods.
 * @param <RealmModel> the mutable model type stored by Realm.
 */
public abstract class RealmDatabaseDao<Key, Model extends Identifiable<Key>, RealmModel extends RealmObject & RealmPersistable>
		implements
		Closeable,
		Dao<Key, Model>,
		RealmConverter<Model, RealmModel> {

	/** The database instance to which {@link RealmModel} instances are persisted. */
	private final Realm realm;

	/** A reference to the {@link Class} of {@link RealmModel}. */
	private final Class<RealmModel> persistableClass;

	/** A reference to the {@link Class} of {@link Model}. */
	private final Class<Model> modelClass;

	/** Whether or not {@link #persistableClass} is assignable as {@link DatedRecord}. */
	private final boolean usesDatedRecords;

	/**
	 * Use {@link Context} to get an instance of the {@link Realm} storage
	 * engine by setting {@code name} and delegating to
	 * {@link #RealmDatabaseDao(RealmConfiguration, Class, Class)}.
	 *
	 * @param context          used to create Realm with defaults.
	 * @param name             of the Realm ({@code ".realm"} is appended to this
	 *                         parameter before setting).
	 * @param modelClass       to be stored by this instance.
	 * @param persistableClass to be stored by this instance.
	 * @throws IllegalArgumentException if {@code configuration} is using
	 *                                  {@link Realm#DEFAULT_REALM_NAME} as its file name.
	 * @deprecated Use {@link #RealmDatabaseDao(RealmConfiguration, Class, Class)}
	 * instead.
	 */
	@Deprecated
	public RealmDatabaseDao(final Context context, final String name,
	                        final Class<Model> modelClass,
	                        final Class<RealmModel> persistableClass) {
		this(
				new RealmConfiguration.Builder().name(name + ".realm").build(),
				modelClass,
				persistableClass
		);
	}

	/**
	 * Use provided {@link RealmConfiguration} to get an instance of the
	 * {@link Realm} storage engine.
	 *
	 * @param configuration    used to create Realm.
	 * @param modelClass       to be stored by this instance.
	 * @param persistableClass to be stored by this instance.
	 * @throws IllegalArgumentException if {@code configuration} is using
	 *                                  {@link Realm#DEFAULT_REALM_NAME} as its file name.
	 */
	public RealmDatabaseDao(final RealmConfiguration configuration,
	                        final Class<Model> modelClass,
	                        final Class<RealmModel> persistableClass) {
		if (configuration.getRealmFileName() == null || Realm.DEFAULT_REALM_NAME
				.equals(configuration.getRealmFileName())) {
			throw new IllegalArgumentException(
					"RealmDatabaseDao may not be used with default name.");
		}
		this.realm = Realm.getInstance(configuration);
		this.modelClass = modelClass;
		this.persistableClass = persistableClass;
		this.usesDatedRecords = DatedRecord.class.isAssignableFrom(persistableClass);
	}

	// region Dao<Key, Model>

	/** {@inheritDoc} */
	@Override
	public void close() {
		this.realm.close();
	}

	/** {@inheritDoc} */
	@Override
	public Model create(final Model model) {
		if (model == null) {
			throw new IllegalArgumentException("Can not create a null record.");
		}
		final Key uuid = this.generateId(model);
		if (this.exists(uuid)) {
			throw new RecordExistsException(this.persistableClass, uuid);
		}
		// Prepare record
		final RealmModel record = this.persistableFrom(model);
		record.setId(uuid.toString());
		this.setUpdatedAtFor(record);
		// Persist record
		this.execute(new CreateRecordTransaction(record));
		final Model created = this.modelFrom(record);
		this.sendBroadcast(DBAction.CREATE, created);
		return created;
	}

	/** {@inheritDoc} */
	@Override
	public Model read(final Key id) {
		final RealmModel record = this.readPersistable(id.toString());
		final Model result;
		if (record == null) {
			result = null;
		} else {
			result = this.modelFrom(record);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 *
	 * An active {@link RealmObject} should not have it's primary key
	 * set during update (only create).
	 */
	@Override
	public Model update(final Key id, final Model model) {
		if (model == null) {
			throw new IllegalArgumentException("Can not update a null record.");
		} else if (id == null) {
			throw new IllegalArgumentException(
					"Must provide an identifier for the record to update.");
		}
		// Read existing record
		final RealmModel record = this.readPersistable(id.toString());
		if (record == null) {
			throw new NoRecordException(this.persistableClass, id);
		}
		// Modify record
		this.execute(new UpdateRecordTransaction(model, record));
		final Model updated = this.modelFrom(record);
		this.sendBroadcast(DBAction.UPDATE, updated);
		return updated;
	}

	/** {@inheritDoc} */
	@Override
	public boolean delete(final Key id) {
		final RealmModel record = this.readPersistable(id.toString());
		final boolean wasFound = record != null;
		if (wasFound) {
			this.execute(new DeleteRecordTransaction(record));
			this.sendBroadcast(DBAction.DELETE, id);
		}
		return wasFound && !this.exists(id);
	}

	/** {@inheritDoc} */
	@Override
	public boolean exists(final Key id) {
		final long count = this.realm.where(this.persistableClass)
				.equalTo(Persistable.ID, id.toString())
				.count();
		return count > 0L;
	}

	/** {@inheritDoc} */
	public void reset() {
		this.execute(new Realm.Transaction() {
			@Override
			public void execute(final Realm realmInstance) {
				realmInstance.delete(RealmDatabaseDao.this.persistableClass);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see #shouldCreate(Model)
	 */
	@Override
	public Model save(final Model model) {
		if (model == null) {
			throw new IllegalArgumentException("Can not save a null record.");
		}
		final Model result;
		if (this.shouldCreate(model)) {
			result = this.create(model);
		} else {
			result = this.update(model.getId(), model);
		}
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public SortedSet<Model> list() {
		final RealmResults<RealmModel> results = this.realm.where(this.persistableClass).findAll();
		final SortedSet<Model> models = new TreeSet<>();
		for (final RealmModel result : results) {
			models.add(this.modelFrom(result));
		}
		return Collections.unmodifiableSortedSet(models);
	}
	// endregion Dao<Key, Model>

	// region RealmConverter

	/**
	 * Creates a new {@link RealmModel} and then copies the given
	 * {@link Model} into it via {@link #copyModel(Object, Object)}.
	 *
	 * @param model from which a {@link RealmModel} is created.
	 * @return the newly created {@link RealmModel} with data populated
	 * from given {@link Model}.
	 */
	@Override
	public RealmModel persistableFrom(final Model model) {
		try {
			final RealmModel record = this.persistableClass.newInstance();
			return this.copyModel(model, record);
		} catch (IllegalAccessException | InstantiationException e) {
			throw new IllegalStateException(
					"RealmModel must have an accessible default constructor or subclass must override #persistableFrom(Model).",
					e
			);
		}
	}
	// endregion RealmConverter

	/**
	 * Execute a {@link Realm.Transaction} on the {@link Realm}
	 * used by this {@link Dao}. Will not throw an exception if the
	 * internal {@link Realm#isInTransaction()} but will instead run
	 * {@code transaction} within the current {@link Realm.Transaction}.
	 *
	 * @param transaction to execute.
	 * @throws io.realm.exceptions.RealmException if any error occurs during transaction.
	 * @see Realm#executeTransaction(Realm.Transaction)
	 */
	protected void execute(final Realm.Transaction transaction) {
		if (this.realm.isInTransaction()) {
			transaction.execute(this.realm);
		} else {
			this.realm.executeTransaction(transaction);
		}
	}

	/**
	 * Generate a value of type {@code Key} to be used as the id for the
	 * given {@code Model} object. This is called when a new {@code Model}
	 * instance is being saved into the data store.
	 *
	 * @param model for which an id is being generated.
	 * @return the id value to use for {@code model}.
	 */
	protected abstract Key generateId(Model model);

	/**
	 * Get a {@link RealmQuery} for the {@link Realm} used by this
	 * {@link Dao} in order to perform data specific queries.
	 *
	 * @return an open query for the {@link RealmModel} data.
	 */
	protected RealmQuery<RealmModel> query() {
		return this.realm.where(this.persistableClass);
	}

	/**
	 * Read a {@link RealmModel} from data storage by a primary key.
	 *
	 * @param id of the instance to retrieve.
	 * @return the persistable record associated with {@code id} if
	 * one exists, {@code null} otherwise.
	 */
	protected RealmModel readPersistable(final String id) {
		return this.realm.where(this.persistableClass)
				.equalTo(Persistable.ID, id)
				.findFirst()
				;
	}

	/**
	 * Transmit a {@link Intent} broadcast for a {@link DBAction}
	 * with the affected {@link Model}.
	 *
	 * @param action which occurred and may be broadcast.
	 * @param model for which {@code action} may be broadcast.
	 *
	 * @see #shouldBroadcast(DBAction, Model, Key)
	 */
	protected final void sendBroadcast(final DBAction action, final Model model) {
		if (this.shouldBroadcast(action, model, model.getId())) {
			action.sendBroadcast(this.modelClass, model);
		}
	}

	/**
	 * Transmit a {@link Intent} broadcast for a {@link DBAction}
	 * using the {@link Key} of the affected {@link Model}.
	 *
	 * @param action which occurred and may be broadcast.
	 * @param key for which {@code action} may be broadcast.
	 *
	 * @see #shouldBroadcast(DBAction, Model, Key)
	 */
	protected final void sendBroadcast(final DBAction action, final Key key) {
		if (this.shouldBroadcast(action, null, key)) {
			action.sendBroadcast(this.modelClass, key);
		}
	}

	/**
	 * Should {@link Intent} be broadcast for the given {@link DBAction}
	 * and {@link Model}.
	 *
	 * @param action which occurred and may be broadcast.
	 * @param model for which {@code action} may be broadcast. May be
	 *        {@code null}.
	 * @param key for {@code model}.
	 *
	 * @return {@code true} if an {@link Intent} should be broadcast,
	 *         {@code false} to prevent the broadcast.
	 */
	protected boolean shouldBroadcast(final DBAction action, final Model model, final Key key) {
		return true;
	}

	/**
	 * Return an indication of whether {@link Model} should be sent to
	 * {@link #create(Model)} or {@link #update(Key, Model)}.
	 *
	 * @param model to test.
	 * @return {@code true} if {@code model} needs to be created;
	 * {@code false} if {@code model} should be updated.
	 */
	protected boolean shouldCreate(final Model model) {
		return model.getId() == null || !this.exists(model.getId());
	}

	/**
	 * Call {@link DatedRecord#setUpdatedAt(java.util.Date)} with the current date
	 * if {@link RealmModel} is of type {@link DatedRecord}. Meant to be
	 * invoked between calls to {@link Realm#beginTransaction()} and
	 * {@link Realm#commitTransaction()} so {@code updatedAt} reflects
	 * the type of the last save from {@link Dao} methods.
	 *
	 * @param record for which {@code updatedAt} is to be set.
	 */
	private void setUpdatedAtFor(final RealmModel record) {
		if (this.usesDatedRecords) {
			((DatedRecord) record).setUpdatedAt(Calendar.getInstance().getTime());
		}
	}

	/**
	 * A {@link Realm.Transaction} implementation which copies a
	 * given {@link RealmObject} into the {@link Realm} upon which
	 * this {@code Transaction} is executed.
	 */
	protected static class CreateRecordTransaction implements Realm.Transaction {
		private final RealmObject record;

		protected CreateRecordTransaction(final RealmObject record) {
			this.record = record;
		}

		@Override
		public void execute(final Realm realm) {
			realm.copyToRealm(this.record);
		}
	}

	/**
	 * A {@link Realm.Transaction} implementation which deletes a given
	 * {@link RealmObject} from the {@link Realm} upon which this
	 * {@code Transaction} is executed.
	 */
	protected static class DeleteRecordTransaction implements Realm.Transaction {
		private final RealmObject record;

		protected DeleteRecordTransaction(final RealmObject record) {
			this.record = record;
		}

		@Override
		public void execute(final Realm realm) {
			this.record.deleteFromRealm();
		}
	}

	/**
	 * A {@link Realm.Transaction} implementation which uses the given
	 * {@link Model} to update the given {@link RealmModel} using
	 * the {@link #copyModel(Object, Object)} method.
	 */
	protected class UpdateRecordTransaction implements Realm.Transaction {
		private final Model model;
		private final RealmModel record;

		protected UpdateRecordTransaction(final Model model, final RealmModel record) {
			this.model = model;
			this.record = record;
		}

		@Override
		public void execute(final Realm ignored) {
			copyModel(this.model, this.record);
			setUpdatedAtFor(this.record);
		}
	}

}