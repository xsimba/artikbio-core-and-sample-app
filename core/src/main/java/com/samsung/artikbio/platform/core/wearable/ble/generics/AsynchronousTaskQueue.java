package com.samsung.artikbio.platform.core.wearable.ble.generics;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.LinkedBlockingQueue;

public class AsynchronousTaskQueue<T extends AsynchronousTaskQueue.Task> {

	public static final int TASK_STATE_IDLE = 0;
	public static final int TASK_STATE_RUNNING = 1;
	public static final int TASK_STATE_PENDING = 2;
	public static final int TASK_STATE_FINISHED = 3;
	public static final int TASK_STATUS_SUCCESS = 0;
	public static final int TASK_STATUS_ERROR = -1;
	public static final int TASK_STATUS_TIMEOUT = -2;
	private final LinkedBlockingQueue<T> mTaskLinkedQueue;
	private final Handler mTaskHandler;

	/**
	 * Default constructor, use the looper were this queue is instantiated to run the tasks.
	 */
	public AsynchronousTaskQueue () {
		this(null);
	}

	/**
	 * Constructor.
	 *
	 * @param looper the looper to use to execute the task. if null we use the looper where this
	 *               queue is instantiated.
	 */
	public AsynchronousTaskQueue (@Nullable Looper looper) {
		mTaskLinkedQueue = new LinkedBlockingQueue<>();
		if (looper != null) {
			mTaskHandler = new Handler(looper);
		} else {
			if (Looper.myLooper() == null) {
				Looper.prepare();
			}
			mTaskHandler = new Handler();
		}
	}

	/**
	 * Post a task to be run into the queue.
	 *
	 * @param task the task to be run.
	 *
	 * @return true if the task is successfully added to the queue.
	 */
	public synchronized boolean post (@NonNull T task) {
		task.setTaskContext(this, mTaskHandler);
		return mTaskLinkedQueue.add(task) && mTaskLinkedQueue.size() == 1 && mTaskHandler.post
				(task);
	}

	/**
	 * Clear all pending Task from the queue.
	 * If a Task is currently running let it finish its work.
	 */
	public synchronized void clear () {
		mTaskLinkedQueue.clear();
	}

	/**
	 * Get the first task of the queue.
	 *
	 * @return the first task of the queue, if none, return null
	 */
	@Nullable
	public synchronized T getCurrentTask () {
		return mTaskLinkedQueue.peek();
	}

	/**
	 * Handle the ending of a task.
	 */
	private synchronized void onTaskEnd () {
		// removing the first element of the queue.
		mTaskLinkedQueue.poll();
		// execute the next task in the queue.
		if (mTaskLinkedQueue.size() > 0) {
			Task task = mTaskLinkedQueue.peek();
			mTaskHandler.post(task);
		}
	}

	@Retention (RetentionPolicy.SOURCE)
	@IntDef ( {TASK_STATE_IDLE, TASK_STATE_PENDING, TASK_STATE_RUNNING, TASK_STATE_FINISHED})
	public @interface TaskState {
	}

	@Retention (RetentionPolicy.SOURCE)
	@IntDef ( {TASK_STATUS_SUCCESS, TASK_STATUS_ERROR, TASK_STATUS_TIMEOUT})
	public @interface TaskStatus {
	}

	/**
	 * Abstract class of a asynchronous task.
	 */
	public static abstract class Task implements Runnable {
		private AsynchronousTaskQueue mTaskLinkedQueue;
		private Handler mTaskHandler;
		private Runnable mTimeOutRunnable;
		@TaskState
		private int mState;
		private long mTimeOut;

		/**
		 * Default constructor.
		 *
		 * @param timeOut the time out value in ms
		 */
		public Task (long timeOut) {
			mState = TASK_STATE_IDLE;
			if (timeOut > 0) {
				mTimeOut = timeOut;
			} else {
				throw new RuntimeException("AsynchronousTaskQueue:Task : timeOut should be " +
						"superior than 0 but was : " + timeOut);
			}
		}

		/**
		 * For internal use only.
		 *
		 * @param taskLinkedQueue the queue we use to store those Task.
		 * @param handler         the handler we use to post those Task.
		 */
		protected void setTaskContext (AsynchronousTaskQueue taskLinkedQueue, Handler handler) {
			mState = TASK_STATE_PENDING;
			mTaskLinkedQueue = taskLinkedQueue;
			mTaskHandler = handler;
		}

		/**
		 * @return true if the task is currently running.
		 */
		public boolean isRunning () {
			return mState == TASK_STATE_RUNNING;
		}

		@Override
		public synchronized final void run () {
			// set the state of this task to running.
			mState = TASK_STATE_RUNNING;
			// set mIsRunning to the execute call return to tell this task still run or net after
			// this execute call, typically if an error occur during execute mIsRunning will be
			// set to false.
			if (execute()) {
				mTimeOutRunnable = () -> onTaskEnd(TASK_STATUS_TIMEOUT);
				mTaskHandler.postDelayed(mTimeOutRunnable, mTimeOut);
			} else {
				onTaskEnd(TASK_STATUS_ERROR);
			}
		}

		/**
		 * Override this method to do your stuff.
		 *
		 * @return true if the execute code goes well, false in case of error.
		 */
		public abstract boolean execute ();

		/**
		 * Fot a task to end you need to call this method.
		 *
		 * @param state the state of the task.
		 */
		public synchronized final void onTaskEnd (@TaskStatus int state) {
			// update the state.
			mState = TASK_STATE_FINISHED;
			// Remove timeout.
			mTaskHandler.removeCallbacks(mTimeOutRunnable);
			// Call the correct finnish method.
			switch (state) {
				case TASK_STATUS_SUCCESS:
					onTaskSucceed();
					break;
				case TASK_STATUS_TIMEOUT:
					onTaskTimeout();
					break;
				case TASK_STATUS_ERROR:
					onTaskError();
					break;
			}
			// handle the end of the task.
			mTaskLinkedQueue.onTaskEnd();
		}

		/**
		 * Override this method to do something after a successful return of your task.
		 */
		public abstract void onTaskSucceed ();

		/**
		 * Override this method to do something after a time out of your task.
		 */
		public abstract void onTaskTimeout ();

		/**
		 * Override this method to do something after an error of your task.
		 */
		public abstract void onTaskError ();


	}
}
