package com.samsung.artikbio.platform.core.persistance.model;

import com.samsung.artikbio.platform.core.persistance.datastore.LocalDataStore;

import net.jcip.annotations.Immutable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.UUID;

/**
 * Immutable representation of a UserIdentity that can be uniquely
 * identified by a {@link UUID}.
 */
@Immutable
public class UserIdentity implements Identifiable<UUID> {

	/** Unique identifier for this UserIdentity. */
	private final UUID uuid;

	private final String userId;

	private final String firstName;

	private final String lastName;

	private final String email;


	/**
	 * Creates an {@code UserIdentity} by providing all the values. Intended for use with
	 * {@link LocalDataStore#setCurrentUserIdentity(UserIdentity)}.
	 *
	 * If the {@code uuid} parameter is {@code null}, the other parameters will be used to create a
	 * new {@code UserIdentity}. A new {@code uuid} field will be generated in the process of
	 * setting this new UserIdentity to the {@link LocalDataStore}. If the {@code uuid} parameter
	 * equals the {@code uuid} of a {@code UserIdentity} that is already in the
	 * {@code LocalDataStore}, the other parameters will instead be used to update the fields of
	 * the
	 * existing {@code UserIdentity} entry.
	 *
	 * @param uuid        unique id for the user. This is internally generated as part of
	 *                    {@link LocalDataStore#setCurrentUserIdentity(UserIdentity)}.
	 * @param userId      External user id, usually obtained upon logging into the Cloud API.
	 * @param firstName   of the user
	 * @param lastName    of the user
	 * @param email       of the user
	 */
	public UserIdentity(final UUID uuid, final String userId, final String firstName,
	                    final String lastName, final String email) {
		this.uuid = uuid;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	/**
	 * Retrieves the user's UUID.
	 *
	 * @return UUID for user
	 */
	@Override
	public UUID getId() {
		return uuid;
	}

	/**
	 * Retrieves the user's ID.
	 *
	 * @return String identifier for user
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Retrieves the user's first name.
	 *
	 * @return String user's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Retrieves the user's last name.
	 *
	 * @return String user's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Retrieves the user's email address.
	 *
	 * @return String user's email address
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Equatable method.
	 *
	 * @param o - the object to be compared.
	 * @return boolean value, indicating whether the objects are equal
	 */
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final UserIdentity userIdentity = (UserIdentity) o;

		return new EqualsBuilder()
				.append(uuid, userIdentity.uuid)
				.append(userId, userIdentity.userId)
				.append(firstName, userIdentity.firstName)
				.append(lastName, userIdentity.lastName)
				.append(email, userIdentity.email)
				.isEquals();
	}

	/**
	 * Hashable method.
	 *
	 * @return integer value for hashcode
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(uuid)
				.append(userId)
				.append(firstName)
				.append(lastName)
				.append(email)
				.toHashCode();
	}
}