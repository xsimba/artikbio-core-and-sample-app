package com.samsung.artikbio.platform.core.wearable.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.samsung.artikbio.platform.core.util.ByteFormat;
import com.samsung.artikbio.platform.core.util.Constants;
import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.GattCode;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.ble.generics.AsynchronousTaskQueue;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Connection;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Status;

import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;


public class BLEConnection extends Connection {

	private final HashMap<UUID, List<SubscribeToCharacteristicCallback>>
			mSubscribeValueChangeCallbacks;
	private BluetoothGattCallback mBluetoothGattCallback;           // Android bluetooth gatt
	// callback.
	private AsynchronousTaskQueue<BLETask> mAsynchronousTaskQueue;  // The BLE task queue.
	private Context mContext;                                       // The application context
	private BluetoothManager mBluetoothManager;                     // The BluetoothManager.
	private BluetoothAdapter mBluetoothAdapter;                     // The BluetoothAdapter.
	private BluetoothGatt mBluetoothGatt;                           // The Bluetooth gatt bind to
	// the connection.
	private PairingState mPairingState;                             // The current paring state.
	private boolean mAutoConnect;                                    // The autoreconnect state.

	public BLEConnection (@NonNull WearableToken token) {
		this(token, null, null);
	}

	public BLEConnection (@NonNull WearableToken token, @Nullable Looper looper,
	                      @Nullable ContextProvider provider) {
		super(token);

		provider = provider != null ? provider : ContextProvider.getInstance();
		mSubscribeValueChangeCallbacks = new HashMap<>();
		mBluetoothGattCallback = buildBluetoothGattCallback();
		mAsynchronousTaskQueue = new AsynchronousTaskQueue<>(looper);

		mContext = provider.getApplicationContext();
		mBluetoothAdapter = provider.getBluetoothAdapter();
		mBluetoothManager = provider.getBluetoothManager();

		// Get the current pairing state
		Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();
		mPairingState = PairingState.INITIAL;
		for (BluetoothDevice bluetoothDevice : bondedDevices) {
			if (bluetoothDevice.getAddress().equals(token.getDeviceAddress())) {
				mPairingState = PairingState.PAIRED;
				break;
			}
		}

		if (mPairingState == PairingState.INITIAL) {
			mPairingState = PairingState.UNPAIRED;
		}
	}

	/**
	 * Builder of a BluetoothGattCallback instance.
	 * The instance is just use to root Android callback back to the appropriate task.
	 *
	 * @return a BluetoothGattCallback instance.
	 */
	protected BluetoothGattCallback buildBluetoothGattCallback () {
		return new BluetoothGattCallback() {
			@Override
			public void onConnectionStateChange (final BluetoothGatt gatt, final int status, final
			int newState) {
				super.onConnectionStateChange(gatt, status, newState);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onConnectionStateChange("
						+ logConnectBluetoothGatt(status) + ", " + logConnectionChangeState
						(newState) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onConnectionStateChange(gatt, status, newState);
				} else if (newState == BluetoothGatt.STATE_CONNECTED) {
					changeConnectionState(States.CONNECTED);
				} else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
					changeConnectionState(States.DISCONNECTED);
				}
			}

			@Override
			public void onServicesDiscovered (final BluetoothGatt gatt, final int status) {
				super.onServicesDiscovered(gatt, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onServicesDiscovered(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onServicesDiscovered(gatt, status);
				}
			}

			@Override
			public void onCharacteristicRead (final BluetoothGatt gatt, final
			BluetoothGattCharacteristic characteristic, final int status) {
				super.onCharacteristicRead(gatt, characteristic, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onCharacteristicRead(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onCharacteristicRead(gatt, characteristic, status);
				}
			}

			@Override
			public void onCharacteristicWrite (final BluetoothGatt gatt, final
			BluetoothGattCharacteristic characteristic, final int status) {
				super.onCharacteristicWrite(gatt, characteristic, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onCharacteristicWrite(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onCharacteristicWrite(gatt, characteristic, status);
				}
			}

			@Override
			public void onCharacteristicChanged (final BluetoothGatt gatt, final
			BluetoothGattCharacteristic characteristic) {
				super.onCharacteristicChanged(gatt, characteristic);

				// broadcast the value to the characteristic subscribed object
				UUID serviceId = characteristic.getService().getUuid();
				UUID characteristicId = characteristic.getUuid();
				byte[] value = characteristic.getValue();
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onCharacteristicChanged(" + ByteFormat.format(value) + "); " +
						"characteristic : " + characteristicId.toString());

				// broadcast method call to the current task
				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onCharacteristicChanged(gatt, characteristic);
				}

				synchronized (mSubscribeValueChangeCallbacks) {
					List<SubscribeToCharacteristicCallback> listeners =
							mSubscribeValueChangeCallbacks.get(characteristicId);
					if (listeners != null && listeners.size() > 0) {
						for (SubscribeToCharacteristicCallback callback : listeners) {
							callback.onValueChangeOfCharacteristic(serviceId, characteristicId,
									value);
						}
					}

				}
			}

			@Override
			public void onDescriptorRead (final BluetoothGatt gatt, final BluetoothGattDescriptor
					descriptor, final int status) {
				super.onDescriptorRead(gatt, descriptor, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onDescriptorRead(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onDescriptorRead(gatt, descriptor, status);
				}
			}

			@Override
			public void onDescriptorWrite (final BluetoothGatt gatt, final
			BluetoothGattDescriptor descriptor, final int status) {
				super.onDescriptorWrite(gatt, descriptor, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onDescriptorWrite(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onDescriptorWrite(gatt, descriptor, status);
				}
			}

			@Override
			public void onReliableWriteCompleted (final BluetoothGatt gatt, final int status) {
				super.onReliableWriteCompleted(gatt, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onReliableWriteCompleted(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onReliableWriteCompleted(gatt, status);
				}
			}

			@Override
			public void onReadRemoteRssi (final BluetoothGatt gatt, final int rssi, final int
					status) {
				super.onReadRemoteRssi(gatt, rssi, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onReadRemoteRssi(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onReadRemoteRssi(gatt, rssi, status);
				}
			}

			@Override
			public void onMtuChanged (final BluetoothGatt gatt, final int mtu, final int status) {
				super.onMtuChanged(gatt, mtu, status);
				Logger.v("BluetoothGattCallback[" + getWearableToken().getDeviceAddress() +
						"]->onMtuChanged(" + logBluetoothGatt(status) + ");");

				BLETask task = mAsynchronousTaskQueue.getCurrentTask();
				if (task != null) {
					task.onMtuChanged(gatt, mtu, status);
				}

			}
		};
	}

	public static String logConnectBluetoothGatt (int status) {
		switch (status) {
			case GattCode.GATT_CONN_SUCCESS:
				return "GATT_CONN_SUCCESS";
			case GattCode.GATT_CONN_L2C_FAILURE:
				return "GATT_CONN_L2C_FAILURE";
			case GattCode.GATT_CONN_TIMEOUT:
				return "GATT_CONN_TIMEOUT";
			case GattCode.GATT_CONN_TERMINATE_PEER_USER:
				return "GATT_CONN_TERMINATE_PEER_USER";
			case GattCode.GATT_CONN_TERMINATE_LOCAL_HOST:
				return "GATT_CONN_TERMINATE_LOCAL_HOST";
			case GattCode.GATT_CONN_FAIL_ESTABLISH:
				return "GATT_CONN_FAIL_ESTABLISH";
			case GattCode.GATT_CONN_LMP_TIMEOUT:
				return "GATT_CONN_LMP_TIMEOUT";
			case GattCode.GATT_CONN_ERROR:
				return "GATT_CONN_ERROR";
			case GattCode.GATT_CONN_CANCEL:
				return "GATT_CONN_CANCEL";
			default:
				return "UNKNOWN";
		}
	}

	public static String logConnectionChangeState (int status) {
		switch (status) {
			case BluetoothProfile.STATE_DISCONNECTED:
				return "STATE_DISCONNECTED";
			case BluetoothProfile.STATE_CONNECTING:
				return "STATE_CONNECTING";
			case BluetoothProfile.STATE_CONNECTED:
				return "STATE_CONNECTED";
			case BluetoothProfile.STATE_DISCONNECTING:
				return "STATE_DISCONNECTING";
			default:
				return "NULL";
		}
	}

	@Override
	protected void changeConnectionState (final States connectionState) {
		super.changeConnectionState(connectionState);
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() +
				"]->changeConnectionState(" + connectionState + ");");
		if (connectionState == States.DISCONNECTED) {
			mSubscribeValueChangeCallbacks.clear();
		}
	}

	public static String logBluetoothGatt (int status) {
		switch (status) {
			case GattCode.GATT_SUCCESS:
				return "GATT_SUCCESS";
			case GattCode.GATT_INVALID_HANDLE:
				return "GATT_INVALID_HANDLE";
			case GattCode.GATT_READ_NOT_PERMIT:
				return "GATT_READ_NOT_PERMIT";
			case GattCode.GATT_WRITE_NOT_PERMIT:
				return "GATT_WRITE_NOT_PERMIT";
			case GattCode.GATT_INVALID_PDU:
				return "GATT_INVALID_PDU";
			case GattCode.GATT_INSUF_AUTHENTICATION:
				return "GATT_INSUF_AUTHENTICATION";
			case GattCode.GATT_REQ_NOT_SUPPORTED:
				return "GATT_REQ_NOT_SUPPORTED";
			case GattCode.GATT_INVALID_OFFSET:
				return "GATT_INVALID_OFFSET";
			case GattCode.GATT_INSUF_AUTHORIZATION:
				return "GATT_INSUF_AUTHORIZATION";
			case GattCode.GATT_PREPARE_Q_FULL:
				return "GATT_PREPARE_Q_FULL";
			case GattCode.GATT_NOT_FOUND:
				return "GATT_NOT_FOUND";
			case GattCode.GATT_NOT_LONG:
				return "GATT_NOT_LONG";
			case GattCode.GATT_INSUF_KEY_SIZE:
				return "GATT_INSUF_KEY_SIZE";
			case GattCode.GATT_INVALID_ATTR_LEN:
				return "GATT_INVALID_ATTR_LEN";
			case GattCode.GATT_ERR_UNLIKELY:
				return "GATT_ERR_UNLIKELY";
			case GattCode.GATT_INSUF_ENCRYPTION:
				return "GATT_INSUF_ENCRYPTION";
			case GattCode.GATT_UNSUPPORT_GRP_TYPE:
				return "GATT_UNSUPPORT_GRP_TYPE";
			case GattCode.GATT_INSUF_RESOURCE:
				return "GATT_INSUF_RESOURCE";
			case GattCode.GATT_ILLEGAL_PARAMETER:
				return "GATT_ILLEGAL_PARAMETER";
			case GattCode.GATT_NO_RESOURCES:
				return "GATT_NO_RESOURCES";
			case GattCode.GATT_INTERNAL_ERROR:
				return "GATT_INTERNAL_ERROR";
			case GattCode.GATT_WRONG_STATE:
				return "GATT_WRONG_STATE";
			case GattCode.GATT_DB_FULL:
				return "GATT_DB_FULL";
			case GattCode.GATT_BUSY:
				return "GATT_BUSY";
			case GattCode.GATT_ERROR:
				return "GATT_ERROR";
			case GattCode.GATT_CMD_STARTED:
				return "GATT_CMD_STARTED";
			case GattCode.GATT_PENDING:
				return "GATT_PENDING";
			case GattCode.GATT_AUTH_FAIL:
				return "GATT_AUTH_FAIL";
			case GattCode.GATT_MORE:
				return "GATT_MORE";
			case GattCode.GATT_INVALID_CFG:
				return "GATT_INVALID_CFG";
			case GattCode.GATT_SERVICE_STARTED:
				return "GATT_SERVICE_STARTED";
			case GattCode.GATT_ENCRYPTED_NO_MITM:
				return "GATT_ENCRYPTED_NO_MITM";
			case GattCode.GATT_NOT_ENCRYPTED:
				return "GATT_NOT_ENCRYPTED";
			default:
				return "UNKNOWN";
		}
	}

	////////////////////////////////////////
	// IConnectable

	public BLEConnection (@NonNull WearableToken token, @Nullable Looper looper) {
		this(token, looper, null);
	}

	private static String logBondState (int status) {
		switch (status) {
			case BluetoothDevice.BOND_BONDED:
				return "BOND_BONDED";
			case BluetoothDevice.BOND_BONDING:
				return "BOND_BONDING";
			case BluetoothDevice.BOND_NONE:
				return "BOND_NONE";
			default:
				return "NULL";
		}
	}

	public PairingState getPairState () {
		return mPairingState;
	}

	public boolean getAutoconnect () {
		return mAutoConnect;
	}

	////////////////////////////////////////
	// ISubscribable

	protected void changePairState (PairingState pairingState) {
		mPairingState = pairingState;
	}

	@Override
	public boolean connect (final boolean autoConnect, final ConnectCallback callback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->connect();");
		mAutoConnect = autoConnect;

		// 2° Report successful connection after discovering services
		final DiscoverServiceCallback serviceCallback = (services, status) -> {

			Logger.d("BLEConnection[" + getWearableToken().getDeviceAddress() +
					"]::onDiscoverService() - " + status);
			Logger.d("BLEConnection[" + getWearableToken().getDeviceAddress() + "]::connect() " +
					"- " + (services != null ? services.toString() : "NULL"));
			if (status == Status.SUCCESS) {
				changeConnectionState(States.CONNECTED);
			}
			callback.onConnect(status);
		};

		// 1° we need to call discover service to get available ones after the BLE connection.
		final ConnectCallback connectCallback = status -> {
			if (status == Status.SUCCESS) {
				Logger.d("BLEConnection[" + getWearableToken().getDeviceAddress() +
						"]::connect() - Discovering services.");
				discoverServices(false, serviceCallback);
			} else if (callback != null) {
				callback.onConnect(status);
			}
		};

		if (getConnectionState() != States.CONNECTED && getConnectionState() != States
				.CONNECTING) {
			Logger.d("BLEConnection[" + getWearableToken().getDeviceAddress() + "]::connect() - " +
					"Ble connection in progress.");
			final ConnectTask connectTask = new ConnectTask(getWearableToken().getDeviceAddress(),
					mAutoConnect, connectCallback);
			return mAsynchronousTaskQueue.post(connectTask);
		}
		return false;
	}

	@Override
	public boolean disconnect (final DisconnectCallback callback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->disconnect();");
		if (getConnectionState() != States.DISCONNECTED && getConnectionState() != States
				.DISCONNECTING) {
			final DisconnectTask disconnectTask = new DisconnectTask(callback);
			return mAsynchronousTaskQueue.post(disconnectTask);
		}
		return false;
	}

	////////////////////////////////////////
	// Specific BLE
	public boolean discoverServices (final boolean invalidateCache, final DiscoverServiceCallback
			discoverServiceCallback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->discoverServices" +
				"()" +
				"; invalidateCache: " + invalidateCache);
		DiscoverServicesTask discoverServicesTask = new DiscoverServicesTask(invalidateCache,
				discoverServiceCallback);
		return mAsynchronousTaskQueue.post(discoverServicesTask);
	}

	@Override
	public boolean subscribeToCharacteristic (final UUID service, final UUID characteristic,
	                                          final SubscribeToCharacteristicType subscribeType,
	                                          final SubscribeToCharacteristicCallback callback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->subscribe(); " +
				"Service : " + service + " characteristic: " + characteristic + " type:" +
				subscribeType);
		final SubscribeToCharacteristicTask subscribeTask = new SubscribeToCharacteristicTask
				(service, characteristic, subscribeType, callback);
		return mAsynchronousTaskQueue.post(subscribeTask);
	}

	@Override
	public boolean unsubscribeToCharacteristic (final UUID service, final UUID characteristic,
	                                            final UnsubscribeToCharacteristicCallback
			                                            callback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->unsubscribe(); " +
				"Service : " + service + " characteristic: " + characteristic);
		final UnsubscribeToCharacteristicTask unsubscribeTask = new
				UnsubscribeToCharacteristicTask(service, characteristic, BluetoothGattDescriptor
				.DISABLE_NOTIFICATION_VALUE, callback);
		return mAsynchronousTaskQueue.post(unsubscribeTask);
	}

	////////////////////////////////////////
	// IReadable
	@Override
	public boolean readCharacteristic (final UUID service, final UUID characteristic, final
	ReadCharacteristicCallback callback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->read(); Service " +
				":" +
				" " +
				"" + service + " characteristic: " + characteristic);
		final ReadCharacteristicTask readTask = new ReadCharacteristicTask(service, characteristic,
				callback);
		return mAsynchronousTaskQueue.post(readTask);
	}

	////////////////////////////////////////
	// IWritable
	@Override
	public boolean writeCharacteristic (final UUID service, final UUID characteristic, final
	byte[] data, final WriteCharacteristicCallback callback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->write(); Service" +
				" " +
				":" +
				" " + service + " characteristic: " + characteristic + "data: " + ByteFormat
				.format(data));
		final WriteCharacteristicTask writeTask = new WriteCharacteristicTask(service,
				characteristic, data, callback);
		return mAsynchronousTaskQueue.post(writeTask);
	}

	public boolean pair (final PairCallback pairCallback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->pair();");
		if (mPairingState != PairingState.PAIRED && mPairingState != PairingState.PAIRING) {
			PairingToDeviceTask pairTask = new PairingToDeviceTask(getWearableToken()
					.getDeviceAddress(), pairCallback);
			return mAsynchronousTaskQueue.post(pairTask);
		}
		return false;
	}

	////////////////////////////////////////
	// Interfaces

	public boolean unpair (final UnpairCallback unpairCallback) {
		Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->unpair();");
		if (mPairingState != PairingState.UNPAIRED && mPairingState != PairingState.UNPAIRING) {
			final UnpairingFromDeviceTask unpairTask = new UnpairingFromDeviceTask
					(getWearableToken().getDeviceAddress(), unpairCallback);
			return mAsynchronousTaskQueue.post(unpairTask);
		}
		return false;
	}

	public boolean requestHighConnectionPriority () {
		return mBluetoothGatt != null && mBluetoothGatt.requestConnectionPriority(BluetoothGatt
				.CONNECTION_PRIORITY_HIGH);
	}

	public boolean requestBalancedConnectionPriority () {
		return mBluetoothGatt != null && mBluetoothGatt.requestConnectionPriority(BluetoothGatt
				.CONNECTION_PRIORITY_BALANCED);
	}

	public boolean requestLowPowerConnectionPirority () {
		return mBluetoothGatt != null && mBluetoothGatt.requestConnectionPriority(BluetoothGatt
				.CONNECTION_PRIORITY_LOW_POWER);
	}

	////////////////////////////////////////
	// BLETask

	public enum PairingState {
		INITIAL,
		PAIRING,
		PAIRED,
		UNPAIRING,
		UNPAIRED
	}

	public interface PairCallback {
		void onPair (Status status);
	}

	public interface UnpairCallback {
		void onUnpair (Status status);
	}

	public interface DiscoverServiceCallback {
		void onDiscoverService (@Nullable List<UUID> services, Status status);
	}

	/**
	 * BLETask extend the standard AsynchronousTaskQueue.Task to add BluetoothGattCallback
	 * call back methods.
	 */
	private abstract class BLETask extends AsynchronousTaskQueue.Task {

		public BLETask (long timeOut) {
			super(timeOut);
		}

		/**
		 * Callback indicating when GATT client has connected/disconnected to/from a remote
		 * GATT server.
		 *
		 * @param gatt     GATT client
		 * @param status   Status of the connect or disconnect operation. {@link
		 *                 BluetoothGatt#GATT_SUCCESS} if the operation succeeds.
		 * @param newState Returns the new connection state. Can be one of {@link
		 *                 BluetoothProfile#STATE_DISCONNECTED} or
		 *                 {@link BluetoothProfile#STATE_CONNECTED}
		 */
		public void onConnectionStateChange (BluetoothGatt gatt, int status, int newState) {
			Logger.w(getClass().getSimpleName() + "::onConnectionStateChange() call not cached " +
					"!!");
		}

		/**
		 * Callback invoked when the list of remote services, characteristics and descriptors
		 * for the remote device have been updated, ie new services have been discovered.
		 *
		 * @param gatt   GATT client invoked {@link BluetoothGatt#discoverServices}
		 * @param status {@link BluetoothGatt#GATT_SUCCESS} if the remote device has been explored
		 *               successfully.
		 */
		public void onServicesDiscovered (BluetoothGatt gatt, int status) {
			Logger.w(getClass().getSimpleName() + "::onServicesDiscovered() call not cached !!");
		}

		/**
		 * Callback reporting the result of a characteristic readCharacteristic operation.
		 *
		 * @param gatt           GATT client invoked {@link BluetoothGatt#readCharacteristic}
		 * @param characteristic Characteristic that was readCharacteristic from the associated
		 *                       remote device.
		 * @param status         {@link BluetoothGatt#GATT_SUCCESS} if the readCharacteristic
		 *                       operation was completed successfully.
		 */
		public void onCharacteristicRead (BluetoothGatt gatt, BluetoothGattCharacteristic
				characteristic, int status) {
			Logger.w(getClass().getSimpleName() + "::onCharacteristicRead() call not cached !!");
		}

		/**
		 * Callback indicating the result of a characteristic writeCharacteristic operation.
		 * <p>
		 * <p>If this callback is invoked while a reliable writeCharacteristic transaction is
		 * in progress, the value of the characteristic represents the value
		 * reported by the remote device. An application should compare this
		 * value to the desired value to be written. If the values don't match,
		 * the application must abort the reliable writeCharacteristic transaction.
		 *
		 * @param gatt           GATT client invoked {@link BluetoothGatt#writeCharacteristic}
		 * @param characteristic Characteristic that was written to the associated remote device.
		 * @param status         The result of the writeCharacteristic operation {@link
		 *                       BluetoothGatt#GATT_SUCCESS} if the operation succeeds.
		 */
		public void onCharacteristicWrite (BluetoothGatt gatt, BluetoothGattCharacteristic
				characteristic, int status) {
			Logger.w(getClass().getSimpleName() + "::onCharacteristicWrite() call not cached !!");
		}

		/**
		 * Callback triggered as a result of a remote characteristic notification.
		 *
		 * @param gatt           GATT client the characteristic is associated with
		 * @param characteristic Characteristic that has been updated as a result of a remote
		 *                       notification event.
		 */
		public void onCharacteristicChanged (BluetoothGatt gatt, BluetoothGattCharacteristic
				characteristic) {
			Logger.w(getClass().getSimpleName() + "::onCharacteristicChanged() call not cached " +
					"!!" +
					" " +
					ArrayUtils.toString(characteristic.getValue()));
		}

		/**
		 * Callback reporting the result of a descriptor readCharacteristic operation.
		 *
		 * @param gatt       GATT client invoked {@link BluetoothGatt#readDescriptor}
		 * @param descriptor Descriptor that was readCharacteristic from the associated remote
		 *                   device.
		 * @param status     {@link BluetoothGatt#GATT_SUCCESS} if the readCharacteristic operation
		 *                   was completed successfully
		 */
		public void onDescriptorRead (BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int
				status) {
			Logger.w(getClass().getSimpleName() + "::onDescriptorRead() call not cached !!");
		}

		/**
		 * Callback indicating the result of a descriptor writeCharacteristic operation.
		 *
		 * @param gatt       GATT client invoked {@link BluetoothGatt#writeDescriptor}
		 * @param descriptor Descriptor that was writte to the associated remote device.
		 * @param status     The result of the writeCharacteristic operation {@link
		 *                   BluetoothGatt#GATT_SUCCESS } if the operation succeeds.
		 */
		public void onDescriptorWrite (BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int
				status) {
			Logger.w(getClass().getSimpleName() + "::onDescriptorWrite() call not cached !!");
		}

		/**
		 * Callback invoked when a reliable writeCharacteristic transaction has been completed.
		 *
		 * @param gatt   GATT client invoked {@link BluetoothGatt#executeReliableWrite}
		 * @param status {@link BluetoothGatt#GATT_SUCCESS} if the reliable writeCharacteristic
		 *               transaction was executed successfully
		 */
		public void onReliableWriteCompleted (BluetoothGatt gatt, int status) {
			Logger.w(getClass().getSimpleName() + "::onReliableWriteCompleted() call not cached " +
					"!!");
		}

		/**
		 * Callback reporting the RSSI for a remote device connection.
		 * <p>
		 * This callback is triggered in response to the
		 * {@link BluetoothGatt#readRemoteRssi} function.
		 *
		 * @param gatt   GATT client invoked {@link BluetoothGatt#readRemoteRssi}
		 * @param rssi   The RSSI value for the remote device
		 * @param status {@link BluetoothGatt#GATT_SUCCESS} if the RSSI was readCharacteristic
		 *               successfully
		 */
		public void onReadRemoteRssi (BluetoothGatt gatt, int rssi, int status) {
			Logger.w(getClass().getSimpleName() + "::onReadRemoteRssi() call not cached !!");
		}

		/**
		 * Callback indicating the MTU for a given device connection has changed.
		 * <p>
		 * This callback is triggered in response to the
		 * {@link BluetoothGatt#requestMtu} function, or in response to a connection
		 * event.
		 *
		 * @param gatt   GATT client invoked {@link BluetoothGatt#requestMtu}
		 * @param mtu    The new MTU size
		 * @param status {@link BluetoothGatt#GATT_SUCCESS} if the MTU has been changed
		 *               successfully
		 */
		public void onMtuChanged (BluetoothGatt gatt, int mtu, int status) {
			Logger.w(getClass().getSimpleName() + "::onMtuChanged() call not cached !!");
		}
	}

	/**
	 * Connection task.
	 */
	private class ConnectTask extends BLETask {

		private final ConnectCallback mCallback;
		private final String mDeviceAddress;
		private final boolean mAutoConnect;

		public ConnectTask (final @NonNull String deviceAddress, final boolean autoConnect, final
		ConnectCallback callback) {
			super(ContextProvider.getInstance().getConfig()
					.BLE_CONNECTION_CONNECTION_TIMEOUT_DELAY);
			mDeviceAddress = deviceAddress;
			mAutoConnect = autoConnect;
			mCallback = callback;
		}

		@Override
		public boolean execute () {
			if (mBluetoothAdapter != null) {
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mDeviceAddress);
				if (device != null) {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() +
							"]->ConnectTask.execute() - CONNECTION on : "
							+ getWearableToken().getDeviceAddress());

					int currentState = mBluetoothManager.getConnectionState(device,
							BluetoothProfile.GATT);

					if (currentState == BluetoothProfile.STATE_CONNECTED) {
						onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
					}

					mBluetoothGatt = device.connectGatt(mContext, mAutoConnect,
							mBluetoothGattCallback);

					if (mBluetoothGatt != null) {
						changeConnectionState(States.CONNECTING);
						return true;
					}
				}
			}
			return false;
		}

		@Override
		public void onConnectionStateChange (final BluetoothGatt gatt, final int status, final int
				newState) {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() +
					"]-> ConnectTask.onConnectionStateChange(..., "
					+ logConnectBluetoothGatt(status) + ", " +
					logConnectionChangeState(newState) + ") - : ");
			if (status == BluetoothGatt.GATT_SUCCESS) {
				if (newState == BluetoothProfile.STATE_CONNECTED) {
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
				} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
				}
			} else {
				onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
			}
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> ConnectTask" +
					".onTaskTimeout()");
			changeConnectionState(States.DISCONNECTED);
			if (mCallback != null) {
				mCallback.onConnect(Status.TIMEOUT);
			}
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> ConnectTask" +
					".onTaskError()");
			changeConnectionState(States.DISCONNECTED);
			if (mCallback != null) {
				mCallback.onConnect(Status.ERROR);
			}
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> ConnectTask" +
					".onTaskSucceed()");
			//changeConnectionState(States.CONNECTED);
			if (mCallback != null) {
				mCallback.onConnect(Status.SUCCESS);
			}
		}
	}

	/**
	 * Disconnection task.
	 */
	private class DisconnectTask extends BLETask {

		private final DisconnectCallback mCallback;

		public DisconnectTask (final DisconnectCallback callback) {
			super(ContextProvider.getInstance().getConfig()
					.BLE_CONNECTION_DISCONNECTION_TIMEOUT_DELAY);
			mCallback = callback;
		}

		@Override
		public boolean execute () {
			if (mBluetoothGatt != null) {
				Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() +
						"]->DisconnectTask.disconnect()");
				mBluetoothGatt.disconnect();
				changeConnectionState(States.DISCONNECTING);
				return true;
			}
			return false;
		}

		@Override
		public void onConnectionStateChange (final BluetoothGatt gatt, final int status, final int
				newState) {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() +
					"]->DisconnectTask" +
					".onConnectionStateChange(..., "
					+ logConnectionChangeState(status) + ", " + logConnectionChangeState(newState)
					+ ") - : ");
			if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile
					.STATE_DISCONNECTED) {
				onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
			} else {
				onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
			}
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"DisconnectTask.onTaskSucceed()");
			terminateConnection();
			if (mCallback != null) {
				mCallback.onDisconnect(Status.SUCCESS);
			}
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"DisconnectTask.onTaskError()");
			terminateConnection();
			if (mCallback != null) {
				mCallback.onDisconnect(Status.ERROR);
			}
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"DisconnectTask.onTaskTimeout()");
			terminateConnection();
			if (mCallback != null) {
				mCallback.onDisconnect(Status.TIMEOUT);
			}
		}

		private void terminateConnection () {
			Logger.v("Terminating BLEConnection[" + getWearableToken().getDeviceAddress() + "]");
			// refresh the connection state.
			changeConnectionState(States.DISCONNECTED);

			// A disconnection happened, clear the BLE tasks queue
			mAsynchronousTaskQueue.clear();

			// close the connection.
			if (mBluetoothGatt != null) {
				mBluetoothGatt.close();
				mBluetoothGatt = null;
			}
		}
	}

	/**
	 * DiscoverServiceTask that bind a mBluetoothGatt.discoverServices() call.
	 */
	private class DiscoverServicesTask extends BLETask {
		private DiscoverServiceCallback mCallback;
		private boolean mInvalidateCache;
		private List<UUID> mServices;

		public DiscoverServicesTask (final boolean invalidateCache, DiscoverServiceCallback
				callback) {
			super(ContextProvider.getInstance().getConfig()
					.BLE_CONNECTION_DISCOVER_SERVICES_TIMEOUT_DELAY);
			mCallback = callback;
			mInvalidateCache = invalidateCache;
		}

		@Override
		public boolean execute () {
			// TODO: 5/22/17 If for some reason connection fails before discover services
			// then service discovery would fail too
			if (/*getConnectionState() == States.CONNECTED &&*/ mBluetoothGatt != null) {
				if (mInvalidateCache) {
					refreshDeviceCache(mBluetoothGatt);
				}
				return mBluetoothGatt.discoverServices();
			} else {
				return false;
			}
		}

		private boolean refreshDeviceCache (final BluetoothGatt bluetoothGatt) {
			//http://stackoverflow.com/questions/22596951/how-to-programmatically-force-bluetooth
			// -low-energy-service-discovery-on-android
			try {
				Method localMethod = bluetoothGatt.getClass().getMethod("refresh");
				if (localMethod != null) {
					return (boolean) localMethod.invoke(bluetoothGatt);
				}
			} catch (Exception e) {
				Logger.e("An exception occured while refreshing device");
			}
			return false;
		}

		@Override
		public void onServicesDiscovered (final BluetoothGatt gatt, final int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				// build the services list
				mServices = new ArrayList<>();
				for (BluetoothGattService service : gatt.getServices()) {
					mServices.add(service.getUuid());
				}
				onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
			} else {
				onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
			}
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"DiscoverServicesTask.onTaskSucceed()");
			if (mCallback != null) {
				mCallback.onDiscoverService(mServices, Status.SUCCESS);
			}
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"DiscoverServicesTask.onTaskTimeout()");
			if (mCallback != null) {
				mCallback.onDiscoverService(null, Status.TIMEOUT);
			}
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"DiscoverServicesTask.onTaskError()");
			if (mCallback != null) {
				mCallback.onDiscoverService(null, Status.ERROR);
			}
		}
	}

	/**
	 * Read task.
	 */
	private class ReadCharacteristicTask extends BLETask {

		private final UUID mService;
		private final UUID mCharacteristic;
		private final ReadCharacteristicCallback mCallback;
		private byte[] mReadValue;

		public ReadCharacteristicTask (final @NonNull UUID service, final @NonNull UUID
				characteristic, final ReadCharacteristicCallback callback) {
			super(ContextProvider.getInstance().getConfig().BLE_CONNECTION_READ_TIMEOUT_DELAY);
			mService = service;
			mCharacteristic = characteristic;
			mCallback = callback;
		}

		@Override
		public boolean execute () {
			if (mBluetoothGatt != null) {
				BluetoothGattService gattService = mBluetoothGatt.getService(mService);
				if (gattService != null) {
					BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic
							(mCharacteristic);
					if (gattCharacteristic != null) {
						Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
								getClass().getSimpleName() + ".readCharacteristic() " +
								" Service : " + mService.toString() +
								" Characteristic : " + mCharacteristic.toString());
						return mBluetoothGatt.readCharacteristic(gattCharacteristic);
					}
				}
			}
			return false;
		}

		@Override
		public void onCharacteristicRead (final BluetoothGatt gatt, final
		BluetoothGattCharacteristic characteristic, final int status) {
			if (mCharacteristic.equals(characteristic.getUuid())) {
				if (status == BluetoothGatt.GATT_SUCCESS) {
					mReadValue = characteristic.getValue();
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
							getClass().getSimpleName() + ".onCharacteristicRead() : " + ByteFormat
							.format(mReadValue));
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
				} else {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
							getClass().getSimpleName() + ".onCharacteristicRead() : NULL");
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
				}
			}
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"ReadCharacteristicTask" +
					".onTaskSucceed()");
			if (mCallback != null) {
				mCallback.onReadCharacteristic(mService, mCharacteristic, mReadValue, Status
						.SUCCESS);
			}
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"ReadCharacteristicTask" +
					".onTaskTimeout()");
			if (mCallback != null) {
				mCallback.onReadCharacteristic(mService, mCharacteristic, null, Status
						.TIMEOUT);
			}
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"ReadCharacteristicTask" +
					".onTaskError()");
			if (mCallback != null) {
				mCallback.onReadCharacteristic(mService, mCharacteristic, null, Status
						.ERROR);
			}
		}
	}

	/**
	 * Write task.
	 */
	private class WriteCharacteristicTask extends BLETask {
		private final WriteCharacteristicCallback mCallback;
		private final UUID mService;
		private final UUID mCharacteristic;
		private final byte[] mWriteValue;

		public WriteCharacteristicTask (final @NonNull UUID service, final @NonNull UUID
				characteristic, final byte[] value, final WriteCharacteristicCallback callback) {
			super(ContextProvider.getInstance().getConfig().BLE_CONNECTION_WRITE_TIMEOUT_DELAY);
			mService = service;
			mCharacteristic = characteristic;
			mWriteValue = value;
			mCallback = callback;
		}

		@Override
		public boolean execute () {
			if (mBluetoothGatt != null) {
				BluetoothGattService gattService = mBluetoothGatt.getService(mService);
				if (gattService != null) {
					BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic
							(mCharacteristic);
					if (gattCharacteristic != null) {
						gattCharacteristic.setWriteType(mCallback != null ?
								BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT :
								BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
						gattCharacteristic.setValue(mWriteValue);
						Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
								" " +
								"WriteCharacteristicTask{@" + Integer.toHexString(System
								.identityHashCode
										(this)) + "}.writeCharacteristic() : " + ByteFormat.format
								(mWriteValue));
						return mBluetoothGatt.writeCharacteristic(gattCharacteristic);
					}
				}
			}
			return false;
		}

		@Override
		public void onCharacteristicWrite (final BluetoothGatt gatt, final
		BluetoothGattCharacteristic characteristic, final int status) {
			if (mCharacteristic.equals(characteristic.getUuid())) {
				if (status == BluetoothGatt.GATT_SUCCESS) {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
							"WriteCharacteristicTask{@" + Integer.toHexString(System
							.identityHashCode(this))
							+ "}.onCharacteristicWrite() -> SUCCESS");
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
				} else {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
							"WriteCharacteristicTask{@" + Integer.toHexString(System
							.identityHashCode(this))
							+ "}.onCharacteristicWrite() -> FAILURE");
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
				}
			}
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"WriteCharacteristicTask{@" + Integer.toHexString(System.identityHashCode
					(this)) + "}" +
					".onTaskSucceed() for : " + ByteFormat.format(mWriteValue));
			if (mCallback != null) {
				mCallback.onWriteCharacteristic(mService, mCharacteristic, mWriteValue, Status
						.SUCCESS);
			}
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"WriteCharacteristicTask{@" + Integer.toHexString(System.identityHashCode
					(this)) + "}" +
					".onTaskTimeout() for : " + ByteFormat.format(mWriteValue));
			if (mCallback != null) {
				mCallback.onWriteCharacteristic(mService, mCharacteristic, mWriteValue, Status
						.TIMEOUT);
			}
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"WriteCharacteristicTask{@" + Integer.toHexString(System.identityHashCode
					(this)) + "}" +
					".onTaskError() for : " + ByteFormat.format(mWriteValue));
			if (mCallback != null) {
				mCallback.onWriteCharacteristic(mService, mCharacteristic, mWriteValue, Status
						.ERROR);
			}
		}
	}

	////////////////////////////////////////
	// BroadcastReceiver for Bonding call

	/**
	 * Subscribtion task.
	 */
	private class SubscribeToCharacteristicTask extends BLETask {

		private final UUID mService;
		private final UUID mCharacteristic;
		private final SubscribeToCharacteristicType mSubscribeType;
		private final SubscribeToCharacteristicCallback mCallback;

		public SubscribeToCharacteristicTask (final @NonNull UUID service, final @NonNull UUID
				characteristic, final SubscribeToCharacteristicType subscribeType, final @NonNull
				                                      SubscribeToCharacteristicCallback callback) {
			super(ContextProvider.getInstance().getConfig()
					.BLE_CONNECTION_SUBSCRIBE_TIMEOUT_DELAY);
			mService = service;
			mCharacteristic = characteristic;
			mSubscribeType = subscribeType;
			mCallback = callback;
		}

		@Override
		public boolean execute () {
			if (mBluetoothGatt != null) {
				BluetoothGattService gattService = mBluetoothGatt.getService(mService);
				if (gattService != null) {
					BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic
							(mCharacteristic);
					if (gattCharacteristic != null) {
						gattCharacteristic.setWriteType(BluetoothGattCharacteristic
								.WRITE_TYPE_DEFAULT);
						mBluetoothGatt.setCharacteristicNotification(gattCharacteristic, true);
						BluetoothGattDescriptor gattDescriptor = gattCharacteristic.getDescriptor
								(Constants.CLIENT_CHARACTERISTIC_CONFIG_UUID);
						switch (mSubscribeType) {
							case NOTIFICATION:
								gattDescriptor.setValue(BluetoothGattDescriptor
										.ENABLE_NOTIFICATION_VALUE);
								break;
							case INDICATION:
								gattDescriptor.setValue(BluetoothGattDescriptor
										.ENABLE_INDICATION_VALUE);
								break;
						}
						Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
								getClass().getSimpleName() + ".writeDescriptor()" +
								" Service : " + mService.toString() +
								" Characteristic : " + mCharacteristic.toString());
						return mBluetoothGatt.writeDescriptor(gattDescriptor);
					}
				}
			}
			return false;
		}

		@Override
		public void onDescriptorWrite (final BluetoothGatt gatt, final BluetoothGattDescriptor
				descriptor, final int status) {
			if (mCharacteristic.equals(descriptor.getCharacteristic().getUuid())) {
				if (status == BluetoothGatt.GATT_SUCCESS) {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
							getClass().getSimpleName() + ".onDescriptorWrite() -> SUCCESS");
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
				} else {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
							getClass().getSimpleName() + ".onDescriptorWrite() -> FAILURE");
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
				}
			}
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"SubscribeToCharacteristicTask" +
					".onTaskSucceed()");
			if (mCallback != null) {
				List<SubscribeToCharacteristicCallback> listeners = mSubscribeValueChangeCallbacks
						.get(mCharacteristic);
				if (listeners == null) {
					mSubscribeValueChangeCallbacks.put(mCharacteristic, listeners = new
							ArrayList<>());
				}
				listeners.add(mCallback);
				mCallback.onSubscribeToCharacteristic(mService, mCharacteristic, mSubscribeType,
						Status.SUCCESS);
			}
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"SubscribeToCharacteristicTask" +
					".onTaskTimeout()");
			if (mCallback != null) {
				mCallback.onSubscribeToCharacteristic(mService, mCharacteristic, mSubscribeType,
						Status.TIMEOUT);
			}
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"SubscribeToCharacteristicTask" +
					".onTaskError()");
			if (mCallback != null) {
				mCallback.onSubscribeToCharacteristic(mService, mCharacteristic, mSubscribeType,
						Status.ERROR);
			}

		}
	}

	////////////////////////////////////////
	// Log helper

	/**
	 * UnSubscribtion task.
	 */
	private class UnsubscribeToCharacteristicTask extends BLETask {

		private UnsubscribeToCharacteristicCallback mCallback;
		private UUID mService;
		private UUID mCharacteristic;
		private byte[] mSubscribeType;

		public UnsubscribeToCharacteristicTask (final @NonNull UUID service, final @NonNull UUID
				characteristic, final byte[] suubscribeType, UnsubscribeToCharacteristicCallback
				                                        callback) {
			super(ContextProvider.getInstance().getConfig()
					.BLE_CONNECTION_UNSUBSCRIBE_TIMEOUT_DELAY);
			mService = service;
			mCharacteristic = characteristic;
			mSubscribeType = suubscribeType;
			mCallback = callback;
		}

		@Override
		public boolean execute () {
			if (mBluetoothGatt != null) {
				BluetoothGattService gattService = mBluetoothGatt.getService(mService);
				if (gattService != null) {
					BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic
							(mCharacteristic);
					if (gattCharacteristic != null) {
						gattCharacteristic.setWriteType(BluetoothGattCharacteristic
								.WRITE_TYPE_DEFAULT);
						mBluetoothGatt.setCharacteristicNotification(gattCharacteristic, false);
						BluetoothGattDescriptor gattDescriptor = gattCharacteristic.getDescriptor
								(Constants.CLIENT_CHARACTERISTIC_CONFIG_UUID);
						gattDescriptor.setValue(mSubscribeType);
						Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
								getClass().getSimpleName() + ".writeDescriptor()" +
								" Service : " + mService.toString() +
								" Characteristic : " + mCharacteristic.toString());
						return mBluetoothGatt.writeDescriptor(gattDescriptor);
					}
				}
			}
			return false;
		}

		@Override
		public void onDescriptorWrite (final BluetoothGatt gatt, final BluetoothGattDescriptor
				descriptor, final int status) {
			if (mCharacteristic.equals(descriptor.getCharacteristic().getUuid())) {
				if (status == BluetoothGatt.GATT_SUCCESS) {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
							getClass().getSimpleName() + ".onDescriptorWrite() -> SUCCESS");
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_SUCCESS);
				} else {
					Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]->" +
							getClass().getSimpleName() + ".onDescriptorWrite() -> FAILURE");
					onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
				}
			}
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"UnsubscribeToCharacteristicTask.onTaskSucceed()");
			mSubscribeValueChangeCallbacks.get(mCharacteristic).clear();
			if (mCallback != null) {
				mCallback.onUnsubscribeToCharacteristic(Status.SUCCESS);
			}
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"UnsubscribeToCharacteristicTask.onTaskTimeout()");
			if (mCallback != null) {
				mCallback.onUnsubscribeToCharacteristic(Status.TIMEOUT);
			}
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"UnsubscribeToCharacteristicTask.onTaskError()");
			if (mCallback != null) {
				mCallback.onUnsubscribeToCharacteristic(Status.ERROR);
			}
		}
	}

	/**
	 * Pair task.
	 */
	private class PairingToDeviceTask extends BLETask {

		private PairCallback mCallback;
		private ParingBroadcastReceiver mPairingReceiver;
		private String mAddress;

		public PairingToDeviceTask (final String address, final PairCallback callback) {
			super(ContextProvider.getInstance().getConfig().BLE_CONNECTION_PAIRING_TIMEOUT_DELAY);
			mCallback = callback;
			mAddress = address;
			mPairingReceiver = new ParingBroadcastReceiver();
		}

		@Override
		public boolean execute () {
			/*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
				Logger.e("Pairing for ble device is not supported on this system");
				return false;
			}*/
			if (mBluetoothAdapter != null) {
				final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mAddress);
				if (device != null) {
					IntentFilter filter = new IntentFilter();
					filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
					filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
					mContext.registerReceiver(mPairingReceiver, filter);

					boolean isBondRequestSend = device.createBond();
					if (isBondRequestSend) {
						changePairState(PairingState.PAIRING);
					}

					return isBondRequestSend;
				}
			}
			return false;
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"PairingToDeviceTask.onTaskSucceed()");
			changePairState(PairingState.PAIRED);
			mContext.unregisterReceiver(mPairingReceiver);
			mCallback.onPair(Status.SUCCESS);
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"PairingToDeviceTask.onTaskTimeout()");
			changePairState(PairingState.UNPAIRED);
			mContext.unregisterReceiver(mPairingReceiver);
			mCallback.onPair(Status.TIMEOUT);
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"PairingToDeviceTask.onTaskError()");
			changePairState(PairingState.UNPAIRED);
			mContext.unregisterReceiver(mPairingReceiver);
			mCallback.onPair(Status.ERROR);
		}
	}

	/**
	 * Unpair Task
	 */
	private class UnpairingFromDeviceTask extends BLETask {

		private final UnpairCallback mCallback;
		private final ParingBroadcastReceiver mPairReceiver;
		private final String mAddress;

		public UnpairingFromDeviceTask (final String address, final UnpairCallback callback) {
			super(ContextProvider.getInstance().getConfig()
					.BLE_CONNECTION_UNPAIRING_TIMEOUT_DELAY);
			mPairReceiver = new ParingBroadcastReceiver();
			mCallback = callback;
			mAddress = address;
		}

		@Override
		public boolean execute () {
			if (mBluetoothAdapter != null) {
				final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mAddress);
				if (device != null) {
					if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
						Method localMethod = null;
						try {
							localMethod = device.getClass().getMethod("removeBond");
						} catch (NoSuchMethodException e) {
							e.printStackTrace();
						}
						if (localMethod != null) {
							IntentFilter intent = new IntentFilter();
							intent.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
							intent.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
							mContext.registerReceiver(mPairReceiver, intent);

							boolean isRemoveBondRequestSend = false;
							try {
								isRemoveBondRequestSend = (boolean) localMethod.invoke(device);
							} catch (IllegalAccessException | InvocationTargetException e) {
								e.printStackTrace();
								mContext.unregisterReceiver(mPairReceiver);
							}

							if (isRemoveBondRequestSend) {
								mPairingState = PairingState.UNPAIRING;
							} else {
								mContext.unregisterReceiver(mPairReceiver);
							}

							return isRemoveBondRequestSend;
						}
					}
				}
			}
			return false;
		}

		@Override
		public void onTaskSucceed () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"UnpairingFromDeviceTask.onTaskSucceed()");
			mPairingState = PairingState.UNPAIRED;
			mContext.unregisterReceiver(mPairReceiver);
			mCallback.onUnpair(Status.SUCCESS);
		}

		@Override
		public void onTaskTimeout () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"UnpairingFromDeviceTask.onTaskTimeout()");
			mPairingState = PairingState.UNPAIRED;
			mContext.unregisterReceiver(mPairReceiver);
			mCallback.onUnpair(Status.TIMEOUT);
		}

		@Override
		public void onTaskError () {
			Logger.v("BLEConnection[" + getWearableToken().getDeviceAddress() + "]-> " +
					"UnpairingFromDeviceTask.onTaskError()");
			mPairingState = PairingState.UNPAIRED;
			mContext.unregisterReceiver(mPairReceiver);
			mCallback.onUnpair(Status.ERROR);
		}
	}

	private class ParingBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive (final Context context, final Intent intent) {
			if (intent.getAction().equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
				final int newState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE,
						BluetoothDevice.ERROR);
				final int prevState = intent.getIntExtra(BluetoothDevice
						.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);
				BluetoothDevice bleDevice = intent.getParcelableExtra(BluetoothDevice
						.EXTRA_DEVICE);

				Logger.v("ParingBroadcastReceiver : ACTION_BOND_STATE_CHANGED :: from : " +
						logBondState(prevState) + " new State : " + logBondState(newState));

				if (getWearableToken().getDeviceAddress().equals(bleDevice.getAddress())) {
					if (newState == BluetoothDevice.ERROR) {
						BLETask task = mAsynchronousTaskQueue.getCurrentTask();
						if (task != null) {
							task.onTaskEnd(AsynchronousTaskQueue.TASK_STATUS_ERROR);
						}
					} else if (newState == BluetoothDevice.BOND_BONDED && prevState ==
							BluetoothDevice.BOND_BONDING) {
						BLETask task = mAsynchronousTaskQueue.getCurrentTask();
						if (task != null) {
							task.onTaskEnd(task instanceof PairingToDeviceTask ?
									AsynchronousTaskQueue.TASK_STATUS_SUCCESS :
									AsynchronousTaskQueue.TASK_STATUS_ERROR);
						}
					} else if (newState == BluetoothDevice.BOND_NONE) {
						BLETask task = mAsynchronousTaskQueue.getCurrentTask();
						if (task != null) {
							task.onTaskEnd(task instanceof UnpairingFromDeviceTask ?
									AsynchronousTaskQueue.TASK_STATUS_SUCCESS :
									AsynchronousTaskQueue.TASK_STATUS_ERROR);
						}
					}
				}
			}
		}
	}
}