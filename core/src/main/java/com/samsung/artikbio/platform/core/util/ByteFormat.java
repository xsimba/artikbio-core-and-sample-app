package com.samsung.artikbio.platform.core.util;

public class ByteFormat {
	private static final String BYTE_FORMAT = "%02X";

	public static String format (final byte[] data) {
		StringBuilder builder = new StringBuilder();
		if (data != null) {
			int length = data.length;
			for (int index = 0; index < data.length; index++) {
				builder.append(format(data[index]));
				if (index + 1 < length) {
					builder.append(" ");
				}
			}
		} else {
			builder.append("NULL");
		}
		return builder.toString();
	}

	public static String format (byte data) {
		return String.format(BYTE_FORMAT, data);
	}
}
