package com.samsung.artikbio.platform.core.persistance.realm;

import com.samsung.artikbio.platform.core.persistance.Dao;
import com.samsung.artikbio.platform.core.persistance.model.Identifiable;

import java.util.UUID;

import io.realm.RealmConfiguration;
import io.realm.RealmObject;

/**
 * {@link RealmDatabaseDao} implementation which uses {@link UUID} as
 * the identifier type.
 *
 * @param <Model>      the immutable model type which returned by
 *                     {@link Dao} methods.
 * @param <RealmModel> the mutable model type stored by Realm.
 */
public abstract class UUIDRealmDatabaseDao<Model extends Identifiable<UUID>, RealmModel extends RealmObject & RealmPersistable>
		extends RealmDatabaseDao<UUID, Model, RealmModel> {

	/**
	 * Use provided {@link RealmConfiguration} to get an instance of the
	 * {@code Realm} storage engine.
	 *
	 * @param configuration    used to create Realm.
	 * @param modelClass       to be stored by this instance.
	 * @param persistableClass to be stored by this instance.
	 * @throws IllegalArgumentException if {@code configuration} is using
	 *                                  {@code Realm.DEFAULT_REALM_NAME} as its file name.
	 */
	public UUIDRealmDatabaseDao(final RealmConfiguration configuration,
	                            final Class<Model> modelClass,
	                            final Class<RealmModel> persistableClass) {
		super(configuration, modelClass, persistableClass);
	}

	/**
	 * Generate a new, random {@link UUID}.
	 *
	 * @param model for which an id is being generated.
	 * @return the newly generated {@link UUID}.
	 */
	@Override
	protected UUID generateId(final Model model) {
		return UUID.randomUUID();
	}

}