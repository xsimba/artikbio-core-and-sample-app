package com.samsung.artikbio.platform.core.uart;

import com.samsung.artikbio.platform.core.event.WearableEvent;

import net.jcip.annotations.Immutable;

import java.util.UUID;

@Immutable
public class UartEvent extends WearableEvent {

	private byte[] mData;

	public UartEvent (final byte[] data, final UUID wearableIdentityUuid) {
		super(wearableIdentityUuid);
		mData = data;
	}

	public byte[] getData () {
		return mData;
	}
}
