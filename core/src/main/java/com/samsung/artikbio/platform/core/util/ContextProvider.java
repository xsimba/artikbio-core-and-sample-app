package com.samsung.artikbio.platform.core.util;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.util.exceptions.ContextProviderException;

public final class ContextProvider {

	private static final Object __SYNCHRONIZED_OBJECT__ = new Object();
	private static ContextProvider mInstance;
	private final Context mContext;
	private Config mConfig;

	private ContextProvider (Context context, Config config) {
		if (context instanceof Activity || context instanceof Service) {
			throw new ContextProviderException.InstantiationException(context);
		}
		mContext = context;
		mConfig = config;
		mConfig.logConfig();
	}

	/**
	 * Create instance methods. this createInstance will use the default config {@link
	 * Config.Builder#defaultConfig()}.
	 *
	 * @param context the application context. avoid using an Activity or a Service instance for
	 *                that.
	 *
	 * @return instance of this singleton.
	 *
	 * @see Config
	 */
	public static ContextProvider createInstance (@NonNull Context context) {
		if (mInstance == null) {
			synchronized (__SYNCHRONIZED_OBJECT__) {
				if (mInstance == null) {
					mInstance = new ContextProvider(GlobalUtils.cleanContext(context), Config
							.Builder.defaultConfig());
				}
			}
		}
		return mInstance;
	}

	/**
	 * Delete this singleton for garbage collection.
	 */
	public static void clear () {
		mInstance = null;
		System.gc();
	}

	/**
	 * Get instance method of this singleton.
	 *
	 * @return
	 */
	public static ContextProvider getInstance () {
		return mInstance;
	}

	/**
	 * Get the bluetooth adapter.
	 *
	 * @return an android BluetoothAdapter instance.
	 */
	public BluetoothAdapter getBluetoothAdapter () {
		BluetoothManager bluetoothManager = getBluetoothManager();
		return bluetoothManager != null ? bluetoothManager.getAdapter() : null;
	}

	/**
	 * Get the the bluetooth manager.
	 *
	 * @return an android BluetoothManager instance.
	 */
	public BluetoothManager getBluetoothManager () {
		return mContext != null ? (BluetoothManager) mContext.getSystemService(Context
				.BLUETOOTH_SERVICE) : null;
	}

	/**
	 * Verifies that there's an Application Context available or throws an {@link
	 * IllegalStateException} if not.
	 *
	 * @throws IllegalStateException if an Application Context was not set
	 */
	public void assertContextAvailable () {
		if (getApplicationContext() == null) {
			throw new IllegalStateException(
					"Context has not been set. Initialize with: Core.init(applicationContext)");
		}
	}

	/**
	 * Get the application context.
	 *
	 * @return the application context.
	 */
	public Context getApplicationContext () {
		return mContext;
	}

	/**
	 * Return the Config object.
	 *
	 * @return an instance of Config class.
	 */
	public Config getConfig () {
		return mConfig;
	}

	/**
	 * Set the Config objcet.
	 *
	 * @param config an instance of Config class.
	 *
	 * @see Config.Builder
	 */
	public void setConfig (@NonNull Config config) {
		mConfig = config;
	}
}