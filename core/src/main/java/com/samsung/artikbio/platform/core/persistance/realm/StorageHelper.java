package com.samsung.artikbio.platform.core.persistance.realm;

import android.content.Context;

import com.samsung.artikbio.platform.core.Internal;
import com.samsung.artikbio.platform.core.util.Logger;

import io.realm.RealmConfiguration;
import io.realm.RealmMigration;

/**
 * Helper base class used to get {@link RealmConfiguration} instances
 * for use based on values defined by subclass and a provided context.
 */
@Internal
public abstract class StorageHelper {

	/**
	 * Durability markers which indicate durability settings in
	 * {@link #getConfiguration(Context)} and {@link #getConfigurationBuilder(Context)}.
	 */
	public enum Durability {
		/**
		 * Indicates data will be persisted to the internal storage of
		 * the companion device.
		 */
		DISK,
		/**
		 * Indicated data will only be stored in memory and once all
		 * references to the Realm instance are released so is the data.
		 */
		MEMORY,
	}

	/** By default store data to disk. */
	public static final Durability DEFAULT_DURABILITY = Durability.DISK;

	/** Define the extension used for realm files on disk. */
	protected static final String REALM_EXT = ".realm";

	/** Initialize durability to {@link #DEFAULT_DURABILITY} for new instances. */
	private Durability durability = DEFAULT_DURABILITY;

	/**
	 * Inform Core to store data in memory only, not persisted to disk.
	 */
	public void inMemory() {
		setDurability(Durability.MEMORY);
	}

	/**
	 * Check whether data is stored in memory or on disk.
	 *
	 * @return {@code true} if data is being persisted only to memory.
	 */
	public boolean isInMemory() {
		return Durability.MEMORY.equals(this.durability);
	}

	/**
	 * Set/reset durability. Used for testing.
	 *
	 * @param durability to set.
	 */
	public void setDurability(final Durability durability) {
		this.durability = durability;
	}

	/**
	 * Get a {@link RealmConfiguration} appropriate for use by DAO
	 * instances based on the given {@code context} and values provided
	 * by other getter methods.
	 *
	 * @param context for which {@code RealmConfiguration} is created.
	 * @return the new {@code RealmConfiguration}.
	 * @throws IllegalArgumentException if {@code context} is {@code null}.
	 * @see #getRealmName()
	 * @see #getBaseModule()
	 * @see #getModules()
	 * @see #getSchemaVersion()
	 * @see #getMigration()
	 */
	public RealmConfiguration getConfiguration(final Context context) {
		return getConfigurationBuilder(context).build();
	}

	/**
	 * Get a {@link RealmConfiguration.Builder} appropriate for use by
	 * DAO instances but which can still be adjusted but the caller based
	 * on the given {@code context} and the values defined by getter
	 * methods.
	 *
	 * @param context for which {@code RealmConfiguration.Builder} is created.
	 * @return the new {@code RealmConfiguration.Builder}.
	 * @throws IllegalArgumentException if {@code context} is {@code null}.
	 * @see #getRealmName()
	 * @see #getBaseModule()
	 * @see #getModules()
	 * @see #getSchemaVersion()
	 * @see #getMigration()
	 */
	public RealmConfiguration.Builder getConfigurationBuilder(final Context context) {
		if (context == null) {
			throw new IllegalArgumentException("Context must be provided.");
		}
		final RealmMigration migration = this.getMigration();
		final long schemaVersion = this.getSchemaVersion();
		// Base Configuration Builder
		RealmConfiguration.Builder builder = new RealmConfiguration.Builder()
				.name(this.getRealmName())
				.modules(this.getBaseModule(), this.getModules());
		// Schema Version
		if (schemaVersion > 0L) {
			builder = builder.schemaVersion(this.getSchemaVersion());
		}
		// Migration
		if (migration != null) {
			builder = builder.migration(this.getMigration());
		} else if (schemaVersion > 1L) {
			Logger.w("It is strongly recommended you define a schema migration.");
		}
		// Durability
		if (isInMemory()) {
			builder = builder.inMemory();
		}
		return builder;
	}

	/**
	 * The file name the realm will use on disk or in memory.
	 *
	 * @return the Realm file name.
	 */
	protected abstract String getRealmName();

	/**
	 * The {@link io.realm.annotations.RealmModule} {@code Object} which
	 * informs a {@link RealmConfiguration} what types of data will be
	 * stored in the Realm.
	 *
	 * @return an object which is annotated with {@code @RealmModule}.
	 */
	protected abstract Object getBaseModule();

	/**
	 * Any {@code @RealModule}s to be included in the configuration
	 * in addition to the {@link #getBaseModule()}.
	 *
	 * @return an array of objects annotated with {@code @RealmModule}.
	 */
	protected abstract Object[] getModules();

	/**
	 * The version of the schema used within the Realm.
	 *
	 * @return the current schema version.
	 */
	protected abstract long getSchemaVersion();

	/**
	 * Provides a {@link RealmMigration} which can be used to update
	 * a Realm schema to the version defined by {@link #getSchemaVersion()}.
	 *
	 * @return a migration instance which can handle migrating any previous
	 * schema version to the current schema version.
	 */
	protected abstract RealmMigration getMigration();

}