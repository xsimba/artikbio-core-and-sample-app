package com.samsung.artikbio.platform.core.util;

import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;

public class Config {
	public final long DEFAULT_TIMEOUT_DELAY;

	////////////////////////////////////////
	// Connection

	// Networking layer
	public final Looper BLE_CONNECTION_LOOPER;
	public final long BLE_CONNECTION_CONNECTION_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_DISCONNECTION_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_READ_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_WRITE_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_SUBSCRIBE_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_UNSUBSCRIBE_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_PAIRING_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_UNPAIRING_TIMEOUT_DELAY;
	public final long BLE_CONNECTION_DISCOVER_SERVICES_TIMEOUT_DELAY;

	private Config (Builder builder) {
		DEFAULT_TIMEOUT_DELAY = builder.mDefaultTimeoutDelay >= 0 ? builder.mDefaultTimeoutDelay :
				10000;

		// Networking layer
		BLE_CONNECTION_LOOPER = builder.mBleConnectionLooper != null ? builder
				.mBleConnectionLooper : Looper.getMainLooper();
		BLE_CONNECTION_CONNECTION_TIMEOUT_DELAY = builder.mBleConnectionConnectionTimeoutDelay >=
				0 ? builder.mBleConnectionConnectionTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_DISCONNECTION_TIMEOUT_DELAY = builder
				.mBleConnectionDisconnectionTimeoutDelay >= 0 ? builder
				.mBleConnectionDisconnectionTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_READ_TIMEOUT_DELAY = builder.mBleConnectionReadTimeoutDelay >= 0 ? builder
				.mBleConnectionReadTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_WRITE_TIMEOUT_DELAY = builder.mBleConnectionWriteTimeoutDelay >= 0 ?
				builder.mBleConnectionWriteTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_SUBSCRIBE_TIMEOUT_DELAY = builder.mBleConnectionSubscribeTimeoutDelay >= 0
				? builder.mBleConnectionSubscribeTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_UNSUBSCRIBE_TIMEOUT_DELAY = builder.mBleConnectionUnsubscribeTimeoutDelay
				>= 0 ? builder.mBleConnectionUnsubscribeTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_PAIRING_TIMEOUT_DELAY = builder.mBleConnectionParingTimeoutDelay >= 0 ?
				builder.mBleConnectionParingTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_UNPAIRING_TIMEOUT_DELAY = builder.mBleConnectionUnpairingTimeoutDelay >= 0
				? builder.mBleConnectionUnpairingTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
		BLE_CONNECTION_DISCOVER_SERVICES_TIMEOUT_DELAY = builder
				.mBleConnectionDiscoverServicesTimeoutDelay >= 0 ? builder
				.mBleConnectionDiscoverServicesTimeoutDelay : DEFAULT_TIMEOUT_DELAY;
	}

	public void logConfig () {
		Logger.d("DEFAULT_TIMEOUT_DELAY : " + DEFAULT_TIMEOUT_DELAY);
		Logger.d("- BLE layer");
		Logger.d("BLE_CONNECTION_LOOPER : " + BLE_CONNECTION_LOOPER.getThread().getName());
		Logger.d("BLE_CONNECTION_CONNECTION_TIMEOUT_DELAY : " +
				BLE_CONNECTION_CONNECTION_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_DISCONNECTION_TIMEOUT_DELAY : " +
				BLE_CONNECTION_DISCONNECTION_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_READ_TIMEOUT_DELAY : " + BLE_CONNECTION_READ_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_WRITE_TIMEOUT_DELAY : " + BLE_CONNECTION_WRITE_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_SUBSCRIBE_TIMEOUT_DELAY : " +
				BLE_CONNECTION_SUBSCRIBE_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_UNSUBSCRIBE_TIMEOUT_DELAY : " +
				BLE_CONNECTION_UNSUBSCRIBE_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_PAIRING_TIMEOUT_DELAY : " + BLE_CONNECTION_PAIRING_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_UNPAIRING_TIMEOUT_DELAY : " +
				BLE_CONNECTION_UNPAIRING_TIMEOUT_DELAY);
		Logger.d("BLE_CONNECTION_DISCOVER_SERVICES_TIMEOUT_DELAY : " +
				BLE_CONNECTION_DISCOVER_SERVICES_TIMEOUT_DELAY);
	}

	////////////////////////////////////////
	// Builder

	public static class Builder {
		private long mDefaultTimeoutDelay = -1L;

		private Looper mBleConnectionLooper;
		private long mBleConnectionConnectionTimeoutDelay = -1L;
		private long mBleConnectionDisconnectionTimeoutDelay = -1L;
		private long mBleConnectionReadTimeoutDelay = -1L;
		private long mBleConnectionWriteTimeoutDelay = -1L;
		private long mBleConnectionSubscribeTimeoutDelay = -1L;
		private long mBleConnectionUnsubscribeTimeoutDelay = -1L;
		private long mBleConnectionParingTimeoutDelay = -1L;
		private long mBleConnectionUnpairingTimeoutDelay = -1L;
		private long mBleConnectionDiscoverServicesTimeoutDelay = -1L;


		public Builder () {
		}

		////////////////////////////////////////
		// Default config
		public static Config defaultConfig () {
			Builder configBuilder = new Builder();

			HandlerThread bleConnectionThread = new HandlerThread("BLE_CONNECTION_LOOPER", Process
					.THREAD_PRIORITY_BACKGROUND);
			bleConnectionThread.start();

			return configBuilder
					.defaultTimeoutDelay(10000)
					.bleConnectionSendLooper(bleConnectionThread.getLooper())
					.bleConnectionDiscoverServicesTimeoutDelay(20000)
					.bleConnectionConnectionTimeoutDelay(30000)
					.build();
		}

		/**
		 * the build method, call this to create an instance of a Config object.
		 *
		 * @return a Config object instance.
		 */
		public Config build () {
			return new Config(this);
		}

		/**
		 * The application connection time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionConnectionTimeoutDelay (long timeOut) {
			mBleConnectionConnectionTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application discover service time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionDiscoverServicesTimeoutDelay (long timeOut) {
			mBleConnectionDiscoverServicesTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * This will define a default looper thread for all the BLE layer (multi device). this
		 * looper is use to send data to a device.
		 *
		 * @param looper a Looper instance.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionSendLooper (Looper looper) {
			mBleConnectionLooper = looper;
			return this;
		}

		/**
		 * The application default time out delay. This is se by default for all ble operation.
		 *
		 * @param timeOut the default value in millisecond a ble task to time out.
		 *
		 * @return the Builder instance.
		 */
		public Builder defaultTimeoutDelay (long timeOut) {
			mDefaultTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application disconnection time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionDisconnectionTimeoutDelay (long timeOut) {
			mBleConnectionDisconnectionTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application paring time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionParingTimeoutDelay (long timeOut) {
			mBleConnectionParingTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application unpairing time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionUnpairingTimeoutDelay (long timeOut) {
			mBleConnectionUnpairingTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application readCharacteristic time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionReadTimeoutDelay (long timeOut) {
			mBleConnectionReadTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application writeCharacteristic time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionWriteTimeoutDelay (long timeOut) {
			mBleConnectionWriteTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application subscribeToCharacteristic time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionSubscribeTimeoutDelay (long timeOut) {
			mBleConnectionSubscribeTimeoutDelay = timeOut;
			return this;
		}

		/**
		 * The application unsubscribeToCharacteristic time out delay for the BLE layer.
		 *
		 * @param timeOut the value in millisecond for this type of ble task to time out. if the
		 *                value is negative the default value will be use instead.
		 *
		 * @return the Builder instance.
		 */
		public Builder bleConnectionUnsubscribeTimeoutDelay (long timeOut) {
			mBleConnectionUnsubscribeTimeoutDelay = timeOut;
			return this;
		}
	}
}
