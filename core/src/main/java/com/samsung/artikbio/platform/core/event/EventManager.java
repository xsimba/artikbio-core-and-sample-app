package com.samsung.artikbio.platform.core.event;

import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A way to inform the sync module of specific {@link EventHandler}
 * implementations. Includes methods to retrieve them based on the
 * {@link Event} types the {@link EventHandler} can process as well
 * as by the service name. This interface provides the means to register
 * and unregister services but these methods do not start the services,
 * services are responsible for performing register and unregister
 * during the {@link EventHandler} lifecycle.
 */
public final class EventManager {

	/** Hold {@link EventHandler} references by service name. */
	private final Map<String, EventHandler> mKnownServiceNames;
	/** Hold {@link EventHandler} references by event type. */
	private final Map<Class<? extends Event>, Set<EventHandler>> mKnownServiceEvents;
	/** Processor to execute {@link Runnable}s in the background. */
	private final Handler mHandler;

	/**
	 * Create a new instance with empty {@link Map}s for
	 * {@link EventHandler} references.
	 */
	EventManager () {
		mKnownServiceNames = new HashMap<>();
		mKnownServiceEvents = new HashMap<>();
		final HandlerThread handlerThread = new HandlerThread("EventHandler");
		handlerThread.start();
		mHandler = new Handler(handlerThread.getLooper());
	}

	/**
	 * Get the singleton instance of the manager.
	 *
	 * @return the singleton instance.
	 */
	public static synchronized EventManager getInstance () {
		return Holder.INSTANCE;
	}

	/**
	 * Retrieve a previously registered service by its name.
	 *
	 * @param serviceName of the service to retrieve.
	 *
	 * @return the previously registered service where {@code name} equals {@link
	 * EventHandler#getName()} if one exists; {@code null} otherwise.
	 */
	@Nullable
	public EventHandler getEventHandler (final String serviceName) {
		return mKnownServiceNames.get(serviceName);
	}

	/**
	 * Informs the module of the given {@link EventHandler} so it can later
	 * be retrieved by either {@link EventHandler#getName()} or one of the
	 * {@link EventHandler#getEventTypes()}. The {@link EventHandler#getName()}
	 * is expected to be a unique identifier for the {@link EventHandler}
	 * instance, if registering a new {@link EventHandler} with the same
	 * name (without first calling {@link #unregister(EventHandler)} has
	 * undocumented results. This method may throw if the given
	 * {@code eventHandler} is {@code null}.
	 *
	 * @param eventHandler to be registered.
	 *
	 * @return the now registered service.
	 */
	public EventHandler register (@NonNull final EventHandler eventHandler) {
		final Collection<Class<? extends Event>> eventTypes = eventHandler.getEventTypes();
		for (final Class<? extends Event> eventType : eventTypes) {
			final Set<EventHandler> services = getKnownServices(eventType);
			services.add(eventHandler);
			mKnownServiceEvents.put(eventType, services);
		}
		mKnownServiceNames.put(eventHandler.getName(), eventHandler);
		return eventHandler;
	}

	/**
	 * Get the {@link EventHandler}s that have been registered for the
	 * given {@code eventType}.
	 *
	 * @param eventType for which services will be retrieved.
	 *
	 * @return the {@link Set} of {@link EventHandler}s registered that can handle the given {@code
	 * eventType}.
	 */
	private Set<EventHandler> getKnownServices (final Class<? extends Event> eventType) {
		final Set<EventHandler> services;
		if (mKnownServiceEvents.containsKey(eventType)) {
			services = mKnownServiceEvents.get(eventType);
		} else {
			services = new HashSet<>();
		}
		return services;
	}

	/**
	 * Informs the module the given {@link EventHandler} should no longer
	 * be retrieved for its {@link EventHandler#getName()} nor its
	 * {@link EventHandler#getEventTypes()}. This method may throw if the
	 * given {@code eventHandler} is {@code null}.
	 *
	 * @param eventHandler to be unregistered.
	 *
	 * @return the now unregistered service.
	 */
	@NonNull
	public EventHandler unregister (@NonNull final EventHandler eventHandler) {
		final Collection<Class<? extends Event>> eventTypes = eventHandler.getEventTypes();
		for (final Class<? extends Event> eventType : eventTypes) {
			final Set<EventHandler> services = getKnownServices(eventType);
			services.remove(eventHandler);
			mKnownServiceEvents.put(eventType, services);
		}
		mKnownServiceNames.remove(eventHandler.getName());
		return eventHandler;
	}

	/**
	 * Send the given events to each registered {@link EventHandler}.
	 *
	 * @param events to be processed.
	 */
	public void post (final Event... events) {
		for (final Event event : events) {
			handle(event);
		}
	}

	/**
	 * Handle each {@link Event} by first getting the {@link EventHandler}
	 * implementations which expect it and processing each
	 * {@link EventHandler#handle(Event)} invocation within a {@link Runnable}
	 * on a {@link Handler}.
	 *
	 * @param event to be processed by {@link EventHandler}s.
	 */
	private void handle (final Event event) {
		if (event == null) {
			return;
		}

		final Set<EventHandler> services = getEventHandlers(event.getClass());
		for (final EventHandler handler : services) {
			mHandler.post(new HandleEventRunnable(handler, event));
		}
	}

	/**
	 * Return the set of previously registered {@link EventHandler}
	 * instances which indicate they can process events of the given
	 * {@code eventType} via {@link EventHandler#getEventTypes()}.
	 *
	 * @param eventType for which services will be retrieved.
	 *
	 * @return a collection of the services which can process the type, may be empty but should not
	 * be {@code null}.
	 */
	@NonNull
	public Set<EventHandler> getEventHandlers (final Class<? extends Event> eventType) {
		final Set<EventHandler> services = new HashSet<>();
		for (final Map.Entry<Class<? extends Event>, Set<EventHandler>> entry :
				mKnownServiceEvents.entrySet()) {
			final Class<? extends Event> eventClass = entry.getKey();
			if (eventType.isAssignableFrom(eventClass)) {
				services.addAll(entry.getValue());
			}
		}
		return Collections.unmodifiableSet(services);
	}

	/**
	 * Holds the {@code EventManager} instance.
	 */
	private static class Holder {
		/** The {@code EventManager} instance for {@link #getInstance()}. */
		private static final EventManager INSTANCE = new EventManager();
	}

	/**
	 * Hold the components necessary to execute
	 * {@link EventHandler#handle(Event)}.
	 */
	private static final class HandleEventRunnable implements Runnable {

		/** The {@code EventHandler} to be executed. */
		private final EventHandler mEventHandler;

		/** The {@code Event} to handle. */
		private final Event mEvent;

		/**
		 * Create a new instance setting all private members.
		 *
		 * @param eventHandler to set.
		 * @param event        to set.
		 */
		private HandleEventRunnable (final EventHandler eventHandler, final Event event) {
			mEventHandler = eventHandler;
			mEvent = event;
		}

		/**
		 * Invoke {@link EventHandler#handle(Event)}, if a {@link Throwable}
		 * is caught post {@link EventHandlerErrorEvent} to the
		 * {@link EventManager}.
		 */
		@Override
		public void run () {
			try {
				mEventHandler.handle(mEvent);
			} catch (Throwable t) {
				final EventHandlerErrorEvent status = new EventHandlerErrorEvent.Builder()
						.event(mEvent)
						.eventHandler(mEventHandler)
						.error(t)
						.build();
				EventManager.getInstance().post(status);
			}
		}
	}
}
