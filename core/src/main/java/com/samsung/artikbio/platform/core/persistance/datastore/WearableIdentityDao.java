package com.samsung.artikbio.platform.core.persistance.datastore;

import android.content.Context;

import com.samsung.artikbio.platform.core.persistance.model.WearableIdentity;
import com.samsung.artikbio.platform.core.persistance.realm.UUIDRealmDatabaseDao;

import java.util.UUID;

import io.realm.RealmConfiguration;

/**
 * A {@link UUIDRealmDatabaseDao} which is responsible for saving {@link WearableIdentity} data.
 */
public class WearableIdentityDao
		extends UUIDRealmDatabaseDao<WearableIdentity, PersistableWearableIdentity> {

	private static final String ADDRESS = "address";

	/**
	 * Creates a new instance using the given {@link Context} to create
	 * a {@link RealmConfiguration}.
	 *
	 * @param context to use when accessing data.
	 */
	public WearableIdentityDao(final Context context) {
		this(LocalDataStore.getConfiguration(context));
	}

	/**
	 * Creates a new instance using the provided {@link RealmConfiguration}.
	 *
	 * @param configuration to pass along to {@link UUIDRealmDatabaseDao}.
	 */
	public WearableIdentityDao(final RealmConfiguration configuration) {
		super(configuration, WearableIdentity.class, PersistableWearableIdentity.class);
	}

	@Override
	public PersistableWearableIdentity copyModel(final WearableIdentity model,
	                                             final PersistableWearableIdentity record) {
		if (record.getId() == null) {
			record.setId(model.getId() == null ? null : model.getId().toString());
		}
		record.setAddress(model.getDeviceAddress());
		record.setDisplayName(model.getDeviceName());
		record.setManufacturer(model.getManufacturer());
		record.setModel(model.getModel());
		record.setFirmwareRevision(model.getFirmwareRevision());
		record.setHardwareRevision(model.getHardwareRevision());
		record.setSoftwareRevision(model.getSoftwareRevision());
		return record;
	}

	@Override
	public WearableIdentity modelFrom(final PersistableWearableIdentity record) {
		return new WearableIdentity(
				UUID.fromString(record.getId()),
				record.getAddress(),
				record.getDisplayName(),
				record.getManufacturer(),
				record.getModel(),
				record.getFirmwareRevision(),
				record.getSoftwareRevision(),
				record.getHardwareRevision()
		);
	}

	/**
	 * Returns the UUID of the WearableIdentity for the given address or null if not found.
	 *
	 * @param address the address of the Wearable Identitiy to look for
	 * @return the UUID of the WearableIdentity for the given address or null if not found.
	 */
	public UUID getUUIDByAddress(final String address) {
		UUID result = null;
		PersistableWearableIdentity persistableWearableIdentity = query().equalTo(ADDRESS, address)
				.findFirst();
		if (persistableWearableIdentity != null) {
			result = UUID.fromString(persistableWearableIdentity.getId());
		}
		return result;
	}

	@Override
	protected UUID generateId(final WearableIdentity model) {
		return model.getId();
	}

}