package com.samsung.artikbio.platform.core.wearable.controller;

import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.Validations;
import com.samsung.artikbio.platform.core.wearable.ConnectionFactory;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.ble.BLEConnection;
import com.samsung.artikbio.platform.core.wearable.ble.BLEDeviceRegistry;
import com.samsung.artikbio.platform.core.wearable.controller.ble.BLEWearableController;
import com.samsung.artikbio.platform.core.wearable.controller.listeners.IWearableControllerListener;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Non-instantiable class that provides functionality for creating a concrete {@link
 * IWearableController} instance.
 */
public final class WearableControllerFactory {

	@NonNull
	private static final Map<WearableToken, IWearableController>
			CONTROLLER_MAP = new ConcurrentHashMap<>();


	/**
	 * Returns a {@link IWearableController} concrete instance based on the given {@link
	 * WearableToken}.
	 * If the wearable controller was already created for the provided token, the existing instance
	 * is returned, with its previously installed listeners if any, plus the provided listener.
	 *
	 * @param token    uniquely identifies the targeted wearable device
	 * @param listener a {@link IWearableControllerListener} to install to the returned wearable
	 *                 controller instance or {@code null} to install no listener
	 *
	 * @return the concrete instance of {@link IWearableController}
	 *
	 * @throws IllegalArgumentException if the token is invalid (null address)
	 */
	@NonNull
	public static IWearableController getWearableController (
			@NonNull final WearableToken token,
			final IWearableControllerListener listener) {
		Validations.validateNotNull(token, "token");
		ContextProvider.getInstance().assertContextAvailable();

		IWearableController wearableController = CONTROLLER_MAP.get(token);

		if (wearableController != null) {
			wearableController.addWearableControllerListener(listener);
			return wearableController;
		}

		Handler mainLooperHandler = new Handler(Looper.getMainLooper());
		BluetoothDevice bluetoothDevice = BLEDeviceRegistry.getBluetoothDevice(token);
		if (bluetoothDevice == null) {
			// no bluetooth device in the registry for this token, try to create one
			if (token.getDeviceAddress() == null) {
				throw new IllegalArgumentException("Invalid Wearable Token");
			} else {
				bluetoothDevice = ContextProvider.getInstance().getBluetoothAdapter()
						.getRemoteDevice(token.getDeviceAddress());
				BLEDeviceRegistry.register(token, bluetoothDevice);
			}
		}

		BLEConnection bleConnection = ConnectionFactory.createBLEConnection(token);
		wearableController = new BLEWearableController(
				token,
				listener,
				mainLooperHandler,
				bluetoothDevice,
				ContextProvider.getInstance().getBluetoothAdapter(),
				bleConnection
		);

		CONTROLLER_MAP.put(token, wearableController);
		return wearableController;
	}

	/**
	 * Dispose an {@link IWearableController} instance, cleaning the resources.
	 *
	 * @param wearableController the controller to be disposed
	 */
	public static void disposeWerableController (@NonNull final IWearableController
			                                             wearableController) {
		if (wearableController instanceof BLEWearableController) {
			((BLEWearableController) wearableController).dispose();
		}
		CONTROLLER_MAP.remove(wearableController.getWearableToken());
	}

	/**
	 * Obtains a {@code Collection} with all {@link IWearableController}s.
	 *
	 * @return Collection of wearable controllers.
	 */
	public static Collection<IWearableController> getWearableControllers () {
		return Collections.unmodifiableCollection(CONTROLLER_MAP.values());
	}
}
