package com.samsung.artikbio.platform.core.persistance.model;

import net.jcip.annotations.Immutable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.UUID;

/**
 * Immutable representation of a WearableIdentity that can be uniquely
 * identified by a {@link java.util.UUID}.
 */
@Immutable
public class WearableIdentity implements Identifiable<UUID> {

	/** Unique identifier for this WearableIdentity. */
	private final UUID mUUID;

	private final String mDeviceAddress;

	private final String mDeviceName;

	private final String mManufacturer;

	private final String mModel;

	private final String mFirmwareRevision;

	private final String mSoftwareRevision;

	private final String mHardwareRevision;


	/**
	 * Creates a {@code WearableIdentity} by providing all the values.
	 * @param UUID              for the wearable device
	 * @param deviceAddress     for the wearable device
	 * @param deviceName        for the wearable device
	 * @param manufacturer      of the wearable device
	 * @param model             of the wearable device
	 * @param firmwareRevision  for the wearable device
	 * @param softwareRevision  for the wearable device
	 * @param hardwareRevision  for the wearable device
	 */
	public WearableIdentity (final UUID UUID, final String deviceAddress, final String deviceName,
	                         final String manufacturer, final String model, final String
			                         firmwareRevision, final String softwareRevision, final String
			                         hardwareRevision) {
		mUUID = UUID;
		mDeviceAddress = deviceAddress;
		mDeviceName = deviceName;
		mManufacturer = manufacturer;
		mModel = model;
		mFirmwareRevision = firmwareRevision;
		mSoftwareRevision = softwareRevision;
		mHardwareRevision = hardwareRevision;
	}

	/**
	 * Retrieves the wearable device's UUID.
	 *
	 * @return UUID for wearable device
	 */
	@Override
	public UUID getId () {
		return mUUID;
	}

	/**
	 * Retrieves the wearable device's address.
	 *
	 * @return address of wearable device
	 */
	public String getDeviceAddress () {
		return mDeviceAddress;
	}

	/**
	 * Retrieves the wearable device's display name.
	 *
	 * @return display name of wearable device
	 */
	public String getDeviceName () {
		return mDeviceName;
	}

	/**
	 * Retrieves the wearable device's manufacturer.
	 *
	 * @return manufacturer of wearable device
	 */
	public String getManufacturer () {
		return mManufacturer;
	}

	/**
	 * Retrieves the wearable device's model.
	 *
	 * @return model of wearable device
	 */
	public String getModel () {
		return mModel;
	}

	/**
	 * Retrieves the wearable device's most recent firmware revision.
	 *
	 * @return firmware revision for wearable device
	 */
	public String getFirmwareRevision () {
		return mFirmwareRevision;
	}

	/**
	 * Retrieves the wearable device's most recent software revision.
	 *
	 * @return software revision for wearable device
	 */
	public String getSoftwareRevision () {
		return mSoftwareRevision;
	}

	/**
	 * Retrieves the wearable device's most recent hardware revision.
	 *
	 * @return hardware revision for wearable device
	 */
	public String getHardwareRevision () {
		return mHardwareRevision;
	}

	/**
	 * Equatable method. Two {@code WearableIdentity} instances are equal if their {@link
	 * WearableIdentity#getId()} is the same (they identify the same wearable device).
	 * Other {@code WearableIdentity} attributes are ignored when determining equality.
	 *
	 * @param o - the object to be compared.
	 * @return boolean value, indicating whether the objects are equal
	 */
	@Override
	public boolean equals (final Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		WearableIdentity wearableIdentity = (WearableIdentity) o;

		return new EqualsBuilder()
				.append(mUUID, wearableIdentity.mUUID)
				.isEquals();
	}

	/**
	 * Hashable method.
	 *
	 * @return integer value for hashcode
	 */
	@Override
	public int hashCode () {
		return new HashCodeBuilder(17, 37)
				.append(mUUID)
				.toHashCode();
	}
}
