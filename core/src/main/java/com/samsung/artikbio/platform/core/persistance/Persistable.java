package com.samsung.artikbio.platform.core.persistance;

/**
 * Meant to be implemented by a class which will be persisted by a
 * {@link Dao} instance. {@link Dao} implementations are only able to
 * interact with objects that have well defined identifiers.
 *
 * @param <Key> the identifier type
 */
public interface Persistable<Key> {

	/** Column name for key data. */
	String ID = "id";

	/**
	 * Gets the id of this instance.
	 *
	 * @return instance id.
	 */
	Key getId();

	/**
	 * Sets the identifier for this instance.
	 *
	 * @param id to set.
	 */
	void setId(Key id);

}