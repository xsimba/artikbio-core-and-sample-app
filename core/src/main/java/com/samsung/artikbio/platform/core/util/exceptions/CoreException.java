package com.samsung.artikbio.platform.core.util.exceptions;

public class CoreException extends RuntimeException {
	public CoreException (String message) {
		super(message);
	}
}
