package com.samsung.artikbio.platform.core.wearable.scanner;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.wearable.scanner.listeners.IWearableScannerListener;

/**
 * Wearable Scanner interface that defines common functionality across different scanner
 * implementations.
 */
public interface IWearableScanner {

	/**
	 * Starts scanning for wearable devices. The scanning process is executed in the background
	 * and events are reported to the provided {@link IWearableScannerListener} in the main UI
	 * thread
	 *
	 * @param wearableScannerListener a {@link IWearableScannerListener} instance to receive events
	 *                                from the scanning process
	 *
	 * @return whether scan could start successfully or not
	 */
	boolean startScan (@NonNull IWearableScannerListener wearableScannerListener);

	/**
	 * Stops scanning for wearable devices.
	 */
	void stopScan ();

	/**
	 * Indicates whether this scanner is currently scanning for wearable devices.
	 *
	 * @return whether this scanner is currently scanning for wearable devices
	 */
	boolean isScanning ();
}
