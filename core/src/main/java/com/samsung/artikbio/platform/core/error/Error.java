package com.samsung.artikbio.platform.core.error;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.samsung.artikbio.platform.core.R;
import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.util.Validations;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Error implements Parcelable {

	/**
	 * Parcelable Creator
	 */
	public static final Creator<Error> CREATOR = new Creator<Error>() {
		@NonNull
		@Override
		public Error createFromParcel (@NonNull final Parcel parcel) {
			Validations.validateNotNull(parcel, "parcel");
			return new Error(parcel);
		}

		@NonNull
		@Override
		public Error[] newArray (final int i) {
			return (i > 0) ? new Error[i] : new Error[0];
		}
	};
	/** Permission error, access denied. */
	public static final int ACCESS_DENIED = 100;
	/** Connection to the Wearable Device was established but its unique identifier is invalid. */
	public static final int BLE_INVALID_UNIQUE_IDENTIFIER = 101;
	/** BLE service or characteristic not found. */
	public static final int BLE_SERVICE_NOT_FOUND = 102;
	/** Bluetooth disabled. */
	public static final int BLUETOOTH_DISABLED = 103;
	/** Failed to connect. */
	public static final int CONNECTION_FAILED = 104;
	/** Destination already exists, won't overwrite. */
	public static final int DESTINATION_EXISTS = 105;
	/** Device disconnected. */
	public static final int DISCONNECTED = 106;
	/** Firmware update failed. */
	public static final int FIRMWARE_UPDATE_FAILED = 107;
	/** Firmware version check failed. */
	public static final int FIRMWARE_VERSION_CHECK_FAILED = 108;
	/** Generic error. */
	public static final int INTERNAL_ERROR = 109;
	/** Invalid data received. */
	public static final int INVALID_DATA = 110;
	/** Invalid parameter provided. */
	public static final int INVALID_PARAMETER = 111;
	/** Device not connected. */
	public static final int NOT_CONNECTED = 112;
	/** Requested operation failed. */
	public static final int OPERATION_FAILED = 113;
	/** Read failure. */
	public static final int READ_FAILED = 114;
	/** Subscription failure. */
	public static final int SUBSCRIBE_FAILED = 116;
	/** Wearable not found. */
	public static final int WEARABLE_NOT_FOUND = 117;
	/** Wearable not ready. */
	public static final int WEARABLE_NOT_READY = 118;
	/** Write failed. */
	public static final int WRITE_FAILED = 119;
	/** Default Error. */
	public static final int DEFAULT_ERROR = INTERNAL_ERROR;
	/**
	 * Bluetooth radio is disabled.
	 */
	public static final int BLE_ERROR_BT_DISABLED = BLUETOOTH_DISABLED;
	/**
	 * Bluetooth scanner already started.
	 */
	public static final int BLE_ERROR_SCANNER_ALREADY_STARTED = OPERATION_FAILED;
	/**
	 * Not connected to BLE device.
	 */
	public static final int BLE_ERROR_NOT_CONNECTED = NOT_CONNECTED;
	/**
	 * Invalid wearable token.
	 */
	public static final int BLE_ERROR_INVALID_WEARABLE_TOKEN = INVALID_PARAMETER;
	/**
	 * Can't readCharacteristic battery status from device.
	 */
	public static final int BLE_ERROR_BATTERY_STATUS_FAILURE = OPERATION_FAILED;
	/**
	 * BLEWearableController connect called with an active connection.
	 */
	public static final int BLE_ERROR_DEVICE_ALREADY_CONNECTED = OPERATION_FAILED;
	/**
	 * BLEWearableController connect called with another active connection.
	 */
	public static final int BLE_ERROR_SINGLE_DEVICE_ALREADY_CONNECTED = OPERATION_FAILED;
	/**
	 * Error trying to readCharacteristic a ble characteristic.
	 */
	public static final int BLE_ERROR_CHARACTERISTIC_READ_ERROR = READ_FAILED;
	/**
	 * Error trying to discard stored data.
	 */
	public static final int BLE_ERROR_DISCARD_STORED_DATA = OPERATION_FAILED;
	/**
	 * Error trying to set the activity report granularity.
	 */
	public static final int BLE_ERROR_ACTIVITY_REPORT_GRANULARITY = OPERATION_FAILED;
	/**
	 * Error trying to set classification enabled.
	 */
	public static final int BLE_ERROR_SET_CLASSIFICATION_ENABLED = OPERATION_FAILED;
	/**
	 * Error when trying to connect to the Wearable Device.
	 */
	public static final int BLE_ERROR_CONNECTION_FAILED = CONNECTION_FAILED;
	/**
	 * Connection to the Wearable Device got dropped.
	 */
	public static final int BLE_ERROR_CONNECTION_DISCONNECTED = DISCONNECTED;
	/**
	 * Connection to the Wearable Device was forcefully dropped.
	 */
	public static final int BLE_ERROR_CONNECTION_FORCEFULLY_DISCONNECTED = DISCONNECTED;
	/**
	 * BLE-protocol error (such as Bluetooth disabled, etc).
	 */
	public static final int BLE_ERROR = DEFAULT_ERROR;
	/**
	 * BT-protocol error (such as Bluetooth disabled, etc).
	 */
	public static final int BT_ERROR = DEFAULT_ERROR;
	/**
	 * Static mapping of predefined error codes and messages.
	 */
	private static final Map<Integer, Integer> ERROR_MESSAGES = new ConcurrentHashMap<>();

	static {
		ERROR_MESSAGES.put(ACCESS_DENIED, R.string.access_denied);
		ERROR_MESSAGES.put(BLE_INVALID_UNIQUE_IDENTIFIER, R.string.ble_invalid_unique_identifier);
		ERROR_MESSAGES.put(BLE_SERVICE_NOT_FOUND, R.string.ble_service_not_found);
		ERROR_MESSAGES.put(BLUETOOTH_DISABLED, R.string.bluetooth_disabled);
		ERROR_MESSAGES.put(CONNECTION_FAILED, R.string.connection_failed);
		ERROR_MESSAGES.put(DESTINATION_EXISTS, R.string.destination_exists);
		ERROR_MESSAGES.put(DISCONNECTED, R.string.disconnected);
		ERROR_MESSAGES.put(FIRMWARE_UPDATE_FAILED, R.string.firmware_update_failed);
		ERROR_MESSAGES.put(FIRMWARE_VERSION_CHECK_FAILED, R.string.firmware_version_check_failed);
		ERROR_MESSAGES.put(INTERNAL_ERROR, R.string.internal_error);
		ERROR_MESSAGES.put(INVALID_DATA, R.string.invalid_data);
		ERROR_MESSAGES.put(INVALID_PARAMETER, R.string.invalid_parameter);
		ERROR_MESSAGES.put(NOT_CONNECTED, R.string.not_connected);
		ERROR_MESSAGES.put(OPERATION_FAILED, R.string.operation_failed);
		ERROR_MESSAGES.put(READ_FAILED, R.string.read_failed);
		ERROR_MESSAGES.put(WEARABLE_NOT_FOUND, R.string.wearable_not_found);
		ERROR_MESSAGES.put(WEARABLE_NOT_READY, R.string.wearable_not_ready);
		ERROR_MESSAGES.put(WRITE_FAILED, R.string.write_failed);

		ERROR_MESSAGES.put(BLE_ERROR_BT_DISABLED, R.string.bluetooth_disabled);
		ERROR_MESSAGES.put(BLE_ERROR_SCANNER_ALREADY_STARTED, R.string.operation_failed);
		ERROR_MESSAGES.put(BLE_ERROR_NOT_CONNECTED, R.string.not_connected);
		ERROR_MESSAGES.put(BLE_ERROR_INVALID_WEARABLE_TOKEN, R.string.invalid_parameter);
		ERROR_MESSAGES.put(BLE_ERROR_BATTERY_STATUS_FAILURE, R.string.operation_failed);
		ERROR_MESSAGES.put(BLE_ERROR_DEVICE_ALREADY_CONNECTED, R.string.operation_failed);
		ERROR_MESSAGES.put(BLE_ERROR_SINGLE_DEVICE_ALREADY_CONNECTED, R.string.operation_failed);
		ERROR_MESSAGES.put(BLE_ERROR_CHARACTERISTIC_READ_ERROR, R.string.read_failed);
		ERROR_MESSAGES.put(BLE_ERROR_DISCARD_STORED_DATA, R.string.operation_failed);
		ERROR_MESSAGES.put(BLE_ERROR_ACTIVITY_REPORT_GRANULARITY, R.string.operation_failed);
		ERROR_MESSAGES.put(BLE_ERROR_SET_CLASSIFICATION_ENABLED, R.string.operation_failed);
		ERROR_MESSAGES.put(BLE_ERROR_CONNECTION_FAILED, R.string.connection_failed);
		ERROR_MESSAGES.put(BLE_ERROR_CONNECTION_DISCONNECTED, R.string.disconnected);
		ERROR_MESSAGES.put(BLE_ERROR_CONNECTION_FORCEFULLY_DISCONNECTED, R.string.disconnected);
		ERROR_MESSAGES.put(BLE_ERROR, R.string.internal_error);
		ERROR_MESSAGES.put(BT_ERROR, R.string.internal_error);
	}

	/**
	 * Integer enumeration of the error type. Must map to open of the static error code
	 * definitions.
	 * The following convention is used to identify error codes by category type. Error code
	 * levels
	 * 0-99 are reserved for general issues.
	 * 100-level error codes are reserved for device-related issues.
	 * 200-level error codes are reserved for companion-app-related issues.
	 */
	private int mErrorCode;
	/**
	 * String description of the error, containing necessary details.
	 */
	@NonNull
	private String mErrorMessage;

	/**
	 * Create an error providing an errorCode present in the mapping.
	 *
	 * @param errorCode an existing error code for which there's an associated error message To
	 *                  create an {@code Error} with a new message use {@link Error#Error(int,
	 *                  String)}
	 */
	public Error (final int errorCode) {
		final Context context = ContextProvider.getInstance().getApplicationContext();
		@StringRes int messageId = ERROR_MESSAGES.get(errorCode);
		String errorMessage = context.getResources().getString(messageId);
		init(errorCode, errorMessage);
	}

	/**
	 * Initializes the error instance with errorCode and an errorMessage
	 *
	 * @param errorCode    The code of the error
	 * @param errorMessage The message of the error
	 */
	private void init (final int errorCode, @NonNull final String errorMessage) {
		Validations.validateNotNull(errorMessage, "errorMessage");
		Validations.validateNotEmpty(errorMessage, "errorMessage");

		mErrorCode = errorCode;
		mErrorMessage = errorMessage;
	}

	/**
	 * Create an {@code Error} instance providing an errorCode and an errorMessage.
	 *
	 * @param errorCode    The code of the error
	 * @param errorMessage The message of the error
	 */
	public Error (final int errorCode, @NonNull final String errorMessage) {
		init(errorCode, errorMessage);
	}

	/**
	 * Private constructor to satisfy Parcelable implementation
	 *
	 * @param parcel
	 */
	private Error (@NonNull final Parcel parcel) {
		Validations.validateNotNull(parcel, "parcel");
		mErrorCode = parcel.readInt();
		mErrorMessage = parcel.readString();
	}

	/**
	 * Get the error code
	 *
	 * @return the error code
	 */
	public int getErrorCode () {
		return mErrorCode;
	}

	/**
	 * Get the error message.
	 *
	 * @return the error message
	 */
	@NonNull
	public String getErrorMessage () {
		return mErrorMessage;
	}

	@Override
	public int describeContents () {
		return 0;
	}

	@Override
	public void writeToParcel (@NonNull final Parcel parcel, final int flags) {
		Validations.validateNotNull(parcel, "parcel");
		parcel.writeInt(mErrorCode);
		parcel.writeString(mErrorMessage);
	}
}
