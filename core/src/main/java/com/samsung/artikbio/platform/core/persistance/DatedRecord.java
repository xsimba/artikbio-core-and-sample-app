package com.samsung.artikbio.platform.core.persistance;

import java.util.Date;

/**
 * Tracks when records change. Intended to be implemented by any record stored in a local database.
 */
public interface DatedRecord {

	/** Column name for the date a record was last persisted locally. */
	String UPDATED_AT = "updatedAt";

	/**
	 * Retrieves the {@link Date} when a record was last persisted to
	 * local storage.
	 *
	 * @return the stored value.
	 */
	Date getUpdatedAt();

	/**
	 * Sets the {@link Date} when a record is persisted.
	 *
	 * @param updatedAt to set.
	 */
	void setUpdatedAt(Date updatedAt);

}