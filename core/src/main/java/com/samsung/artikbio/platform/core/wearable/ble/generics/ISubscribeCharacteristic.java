package com.samsung.artikbio.platform.core.wearable.ble.generics;

import java.util.UUID;

public interface ISubscribeCharacteristic {

	boolean subscribeToCharacteristic (UUID service, UUID characteristic,
	                                   SubscribeToCharacteristicType subscribeType,
	                                   SubscribeToCharacteristicCallback callback);

	boolean unsubscribeToCharacteristic (UUID service, UUID characteristic,
	                                     UnsubscribeToCharacteristicCallback callback);

	enum SubscribeToCharacteristicType {
		NOTIFICATION,
		INDICATION
	}

	interface SubscribeToCharacteristicCallback {
		void onSubscribeToCharacteristic (UUID service, UUID characteristic,
		                                  SubscribeToCharacteristicType subscribeType, Status
				                                  status);

		void onValueChangeOfCharacteristic (UUID service, UUID characteristic, byte[] value);
	}

	interface UnsubscribeToCharacteristicCallback {
		void onUnsubscribeToCharacteristic (Status status);
	}
}