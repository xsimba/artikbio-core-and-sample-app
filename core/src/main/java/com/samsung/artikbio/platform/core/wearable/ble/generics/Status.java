package com.samsung.artikbio.platform.core.wearable.ble.generics;

/**
 * Status used on BLE operations
 */
public enum Status {
	SUCCESS,
	ERROR,
	TIMEOUT,
	NOT_FOUND,
	INVALID_PARAMETER,
	NOT_READY,
	TOO_LONG,
	NO_SPACE,
	IN_PROGRESS,
	NOT_CONNECTED
}