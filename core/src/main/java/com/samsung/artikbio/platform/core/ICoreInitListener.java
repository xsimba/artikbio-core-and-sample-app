package com.samsung.artikbio.platform.core;

/**
 * Interface for listening to Core SDK initialization events
 */
public interface ICoreInitListener {

	/**
	 * Called when the Core has initialized and it's ready to be used
	 */
	void onCoreInitialized ();
}
