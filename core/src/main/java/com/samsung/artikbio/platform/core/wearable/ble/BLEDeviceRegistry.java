package com.samsung.artikbio.platform.core.wearable.ble;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.Internal;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.util.Validations;
import com.samsung.artikbio.platform.core.wearable.WearableToken;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Utility class to hold the association between WearableTokens and BluetoothDevices.
 */
@Internal
public class BLEDeviceRegistry {

	@NonNull
	private static final Map<WearableToken, BluetoothDevice> BLE_DEVICE_MAP =
			new ConcurrentHashMap<>();

	/**
	 * Default private constructor (non-instantiable class).
	 */
	private BLEDeviceRegistry () {
		// Non-instantiable class
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Register a {@link BluetoothDevice} associated with a {@link WearableToken}.
	 *
	 * @param wearableToken   the {@link WearableToken} to associate the {@link BluetoothDevice}
	 *                        with
	 * @param bluetoothDevice the {@link BluetoothDevice}
	 *
	 * @return the previously associated {@link BluetoothDevice} if the {@code wearableToken} was
	 * already registered to a different {@link BluetoothDevice} instance
	 */
	public static BluetoothDevice register (@NonNull final WearableToken wearableToken,
	                                        @NonNull final BluetoothDevice bluetoothDevice) {
		Validations.validateNotNull(wearableToken, "wearableToken");
		Validations.validateNotNull(bluetoothDevice, "bluetoothDevice");

		Logger.d("Registering wearableToken [%s]", wearableToken.toString());
		return BLE_DEVICE_MAP.put(wearableToken, bluetoothDevice);
	}

	/**
	 * Return whether the {@code wearableToken} is registered or not.
	 *
	 * @param wearableToken the {@link WearableToken} instance
	 *
	 * @return whether the {@code wearableToken} is registered not.
	 */
	public static BluetoothDevice getBluetoothDevice (@NonNull WearableToken wearableToken) {
		Validations.validateNotNull(wearableToken, "wearableToken");
		return BLE_DEVICE_MAP.get(wearableToken);
	}

	/**
	 * Clear this registry removing all associations
	 */
	public static void clearRegistry () {
		BLE_DEVICE_MAP.clear();
	}
}
