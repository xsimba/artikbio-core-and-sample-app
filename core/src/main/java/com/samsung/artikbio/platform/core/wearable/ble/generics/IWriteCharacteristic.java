package com.samsung.artikbio.platform.core.wearable.ble.generics;

import java.util.UUID;

public interface IWriteCharacteristic {

	boolean writeCharacteristic (UUID service, UUID characteristic, byte[] data,
	                             WriteCharacteristicCallback callback);

	interface WriteCharacteristicCallback {
		void onWriteCharacteristic (UUID service, UUID characteristic, byte[] data, Status status);
	}
}