package com.samsung.artikbio.platform.core.wearable;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.util.ContextProvider;
import com.samsung.artikbio.platform.core.wearable.ble.BLEConnection;
import com.samsung.artikbio.platform.core.wearable.ble.generics.Connection;

public final class ConnectionFactory {

	/**
	 * Helper method to create an instance of a network connection depending of the device type.
	 *
	 * @param token token associated with wearable device to connect to.
	 *
	 * @return a Connection object.
	 *
	 * @see Connection
	 */
	@NonNull
	public static BLEConnection createBLEConnection (@NonNull WearableToken token) {
		return new BLEConnection(token, ContextProvider.getInstance().getConfig()
				.BLE_CONNECTION_LOOPER);
	}
}
