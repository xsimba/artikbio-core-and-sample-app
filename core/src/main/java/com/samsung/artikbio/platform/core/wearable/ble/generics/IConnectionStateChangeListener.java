package com.samsung.artikbio.platform.core.wearable.ble.generics;

public interface IConnectionStateChangeListener {
	void onConnectionStateChange (IConnectable.States oldState, IConnectable.States newState);
}