package com.samsung.artikbio.platform.core.util;

import android.app.Activity;
import android.app.Service;
import android.app.backup.BackupAgent;
import android.content.Context;
import android.support.annotation.NonNull;

import net.jcip.annotations.ThreadSafe;

/**
 * Utilities for interaction with {@link Context}.
 */
@ThreadSafe
public final class GlobalUtils {

	/**
	 * Private constructor prevents instantiation.
	 *
	 * @throws UnsupportedOperationException because this class cannot be instantiated.
	 */
	private GlobalUtils () {
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Gets the Application Context of {@code context} to prevent memory leaks
	 * from {@code Activity}, {@code Service}, or similar contexts.
	 * <p>
	 * This is typically used to check a Context parameter when entering a
	 * method that expects an Application context, because this will log whether
	 * the Context is correctly cleaned or not.
	 *
	 * @param context {@code Context} to clean. If this is a test context that doesn't support
	 *                {@link Context#getApplicationContext()}, then {@code context} will be
	 *                returned.
	 *
	 * @return Cleaned instance of {@code context}.
	 */
	@NonNull
	public static Context cleanContext (@NonNull final Context context) {
		Validations.validateNotNull(context, "context");

        /*
         * These warnings are important, because they allow Context memory leaks
         * to be fixed.
         */
		if (context instanceof Activity) {
			Logger.w("context was an instance of Activity%s", new Exception());
		} else if (context instanceof Service) {
			Logger.w("context was an instance of Service%s", new Exception());
		} else if (context instanceof BackupAgent) {
			Logger.w("context was an instance of BackupAgent%s", new Exception());
		}

		try {
			final Context returnContext = context.getApplicationContext();

            /*
             * Check for null is required because during unit tests the
             * Application context might not have been initialized yet
             */
			if (null == returnContext) {
				return context;
			}
			return returnContext;
		} catch (final UnsupportedOperationException e) {
	        /*
             * This is required for when the app's JUnit test suite is run.
             * Calling getApplicationContext() on a test context will fail.
             */
			Logger.w("Couldn't clean context; probably running in test mode%s", e);

			return context;
		}
	}
}