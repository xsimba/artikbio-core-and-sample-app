package com.samsung.artikbio.platform.core.persistance.datastore;

import com.samsung.artikbio.platform.core.Internal;

import io.realm.annotations.RealmModule;

/**
 * Internal Realm Module for Core.
 */
@RealmModule (library = true, allClasses = true)
@Internal
public class CoreModule {

}