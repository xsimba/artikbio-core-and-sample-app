package com.samsung.artikbio.platform.core.wearable;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.samsung.artikbio.platform.core.util.Validations;

import net.jcip.annotations.Immutable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;


@Immutable
public final class WearableToken implements Serializable {
	private static final long serialVersionUID = -1135359179156111445L;

	@NonNull
	private final String mDeviceAddress;

	@Nullable
	private final String mDeviceName;

	/**
	 * Constructor of the wearable token object
	 *
	 * @param deviceAddress wearable device address
	 * @param deviceName    optional wearable device display name
	 */
	public WearableToken (@NonNull final String deviceAddress, @Nullable final String deviceName) {

		Validations.validateNotNull(deviceAddress, "deviceAddress");
		Validations.validateNotEmpty(deviceAddress.trim(), "deviceAddress");

		mDeviceAddress = deviceAddress;
		mDeviceName = deviceName;
	}

	/**
	 * Retrieves the address associated with the wearable token
	 *
	 * @return - the address of the wearable device
	 */
	@NonNull
	public String getDeviceAddress () {
		return mDeviceAddress;
	}

	/**
	 * Retrieves a human-readable name associated with the wearable token
	 *
	 * @return - the display name of the wearable device
	 */
	@Nullable
	public String getDeviceName () {
		return mDeviceName;
	}

	/**
	 * Hashable method.
	 *
	 * @return integer value for hashcode
	 */
	@Override
	public int hashCode () {
		return new HashCodeBuilder(17, 37)
				.append(mDeviceAddress)
				.toHashCode();
	}

	/**
	 * Equatable method.
	 *
	 * @param o - the object to be compared.
	 *
	 * @return boolean value, indicating whether the objects are equal
	 */
	@Override
	public boolean equals (final Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		WearableToken that = (WearableToken) o;

		return new EqualsBuilder()
				.append(mDeviceAddress, that.mDeviceAddress)
				.isEquals();
	}

	/**
	 * Custom string convertible function.
	 *
	 * @return String representation for token
	 */
	@Override
	public String toString () {
		return new ToStringBuilder(this)
				.append("mAddress", mDeviceAddress)
				.append("mDisplayName", mDeviceName)
				.toString();
	}

}
