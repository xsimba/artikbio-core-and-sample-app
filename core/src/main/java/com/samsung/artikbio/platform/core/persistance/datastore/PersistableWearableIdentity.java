package com.samsung.artikbio.platform.core.persistance.datastore;

import com.samsung.artikbio.platform.core.persistance.model.WearableIdentity;
import com.samsung.artikbio.platform.core.persistance.realm.RealmPersistable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * A {@link RealmObject} for holding
 * {@link WearableIdentity} data.
 */
public class PersistableWearableIdentity extends RealmObject implements RealmPersistable {

	@Required
	@PrimaryKey
	private String id;

	private String address;

	private String displayName;

	private String manufacturer;

	private String model;

	private String firmwareRevision;

	private String softwareRevision;

	private String hardwareRevision;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(final String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	public String getFirmwareRevision() {
		return firmwareRevision;
	}

	public void setFirmwareRevision(final String firmwareRevision) {
		this.firmwareRevision = firmwareRevision;
	}

	public String getSoftwareRevision() {
		return softwareRevision;
	}

	public void setSoftwareRevision(final String softwareRevision) {
		this.softwareRevision = softwareRevision;
	}

	public String getHardwareRevision() {
		return hardwareRevision;
	}

	public void setHardwareRevision(final String hardwareRevision) {
		this.hardwareRevision = hardwareRevision;
	}
}