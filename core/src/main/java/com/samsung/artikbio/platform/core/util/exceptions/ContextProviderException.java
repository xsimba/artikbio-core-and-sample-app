package com.samsung.artikbio.platform.core.util.exceptions;

public final class ContextProviderException {
	public static class InstantiationException extends CoreException {
		public InstantiationException (Object context) {
			super("ContextProvider create instance should be call with the application context to " +
					"avoid leak. AVOID using " + context.getClass().getSimpleName());
		}
	}
}