package com.samsung.artikbio.platform.core.sample.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.samsung.artikbio.platform.core.sample.R;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.wearable.WearableToken;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * Adapter class for a collection of {@link WearableToken}s.
 */
public class WearableTokenAdapter extends ArrayAdapter<WearableToken> {

	/** Set of tokens. */
	public LinkedHashSet<WearableToken> tokenSet;

	/**
	 * WearableTokenAdapter constructor.
	 *
	 * @param context            the context
	 * @param textViewResourceId the id of the text view resource
	 */
	public WearableTokenAdapter (@NonNull final Context context,
	                             @LayoutRes final int textViewResourceId) {
		super(context, textViewResourceId);
		Logger.d("WearableTokenAdapter constructor");
		tokenSet = new LinkedHashSet<>();
	}

	/**
	 * Return {@code true} if one of the {@link WearableToken}s in the collection belongs to a
	 * connected Wearable device or {@code false} otherwise.
	 *
	 * @return {@code true} if one of the {@link WearableToken}s in the collection belongs to a
	 * connected Wearable device or {@code false} otherwise.
	 */
	public boolean hasConnectedWearables () {
		if (tokenSet != null && tokenSet.size() > 0) {
		    /*for (final WearableToken token : tokenSet) {
                final IWearableController controller = WearableControllerFactory
                        .getWearableController(token, WearableControllerEventManager.getInstance());
                if (controller.isConnected()) {
                    return true;
                }
            }*/
		}
		return false;
	}

	/**
	 * Return the list of connected {@link WearableToken}s.
	 *
	 * @return the list of connected {@link WearableToken}s
	 */
	public ArrayList<WearableToken> getConnectedTokens () {
		final ArrayList<WearableToken> connectedTokens = new ArrayList<>();
		if (tokenSet != null && tokenSet.size() > 0) {
			for (final WearableToken token : tokenSet) {
                /*final IWearableController controller = WearableControllerFactory
                        .getWearableController(token, WearableControllerEventManager.getInstance());
                if (controller.isConnected()) {
                    connectedTokens.add(token);
                }*/
			}
		}
		return connectedTokens;
	}

	/**
	 * Add a new {@link WearableToken} to the adapter.
	 *
	 * @param token the {@link WearableToken} to add
	 */
	public void add (final WearableToken token) {
		Logger.d("Adding device [%s]", token.getDeviceAddress());
		tokenSet.add(token);
		notifyDataSetChanged();
	}

	@Override
	public int getCount () {
		return tokenSet.size();
	}

	@Override
	public View getView (final int position, final View convertView, final ViewGroup parent) {

		final ViewHolder holder;
		View convertedView = convertView;
		if (null == convertedView) {
			final LayoutInflater inflater = LayoutInflater.from(getContext());
			convertedView = inflater.inflate(R.layout.token_list_item, parent, false);
			holder = new ViewHolder();
			holder.label = (TextView) convertedView.findViewById(R.id.token_label_item);
			holder.address = (TextView) convertedView.findViewById(R.id.token_id_item);
			holder.connectStatus = (TextView) convertedView.findViewById(R.id.connect_status);
			holder.pairStatus = (TextView) convertedView.findViewById(R.id.pair_status);
			convertedView.setTag(holder);
		} else {
			holder = (ViewHolder) convertedView.getTag();
		}

		final ArrayList<WearableToken> temp = new ArrayList<>(tokenSet);
		holder.label.setText(temp.get(position).getDeviceName());
		holder.address.setText(temp.get(position).getDeviceAddress());

		return convertedView;
	}

	/**
	 * Return the {@link WearableToken} in the specified position.
	 *
	 * @param position the position of the {@link WearableToken} to obtain
	 *
	 * @return the {@link WearableToken} at position
	 */
	public WearableToken getWearableToken (final int position) {
		final ArrayList<WearableToken> temp = new ArrayList<>(tokenSet);
		return temp.get(position);
	}

	/**
	 * Remove from the adapter all {@link WearableToken}s that belong to Wearable Devices that are
	 * not currently connected.
	 */
	public void removeDisconnectedDevices () {
		for (final Iterator<WearableToken> i = tokenSet.iterator(); i.hasNext(); ) {
			final WearableToken token = i.next();
		}
	}

	static class ViewHolder {

		TextView label, address, connectStatus, pairStatus;
	}
}
