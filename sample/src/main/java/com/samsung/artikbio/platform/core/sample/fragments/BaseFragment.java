package com.samsung.artikbio.platform.core.sample.fragments;

import android.support.v4.app.Fragment;

import org.greenrobot.eventbus.EventBus;

public abstract class BaseFragment extends Fragment {


	@Override
	public void onStart () {
		super.onStart();
		if (shouldRegisterForEventBus()) {
			EventBus.getDefault().register(this);
		}
	}

	@Override
	public void onStop () {
		if (shouldRegisterForEventBus()) {
			EventBus.getDefault().unregister(this);
		}
		super.onStop();
	}

	public abstract boolean shouldRegisterForEventBus ();
}
