package com.samsung.artikbio.platform.core.sample.util;

import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.wearable.WearableBatteryStatus;
import com.samsung.artikbio.platform.core.wearable.controller.IWearableController;

/**
 * Contains all {@link org.greenrobot.eventbus.EventBus} events that are used in the app as inner
 * classes.
 */
public final class EventBusUtil {

	/**
	 * Default private constructor (non-instantiable class).
	 */
	private EventBusUtil () {
		throw new UnsupportedOperationException("This class is non-instantiable");
	}

	/**
	 * Parent to all {@link EventBusUtil} events that contain an {@link
	 * IWearableController} field to denote which wearable device triggered the event.
	 */
	public abstract static class BaseWearableEvent {

		/**
		 * Wearable Controller instance.
		 */
		public IWearableController wearableController;
	}

	/**
	 * Broadcasts when a device is starting the connecting process.
	 */
	public static class ConnectingEvent extends BaseWearableEvent {

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device that is connecting.
		 */
		public ConnectingEvent (final IWearableController wearableController) {
			this.wearableController = wearableController;
		}
	}

	/**
	 * Broadcasts when a device has finished the connecting process.
	 */
	public static class ConnectedEvent extends BaseWearableEvent {

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device that is now connected.
		 */
		public ConnectedEvent (final IWearableController wearableController) {
			this.wearableController = wearableController;
		}
	}

	/**
	 * Broadcasts when a wearable device is starting the process of disconnecting.
	 */
	public static class DisconnectingEvent extends BaseWearableEvent {

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device that is disconnecting.
		 */
		public DisconnectingEvent (final IWearableController wearableController) {
			this.wearableController = wearableController;
		}
	}

	/**
	 * Broadcasts when a wearable device has finished disconnecting.
	 */
	public static class DisconnectedEvent extends BaseWearableEvent {

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device that is now disconnected.
		 */
		public DisconnectedEvent (final IWearableController wearableController) {
			this.wearableController = wearableController;
		}
	}

	/**
	 * Broadcasts when a wearable device is paired or unpaired.
	 */
	public static class PairedStatusChangedEvent extends BaseWearableEvent {

		/**
		 * Is paired value.
		 */
		public boolean isPaired;

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device that has had a change in paired
		 *                           status.
		 * @param isPaired           the new paired status.
		 */
		public PairedStatusChangedEvent (final IWearableController wearableController,
		                                 final boolean isPaired) {
			this.wearableController = wearableController;
			this.isPaired = isPaired;
		}
	}

	/**
	 * Broadcasts when a subscription to battery status is made or unmade.
	 */
	public static class BatterySubscriptionChangedEvent extends BaseWearableEvent {

		/**
		 * Is subscribed value.
		 */
		public boolean isSubscribed;

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device that has had a change in battery
		 *                           subscription status.
		 * @param isSubscribed       the new subscription status.
		 */
		public BatterySubscriptionChangedEvent (final IWearableController wearableController,
		                                        final boolean isSubscribed) {
			this.wearableController = wearableController;
			this.isSubscribed = isSubscribed;
		}
	}

	/**
	 * Broadcasts when a wearable device sends a battery status update.
	 */
	public static class BatteryStatusUpdateEvent extends BaseWearableEvent {

		/**
		 * Battery Status instance.
		 */
		public WearableBatteryStatus batteryStatus;

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device sending the update.
		 * @param batteryStatus      the new battery status.
		 */
		public BatteryStatusUpdateEvent (final IWearableController wearableController,
		                                 final WearableBatteryStatus batteryStatus) {
			this.wearableController = wearableController;
			this.batteryStatus = batteryStatus;
		}
	}

	/**
	 * Broadcasts when a wearable device transmits a failure message.
	 */
	public static class FailureEvent extends BaseWearableEvent {

		/**
		 * Error instance.
		 */
		public Error error;

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device sending the failure message.
		 * @param error              contains information on what the failure was.
		 */
		public FailureEvent (final IWearableController wearableController,
		                     final Error error) {
			this.wearableController = wearableController;
			this.error = error;
		}
	}
}
