package com.samsung.artikbio.platform.core.sample.eventmanager;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.event.Event;
import com.samsung.artikbio.platform.core.event.EventHandler;
import com.samsung.artikbio.platform.core.uart.UartEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WearableEventHandler implements EventHandler {

	private static final Set<Class<? extends Event>> EVENT_TYPES;

	static {
		final List<Class<? extends Event>> typeList = Arrays.asList(
				UartEvent.class
		);
		EVENT_TYPES = Collections.unmodifiableSet(new HashSet<>(typeList));
	}

	public static WearableEventHandler getInstance () {
		return Holder.INSTANCE;
	}

	@Override
	public String getName () {
		return this.getClass().getCanonicalName();
	}

	@NonNull
	@Override
	public Collection<Class<? extends Event>> getEventTypes () {
		return EVENT_TYPES;
	}

	@Override
	public void handle (final Event event) {
		EventBus.getDefault().post(event);
	}

	private static class Holder {
		private static final WearableEventHandler INSTANCE = new WearableEventHandler();
	}
}
