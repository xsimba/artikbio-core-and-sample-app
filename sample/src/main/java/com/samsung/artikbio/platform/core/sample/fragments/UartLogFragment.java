package com.samsung.artikbio.platform.core.sample.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.samsung.artikbio.platform.core.persistance.datastore.LocalDataStore;
import com.samsung.artikbio.platform.core.persistance.model.WearableIdentity;
import com.samsung.artikbio.platform.core.sample.R;
import com.samsung.artikbio.platform.core.sample.eventmanager.WearableEventHandler;
import com.samsung.artikbio.platform.core.uart.UartEvent;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.wearable.controller.IWearableController;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class UartLogFragment extends BaseFragment {


	@BindView (R.id.btn_enable)
	Button mBtnEnable;
	@BindView (R.id.btn_clear)
	Button mBtnClear;
	@BindView (R.id.log_window)
	TextView mLogWindow;
	@BindView (R.id.log_scroll)
	ScrollView mLogScroll;

	private boolean isEnabled;

	private IWearableController mWearableController;

	private WearableEventHandler mEventHandler = WearableEventHandler.getInstance();

	private String mDeviceAddress;


	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container,
	                          Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_uart_log, container, false);
		ButterKnife.bind(this, view);
		mLogWindow.setText(null, TextView.BufferType.EDITABLE);
		return view;
	}

	@Override
	public void onActivityCreated (@Nullable final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (mWearableController != null) {
			isEnabled = mWearableController.isWearableEventEnabled(UartEvent.class);
			mDeviceAddress = mWearableController.getWearableToken().getDeviceAddress();
		}

		mBtnEnable.setText(isEnabled ? getString(R.string.disable) : getString(R.string.enable));
	}

	@Override
	public void onResume () {
		super.onResume();
		if (mWearableController != null && mWearableController.getUartController() != null) {
			isEnabled = mWearableController.isWearableEventEnabled(UartEvent.class);
			mBtnEnable.setText(isEnabled ? getString(R.string.disable) : getString(R.string
					.enable));
		}
	}

	public void setWearableController (final IWearableController wearableController) {
		mWearableController = wearableController;
	}

	/**
	 * Dictates the actions to take when the "Subscribe/Unsubscribe" button is clicked. Subscribes
	 * or unsubscribes to receive UART logs from the device.
	 */
	@SuppressWarnings ("unused")
	@OnClick (R.id.btn_enable)
	public void subscribeButtonClicked () {
		if (mBtnEnable.getText().toString().equals(getString(R.string.enable))) {
			isEnabled = true;
			mBtnEnable.setText(R.string.disable);
			subscribe();
		} else {
			isEnabled = false;
			mBtnEnable.setText(R.string.enable);
			unsubscribe();
		}
	}

	public void subscribe () {
		if (mWearableController != null) {
			//noinspection unchecked
			mWearableController.enableWearableEvent(UartEvent.class);
		} else {
			Logger.e("controller is null while subscribing");
		}
	}

	public void unsubscribe () {
		if (mWearableController != null) {
			//noinspection unchecked
			mWearableController.disableWearableEvent(UartEvent.class);
		} else {
			Logger.e("controller is null while unsubscribing");
		}
	}

	/**
	 * Dictates the actions to take when the "Clear Logs" button is clicked, namely removing all
	 * displayed logs.
	 */
	@SuppressWarnings ("unused")
	@OnClick (R.id.btn_clear)
	public void clearLogsButtonClicked () {
		mLogWindow.getEditableText().clear();
	}

	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onUartEvent (final UartEvent event) {
		final WearableIdentity wearableIdentity = LocalDataStore.getWearableIdentity(event
				.getWearableIdentityUuid());
		if (wearableIdentity != null && wearableIdentity.getDeviceAddress() != null &&
				wearableIdentity.getDeviceAddress().equals(mDeviceAddress)) {
			if (!isEnabled) {
				isEnabled = true;
				mBtnEnable.setText(getString(R.string.disable));
			}
		}
	}

	private void log (final String message) {
		mLogWindow.getEditableText().append(message).append("\n");
		mLogScroll.fullScroll(View.FOCUS_DOWN);
	}

	@Override
	public boolean shouldRegisterForEventBus () {
		return true;
	}
}
