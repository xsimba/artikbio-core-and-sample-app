package com.samsung.artikbio.platform.core.sample.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.samsung.artikbio.platform.core.sample.BuildConfig;
import com.samsung.artikbio.platform.core.sample.R;
import com.samsung.artikbio.platform.core.sample.activity.DeviceDetailsActivity;
import com.samsung.artikbio.platform.core.sample.activity.MainActivity;
import com.samsung.artikbio.platform.core.sample.adapter.WearableTokenAdapter;
import com.samsung.artikbio.platform.core.wearable.WearableToken;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainFragment extends BaseFragment {

	final String versionLabel = "CORE v%s";
	/** WearableTokenAdapter instance. */
	public WearableTokenAdapter mWearableTokenListAdapter;
	@BindView (R.id.version_display)
	TextView mVersionDisplay;
	@BindView (R.id.wearableTokenListView)
	ListView mWearableTokenListView;
	@BindView (R.id.scanning_indicator)
	ProgressBar mScanningIndicator;
	private MainActivity mMainActivity;

	public MainFragment () {
		// Required empty public constructor
	}


	@Override
	public void onAttach (final Context context) {
		super.onAttach(context);
		mMainActivity = (MainActivity) context;
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container,
	                          Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		// Inflate the layout for this fragment
		final View view = inflater.inflate(R.layout.fragment_main, container, false);
		ButterKnife.bind(this, view);

		mWearableTokenListAdapter = new WearableTokenAdapter(mMainActivity,
				R.layout.token_list_item);
		mWearableTokenListView.setAdapter(mWearableTokenListAdapter);
		mWearableTokenListView.setOnItemClickListener((parent, view1, position, id) -> {
			final WearableToken token = mWearableTokenListAdapter.getWearableToken(position);
			if (token != null) {
				final Intent intent = new Intent(getActivity(), DeviceDetailsActivity.class);
				intent.putExtra(DeviceDetailsActivity.WEARABLE_TOKEN, token);
				startActivity(intent);
				stopScanButtonClicked();
			}
		});

		getSdkVersion();
		return view;
	}

	@Override
	public void onResume () {
		super.onResume();
		refreshList();
	}

	private void refreshList () {
		if (mWearableTokenListAdapter != null) {
			mWearableTokenListAdapter.notifyDataSetChanged();
		}
	}

	/**
	 * Called when the "Stop Scan" button is clicked. Stops scanning for nearby wearable devices
	 * and
	 * hides the scanning indicator.
	 */
	@OnClick (R.id.stop_scan_btn)
	public void stopScanButtonClicked () {
		showScanningIndicator(false);
		mMainActivity.stopScan();
	}

	/**
	 * Displays the version of the Core SDK library in the UI.
	 */
	public void getSdkVersion () {
		mVersionDisplay.setText(
				String.format(versionLabel, BuildConfig.VERSION_NAME));
	}

	/**
	 * Shows or hides a spinning indicator to let the user know if wearable device scanning is
	 * ongoing.
	 *
	 * @param show dictates whether the indicator should be shown or hidden.
	 */
	public void showScanningIndicator (final boolean show) {
		mScanningIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
	}

	/**
	 * Called when the "Start Scan" button is clicked. Initiates scanning for nearby wearable
	 * devices, and shows the spinning scanning indicator.
	 */
	@OnClick (R.id.start_scan_btn)
	public void startScanButtonClicked () {
		showScanningIndicator(true);
		mWearableTokenListAdapter.removeDisconnectedDevices();
		mWearableTokenListAdapter.notifyDataSetChanged();
		mMainActivity.startScan();
	}

	@Override
	public boolean shouldRegisterForEventBus () {
		return false;
	}
}
