package com.samsung.artikbio.platform.core.sample.activity;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.sample.R;
import com.samsung.artikbio.platform.core.sample.fragments.MainFragment;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.scanner.IWearableScanner;
import com.samsung.artikbio.platform.core.wearable.scanner.WearableScannerFactory;
import com.samsung.artikbio.platform.core.wearable.scanner.listeners.IWearableScannerListener;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

	private static final int PERMISSIONS_REQUEST_FINE_LOCATION = 1;

	private static final int REQUEST_ENABLE_BT = 1;

	IWearableScanner mWearableScanner;

	MainFragment mMainFragment;

	private boolean mPermitted = false;

	private FragmentManager mFragmentManager;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		mFragmentManager = getSupportFragmentManager();

		// For Android version upper or equal to Android M, permissions need to be asked
		// at the runtime. As MAC address are considerate as location data, ACCESS_COARSE_LOCATION
		// and ACCESS_FINE_LOCATION permissions are now required to make a BLE scan.
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
					!= PackageManager.PERMISSION_GRANTED) {
				final String[] request = new String[] {Manifest.permission.ACCESS_FINE_LOCATION};
				ActivityCompat.requestPermissions(this, request,
						PERMISSIONS_REQUEST_FINE_LOCATION);
			} else {
				mPermitted = true;
			}
		} else {
			mPermitted = true;
		}

		mMainFragment = (MainFragment) mFragmentManager.findFragmentById(R.id.fragment_main);
		mWearableScanner = WearableScannerFactory.getDefaultScanner();
	}

	/**
	 * Stops scanning for bluetooth wearable devices.
	 */
	public void stopScan () {
		mWearableScanner.stopScan();
	}

	@Override
	protected void onActivityResult (final int requestCode, final int resultCode,
	                                 final Intent data) {
		if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) {
			mMainFragment.showScanningIndicator(true);
			startScan();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Begins scanning for nearby bluetooth wearable devices. If any are discovered, they are sent
	 * to the {@link MainFragment} to be listed in the UI.
	 */
	public void startScan () {
		if (isLocationPermissionGranted()) {
			mWearableScanner.startScan(new IWearableScannerListener() {
				@Override
				public void onWearableFound (@NonNull final IWearableScanner wearableScanner,
				                             @NonNull final WearableToken wearableToken) {
					mMainFragment.mWearableTokenListAdapter.add(wearableToken);
				}

				@Override
				public void onScannerError (@NonNull final IWearableScanner wearableScanner,
				                            @NonNull final Error error) {
					if (error.getErrorCode() == Error.BLE_ERROR_BT_DISABLED) {
						mMainFragment.showScanningIndicator(false);
						final Intent enableBtIntent = new Intent(
								BluetoothAdapter.ACTION_REQUEST_ENABLE);
						startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
					} else {
						Toast.makeText(MainActivity.this, error.getErrorMessage(),
								Toast.LENGTH_LONG).show();
					}

				}
			});
		} else {
			Toast.makeText(getApplication(), getString(R.string.location_permission_from_settings),
					Toast.LENGTH_LONG).show();
		}
	}

	private boolean isLocationPermissionGranted () {
		mPermitted =
				ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
						== PackageManager.PERMISSION_GRANTED;
		return mPermitted;
	}

	@Override
	public void onRequestPermissionsResult (final int requestCode,
	                                        @NonNull final String[] permissions,
	                                        @NonNull final int[] grantResults) {
		switch (requestCode) {
			case PERMISSIONS_REQUEST_FINE_LOCATION:
				if (grantResults.length > 0) {
					boolean allGranted = true;
					for (int grantResult : grantResults) {
						allGranted = allGranted && (grantResult
								== PackageManager.PERMISSION_GRANTED);
					}
					mPermitted = allGranted;
				} else {
					Toast.makeText(this, R.string.location_permission, Toast.LENGTH_SHORT).show();
					finish();
				}
				break;
			default:
				Logger.w("Unknown request code: %d", requestCode);
		}
	}

	public boolean isPermitted () {
		return mPermitted;
	}

	@Override
	public boolean shouldRegisterForEventBus () {
		return false;
	}
}
