package com.samsung.artikbio.platform.core.sample.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.samsung.artikbio.platform.core.persistance.model.WearableIdentity;
import com.samsung.artikbio.platform.core.sample.R;
import com.samsung.artikbio.platform.core.sample.eventmanager.WearableControllerEventManager;
import com.samsung.artikbio.platform.core.sample.fragments.UartLogFragment;
import com.samsung.artikbio.platform.core.sample.util.EventBusUtil;
import com.samsung.artikbio.platform.core.util.Logger;
import com.samsung.artikbio.platform.core.wearable.WearableToken;
import com.samsung.artikbio.platform.core.wearable.controller.IWearableController;
import com.samsung.artikbio.platform.core.wearable.controller.WearableControllerFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeviceDetailsActivity extends BaseActivity {

	/**
	 * Wearable Token.
	 */
	public static final String WEARABLE_TOKEN = "WEARABLE_TOKEN";
	private static final String UART_FRAGMENT_TAG = "uartLogFragment";
	@BindView (R.id.connect_button)
	Button mConnectButton;
	@BindView (R.id.name_edit)
	TextView mDeviceNameEdit;
	@BindView (R.id.device_name_text)
	TextView mDeviceNameText;
	@BindView (R.id.device_address_text)
	TextView mDeviceAddressText;
	@BindView (R.id.manufacturer_name_text)
	TextView mManufacturerNameText;
	@BindView (R.id.model_number_text)
	TextView mModelNumberText;
	@BindView (R.id.identifier_text)
	TextView mIdentifierText;
	@BindView (R.id.firmware_revision_text)
	TextView mFirmwareRevisionText;
	@BindView (R.id.software_revision_text)
	TextView mSoftwareRevisionText;
	@BindView (R.id.hardware_revision_text)
	TextView mHardwareRevisionText;
	@BindView (R.id.battery_subscription)
	TextView mBatterySubscription;
	@BindView (R.id.battery_level_text)
	TextView mBatteryLevelText;
	private IWearableController mWearableController;
	private WearableToken mToken;
	private Menu mMenu;
	private FragmentManager mFragmentManager;
	private UartLogFragment mUartLogFragment;
	private FragmentManager.OnBackStackChangedListener deviceDetailsBackStackListener = new
			FragmentManager.OnBackStackChangedListener() {


				@Override
				public void onBackStackChanged () {
					String address = "";
					if (mWearableController != null && mWearableController.getWearableToken() !=
							null) {
						address = mWearableController.getWearableToken().getDeviceAddress();
					}

					final Fragment fragment = getSupportFragmentManager()
							.findFragmentById(R.id.container_fragment);

					setActivityTitle(address, fragment);
				}

				private void setActivityTitle (final String address, final Fragment fragment) {
					if (fragment != null) {
						if (fragment == mUartLogFragment) {
							setTitle("UartLogs: " + address);
						}
					}
				}
			};

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_device_details);
		ButterKnife.bind(this);

		mFragmentManager = getSupportFragmentManager();

		mFragmentManager.addOnBackStackChangedListener(deviceDetailsBackStackListener);

		final Bundle extras = getIntent().getExtras();
		if (null != extras) {
			mToken = (WearableToken) extras.getSerializable(WEARABLE_TOKEN);
			mDeviceAddressText.setText(mToken.getDeviceAddress());
			mDeviceNameText.setText(mToken.getDeviceName());
			mWearableController = WearableControllerFactory.getWearableController(mToken,
					WearableControllerEventManager.getInstance());
		}
	}

	@Override
	protected void onStart () {
		super.onStart();
		if (mWearableController != null && mWearableController.isConnected()) {
			showBatterySubscribedStatus(true);
		}
	}

	@Override
	protected void onStop () {
		super.onStop();
		if (mWearableController != null && mWearableController.isConnected()) {
			showBatterySubscribedStatus(false);
		}
	}

	@Override
	public boolean shouldRegisterForEventBus () {
		return true;
	}

	private void showBatterySubscribedStatus (final boolean subscribed) {
		if (subscribed) {
			mBatterySubscription.setText(getResources().getString(R.string.disable));
			mWearableController.subscribeToBatteryStatusUpdateEvents();
		} else {
			mBatterySubscription.setText(getResources().getString(R.string.enable));
			mWearableController.unsubscribeFromBatteryStatusUpdateEvents();
			mBatteryLevelText.setText("");
		}
	}

	@Override
	protected void onResume () {
		super.onResume();
		if (null != mWearableController) {
			mConnectButton.setText(mWearableController.isConnected() ? getResources().getString(R
					.string
					.disconnect) : getResources().getString(R.string.connect));
			if (mWearableController.isConnected()) {
				Logger.d("Displaying Device Data");
				final WearableIdentity wearableIdentity = mWearableController
						.getWearableIdentity();
				setDeviceData(wearableIdentity);
			}
		}
	}

	private void setDeviceData (final WearableIdentity wearableIdentity) {
		mDeviceAddressText.setText(mToken.getDeviceAddress());
		if (null != wearableIdentity) {
			mDeviceNameText.setText(wearableIdentity.getDeviceName());
			mManufacturerNameText.setText(wearableIdentity.getManufacturer());
			mIdentifierText.setText(wearableIdentity.getId().toString());
			mFirmwareRevisionText.setText(wearableIdentity.getFirmwareRevision());
			mSoftwareRevisionText.setText(wearableIdentity.getSoftwareRevision());
			mHardwareRevisionText.setText(wearableIdentity.getHardwareRevision());
			mModelNumberText.setText(wearableIdentity.getModel());
			mWearableController.getBatteryStatus();
			enabledSubscribedToggleAndNameEdit(true);
			showBatterySubscribedStatus(true);
		}
	}

	private void enabledSubscribedToggleAndNameEdit (final boolean enable) {
		final int color = ContextCompat.getColor(this, enable ? R.color.text_blue : R.color
				.disabled);

		mBatterySubscription.setEnabled(enable);
		mBatterySubscription.setTextColor(color);

		// TODO: 5/23/17 Once Device name change feature available enable this
		/*mDeviceNameEdit.setEnabled(enable);
		mDeviceNameEdit.setTextColor(color);*/
	}

	@Override
	public boolean onCreateOptionsMenu (final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.menu_device_details, menu);
		mMenu = menu;
		return true;
	}

	@Override
	public boolean onOptionsItemSelected (final MenuItem item) {
		final int id = item.getItemId();

		boolean consumeMenuProcessing = true;

		if (R.id.uart_log == id) {
			gotoUartLogFragment();
		} else {
			consumeMenuProcessing = false;
		}
		return consumeMenuProcessing || super.onOptionsItemSelected(item);
	}

	private void gotoUartLogFragment () {
		if (mUartLogFragment == null) {
			mUartLogFragment = new UartLogFragment();
		}

		mUartLogFragment.setWearableController(mWearableController);

		final boolean fragmentPopped = mFragmentManager.popBackStackImmediate(UART_FRAGMENT_TAG,
				0);
		if (!fragmentPopped) {
			mFragmentManager.beginTransaction()
					.replace(R.id.container_fragment, mUartLogFragment, UART_FRAGMENT_TAG)
					.addToBackStack(UART_FRAGMENT_TAG).commit();
		}
	}

	/**
	 * Called when the "Connect/Disconnect" button is clicked, this attempts to make a
	 * connection to
	 * the device so that the device information can be displayed in the UI. Disconnecting from a
	 * device also clears the information in the UI.
	 */
	@OnClick (R.id.connect_button)
	public void connectButtonClicked () {
		if (mWearableController.isConnected()) {
			mConnectButton.setEnabled(false);

			showBatterySubscribedStatus(false);
			mWearableController.disconnect();
		} else {
			mConnectButton.setEnabled(false);
			mWearableController.connect();
		}
	}

	/**
	 * Called when the "Subscribe/UnSubscribe" button which is on the battery level line in the UI
	 * is clicked. When subscribed, the battery level is displayed and regularly updated
	 */
	@OnClick (R.id.battery_subscription)
	public void batterySubscriptionClicked () {
		if (mWearableController.isConnected()) {
			if (mBatterySubscription.getText().toString().equals(getResources().getString(R.string
					.enable))) {
				showBatterySubscribedStatus(true);
			} else if (mBatterySubscription.getText().toString().equals(getResources().getString(R
					.string.disable))) {
				showBatterySubscribedStatus(false);
			}
		}
	}

	/**
	 * Called upon receiving an {@link EventBus} event. Disables the connect button while the
	 * wearable device is in the process of connecting.
	 *
	 * @param event that is received
	 */
	@SuppressLint ("unused")
	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onConnectingEvent (@SuppressWarnings (("UnusedParamters")) final EventBusUtil
			.ConnectingEvent event) {
		mConnectButton.setEnabled(false);
	}

	/**
	 * Called upon receiving an {@link EventBus} event. Sets the "Connect/Disconnect" button to
	 * "Disconnect" and enables it once the device is connected. Also populates the screen UI with
	 * information gotten from the connected device.
	 *
	 * @param event that is received
	 */
	@SuppressLint ("unused")
	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onConnectedEvent (final EventBusUtil.ConnectedEvent event) {
		mConnectButton.setEnabled(true);
		mConnectButton.setText(getResources().getString(R.string.disconnect));
		final WearableIdentity wearableIdentity = event.wearableController.getWearableIdentity();
		setDeviceData(wearableIdentity);
	}

	/**
	 * Called upon receiving an {@link EventBus} event. Disables the "Connect/Disconnect" buttong
	 * while the device is in the process of disconnecting.
	 *
	 * @param event that is received
	 */
	@SuppressLint ("unused")
	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onDisconnectingEvent (@SuppressWarnings (("UnusedParamters")) final EventBusUtil
			.DisconnectingEvent event) {
		mConnectButton.setEnabled(false);
	}

	/**
	 * Called upon receiving an {@link EventBus} event. Sets the "Connect/Disconnect" button to
	 * "Connect" and re-enables it now that the device has been disconnected. Also clears all
	 * device
	 * infromation out of the UI.
	 *
	 * @param event that is received
	 */
	@SuppressLint ("unused")
	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onDisconnectedEvent (@SuppressWarnings (("UnusedParamters")) final EventBusUtil
			.DisconnectedEvent event) {
		mConnectButton.setEnabled(true);
		mConnectButton.setText(getResources().getString(R.string.connect));

		mManufacturerNameText.setText(null);
		mIdentifierText.setText(null);
		mFirmwareRevisionText.setText(null);
		mSoftwareRevisionText.setText(null);
		mHardwareRevisionText.setText(null);
		mModelNumberText.setText(null);
		mBatteryLevelText.setText(null);
		enabledSubscribedToggleAndNameEdit(false);
	}

	/**
	 * Called upon receiving an {@link EventBus} event. Displays the current level of the
	 * battery in the UI.
	 *
	 * @param event that is received
	 */
	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onBatteryStatusUpdateEvent (final EventBusUtil.BatteryStatusUpdateEvent event) {
		mBatteryLevelText.setText(String.format("%s%%", Byte.toString(event.batteryStatus
				.getBatteryLevel())));
	}

	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onBatterySubscriptionChangedEvent (final EventBusUtil
			.BatterySubscriptionChangedEvent event) {
		if (event.isSubscribed) {
			event.wearableController.getBatteryStatus();
		}
	}

	/**
	 * Called upon receiving an {@link EventBus} event. When the wearable device transmits an
	 * error, this displays that error to the user in the form of an on-screen message.
	 *
	 * @param event that is received
	 */
	@Subscribe (threadMode = ThreadMode.MAIN)
	public void onFailureEvent (final EventBusUtil.FailureEvent event) {
		Logger.e("Error %d - %s", event.error.getErrorCode(), event.error.getErrorMessage());
		Toast.makeText(this, mWearableController.getWearableToken().getDeviceName() + " - " +
				mWearableController
				.getWearableToken().getDeviceAddress() + "\nError: " + event.error.getErrorCode()
				+ " - " + event.error.getErrorMessage(), Toast.LENGTH_SHORT).show();
		if (!event.wearableController.isConnected()) {
			WearableControllerEventManager.getInstance().onDisconnected(event.wearableController);
		}
	}
}
