package com.samsung.artikbio.platform.core.sample;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import com.samsung.artikbio.platform.core.Core;
import com.samsung.artikbio.platform.core.util.EncryptionKeyUtil;
import com.samsung.artikbio.platform.core.util.Logger;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * {@link Application} definition for the Core Sample Application. Initializes Core and
 * holds an application context for use throughout the app.
 */
public class BaseApplication extends Application {
	private static BaseApplication sInstance;

	public static Context getContext () {
		return sInstance.getApplicationContext();
	}

	@Override
	public void onCreate () {
		super.onCreate();
		if (sInstance == null) {
			sInstance = this;
		}

		Realm.init(sInstance);
		final RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
		Realm.setDefaultConfiguration(realmConfiguration);

		try {
			Core.init(sInstance.getApplicationContext(), () -> Logger.d("Core initialized in " +
							"Application"),
					EncryptionKeyUtil.getKey(getApplicationContext()));
			Logger.init(Logger.Level.DEBUG);
		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(this, "Couldn't create the encryption key for initializing Core",
					Toast.LENGTH_SHORT).show();
		}

		Logger.d("Core initialization success");
	}
}
