package com.samsung.artikbio.platform.core.sample.eventmanager;

import android.support.annotation.NonNull;

import com.samsung.artikbio.platform.core.error.Error;
import com.samsung.artikbio.platform.core.sample.util.EventBusUtil;
import com.samsung.artikbio.platform.core.wearable.WearableBatteryStatus;
import com.samsung.artikbio.platform.core.wearable.controller.IWearableController;
import com.samsung.artikbio.platform.core.wearable.controller.listeners.IWearableControllerListener;

import org.greenrobot.eventbus.EventBus;


/**
 * Singleton class to handle propagation of {@link IWearableController} related events.
 *
 * @see IWearableControllerListener
 */
public final class WearableControllerEventManager implements IWearableControllerListener {

	private WearableControllerEventManager () {

	}

	public static WearableControllerEventManager getInstance () {
		return Holder.INSTANCE;
	}

	@Override
	public void onConnecting (@NonNull final IWearableController wearableController) {
		EventBus.getDefault().post(new EventBusUtil.ConnectingEvent(wearableController));
	}

	@Override
	public void onConnected (@NonNull final IWearableController wearableController) {
		EventBus.getDefault().post(new EventBusUtil.ConnectedEvent(wearableController));
	}

	@Override
	public void onDisconnecting (@NonNull final IWearableController wearableController) {
		EventBus.getDefault().post(new EventBusUtil.DisconnectingEvent(wearableController));
	}

	@Override
	public void onDisconnected (@NonNull final IWearableController wearableController) {
		EventBus.getDefault().post(new EventBusUtil.DisconnectedEvent(wearableController));
	}

	@Override
	public void onPairedStatusChanged (@NonNull final IWearableController wearableController,
	                                   final boolean isPaired) {
		EventBus.getDefault()
				.post(new EventBusUtil.PairedStatusChangedEvent(wearableController, isPaired));
	}

	@Override
	public void onBatteryStatusUpdate (@NonNull final IWearableController wearableController,
	                                   @NonNull final WearableBatteryStatus batteryStatus) {
		EventBus.getDefault()
				.post(new EventBusUtil.BatteryStatusUpdateEvent(wearableController,
						batteryStatus));
	}

	@Override
	public void onFailure (@NonNull final IWearableController wearableController,
	                       @NonNull final Error error) {
		EventBus.getDefault().post(new EventBusUtil.FailureEvent(wearableController, error));
	}

	@Override
	public void subscribedToBatteryStatusUpdateEvents (
			@NonNull final IWearableController wearableController) {
		EventBus.getDefault().post(new EventBusUtil.BatterySubscriptionChangedEvent(
				wearableController, true
		));
	}

	@Override
	public void unsubscribedFromBatteryStatusUpdateEvents (
			@NonNull final IWearableController wearableController) {
		EventBus.getDefault().post(new EventBusUtil.BatterySubscriptionChangedEvent(
				wearableController, false
		));
	}

	private static class Holder {

		private static final WearableControllerEventManager INSTANCE = new
				WearableControllerEventManager();
	}

	/**
	 * Broadcasts when a wearable device sends a battery status update.
	 */
	public static class BatteryStatusUpdateEvent extends EventBusUtil.BaseWearableEvent {

		/**
		 * Battery Status instance.
		 */
		public WearableBatteryStatus batteryStatus;

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device sending the update.
		 * @param batteryStatus      the new battery status.
		 */
		public BatteryStatusUpdateEvent (final IWearableController wearableController,
		                                 final WearableBatteryStatus batteryStatus) {
			this.wearableController = wearableController;
			this.batteryStatus = batteryStatus;
		}
	}

	/**
	 * Broadcasts when a wearable device transmits a failure message.
	 */
	public static class FailureEvent extends EventBusUtil.BaseWearableEvent {

		/**
		 * Error instance.
		 */
		public Error error;

		/**
		 * Constructs and returns an instance of the event.
		 *
		 * @param wearableController the controller for the device sending the failure message.
		 * @param error              contains information on what the failure was.
		 */
		public FailureEvent (final IWearableController wearableController,
		                     final Error error) {
			this.wearableController = wearableController;
			this.error = error;
		}
	}
}
