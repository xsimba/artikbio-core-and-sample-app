package com.samsung.artikbio.platform.core.sample.activity;

import android.support.v7.app.AppCompatActivity;

import org.greenrobot.eventbus.EventBus;

/**
 * A parent activity to all activities in the app which use {@link EventBus}. Handles registering
 * and unregistering from EventBus.
 */
public abstract class BaseActivity extends AppCompatActivity {

	@Override
	protected void onStart () {
		super.onStart();
		if (shouldRegisterForEventBus()) {
			EventBus.getDefault().register(this);
		}
	}

	@Override
	protected void onStop () {
		if (shouldRegisterForEventBus()) {
			EventBus.getDefault().unregister(this);
		}
		super.onStop();
	}

	public abstract boolean shouldRegisterForEventBus ();
}
